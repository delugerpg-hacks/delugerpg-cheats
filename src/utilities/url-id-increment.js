// 2022-08-02 1:54 AM "17466369" last id

function weightedRandom(min, fixed = 1, max) {
  return (
    (min * 10 -
      1 +
      Math.round(
        (max * 10 - (min * 10 - 1)) /
          (Math.random() * (max * 10 - (min * 10 - 1)) + fixed)
      )) /
    10
  );
}

(function () {
  // ***
  const STOP = 1000;
  const timeLapse = 250;
  const start = 17452943;
  // ***
  let incremental = 0;
  let whatStatus = 0;
  let duration = 0;
  const partURL = 'https://www.delugerpg.com/allpokemon/user/';
  const partURL2 = '/filter/@defspe';
  const partURL3 = '/filter/@atkspe';
  const partURL4 = '/search/%40legends/class/retro';
  let id = 0;
  while (incremental < STOP) {
    incremental += 1;
    id = start + incremental;
    const interval = weightedRandom(
      Math.random() * (2 - 0.1) + 0.1,
      undefined,
      Math.random() * (10 - 6) + 6
    );
    duration += interval;
    const URL = partURL + id + partURL2;
    const URL2 = partURL + id + partURL3;
    const URL3 = partURL + id + partURL4;
    setTimeout(() => {
      console.log(++whatStatus);
      window.open(URL, '_blank');
      window.open(URL2, '_blank');
      window.open(URL3, '_blank');
    }, duration * timeLapse);
  }
  console.log('Next cycle from ID:', id + 1);
})();
