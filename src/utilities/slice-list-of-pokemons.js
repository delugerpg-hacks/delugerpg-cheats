const GetWants = (string) => {
  return string.split(', ');
};

// prettier-ignore
const wants = GetWants("Shiny Zapdos, Shiny Mega Rayquaza, Retro Shaymin, Ghostly G-Max Garbodor, Shadow G-Max Garbodor, Mirage G-Max Garbodor, Negative G-Max Garbodor, Retro G-Max Garbodor, Retro Pheromosa, Retro Zeraora, Ghostly G-Max Melmetal, Mirage G-Max Melmetal, Negative G-Max Melmetal, Shiny G-Max Rillaboom, Metallic G-Max Rillaboom, Ghostly G-Max Rillaboom, Shadow G-Max Rillaboom, Mirage G-Max Rillaboom, Chrome G-Max Rillaboom, Retro G-Max Rillaboom, Shiny G-Max Cinderace, Ghostly G-Max Cinderace, Dark G-Max Cinderace, Shadow G-Max Cinderace, Mirage G-Max Cinderace, Negative G-Max Cinderace, Retro G-Max Cinderace, Shiny G-Max Inteleon, Metallic G-Max Inteleon, Mirage G-Max Inteleon, Chrome G-Max Inteleon, Negative G-Max Inteleon, Shiny G-Max Corviknight, Metallic G-Max Corviknight, Ghostly G-Max Corviknight, Dark G-Max Corviknight, Shadow G-Max Corviknight, Mirage G-Max Corviknight, Chrome G-Max Corviknight, Negative G-Max Corviknight, Retro G-Max Corviknight, Shiny G-Max Orbeetle, Metallic G-Max Orbeetle, Ghostly G-Max Orbeetle, Dark G-Max Orbeetle, Shadow G-Max Orbeetle, Mirage G-Max Orbeetle, Negative G-Max Orbeetle, Retro G-Max Orbeetle, Shiny G-Max Drednaw, Metallic G-Max Drednaw, Ghostly G-Max Drednaw, Dark G-Max Drednaw, Mirage G-Max Drednaw, Chrome G-Max Drednaw, Negative G-Max Drednaw, Retro G-Max Drednaw, Shiny G-Max Coalossal, Metallic G-Max Coalossal, Ghostly G-Max Coalossal, Dark G-Max Coalossal, Shadow G-Max Coalossal, Mirage G-Max Coalossal, Chrome G-Max Coalossal, Negative G-Max Coalossal, Retro G-Max Coalossal, Shiny G-Max Flapple, Metallic G-Max Flapple, Ghostly G-Max Flapple, Dark G-Max Flapple, Shadow G-Max Flapple, Mirage G-Max Flapple, Chrome G-Max Flapple, Negative G-Max Flapple, Retro G-Max Flapple, Shiny G-Max Appletun, Metallic G-Max Appletun, Ghostly G-Max Appletun, Dark G-Max Appletun, Shadow G-Max Appletun, Mirage G-Max Appletun, Chrome G-Max Appletun, Negative G-Max Appletun, Retro G-Max Appletun, Shiny G-Max Sandaconda, Metallic G-Max Sandaconda, Ghostly G-Max Sandaconda, Shadow G-Max Sandaconda, Mirage G-Max Sandaconda, Chrome G-Max Sandaconda, Negative G-Max Sandaconda, Shiny G-Max Toxtricity, Metallic G-Max Toxtricity, Ghostly G-Max Toxtricity, Dark G-Max Toxtricity, Shadow G-Max Toxtricity, Mirage G-Max Toxtricity, Chrome G-Max Toxtricity, Shiny G-Max Centiskorch, Metallic G-Max Centiskorch, Ghostly G-Max Centiskorch, Chrome G-Max Centiskorch, Negative G-Max Centiskorch, Retro G-Max Centiskorch, Shiny G-Max Hatterene, Metallic G-Max Hatterene, Ghostly G-Max Hatterene, Shadow G-Max Hatterene, Mirage G-Max Hatterene, Chrome G-Max Hatterene, Negative G-Max Hatterene, Metallic G-Max Grimmsnarl, Ghostly G-Max Grimmsnarl, Dark G-Max Grimmsnarl, Shadow G-Max Grimmsnarl, Mirage G-Max Grimmsnarl, Negative G-Max Grimmsnarl, Retro G-Max Grimmsnarl, Shiny G-Max Alcremie, Metallic G-Max Alcremie, Ghostly G-Max Alcremie, Dark G-Max Alcremie, Shadow G-Max Alcremie, Mirage G-Max Alcremie, Chrome G-Max Alcremie, Negative G-Max Alcremie, Shiny G-Max Copperajah, Metallic G-Max Copperajah, Ghostly G-Max Copperajah, Dark G-Max Copperajah, Shadow G-Max Copperajah, Mirage G-Max Copperajah, Chrome G-Max Copperajah, Shiny G-Max Duraludon, Ghostly G-Max Duraludon, Shadow G-Max Duraludon, Mirage G-Max Duraludon, Chrome G-Max Duraludon, Negative G-Max Duraludon, Retro G-Max Duraludon, Shiny G-Max Urshifu, Metallic G-Max Urshifu, Ghostly G-Max Urshifu, Dark G-Max Urshifu, Shadow G-Max Urshifu, Mirage G-Max Urshifu, Chrome G-Max Urshifu, Negative G-Max Urshifu, Retro G-Max Urshifu, Chrome Sprigatito, Negative Sprigatito, Retro Sprigatito, Shiny Floragato, Mirage Floragato, Chrome Floragato, Negative Floragato, Retro Floragato, Shiny Meowscarada, Metallic Meowscarada, Ghostly Meowscarada, Dark Meowscarada, Shadow Meowscarada, Mirage Meowscarada, Chrome Meowscarada, Negative Meowscarada, Retro Meowscarada, Dark Fuecoco, Chrome Fuecoco, Retro Fuecoco, Shiny Crocalor, Ghostly Crocalor, Dark Crocalor, Shadow Crocalor, Mirage Crocalor, Chrome Crocalor, Retro Crocalor, Shiny Skeledirge, Metallic Skeledirge, Ghostly Skeledirge, Dark Skeledirge, Shadow Skeledirge, Mirage Skeledirge, Chrome Skeledirge, Negative Skeledirge, Retro Skeledirge, Shiny Quaxly, Shadow Quaxly, Chrome Quaxly, Negative Quaxly, Retro Quaxly, Shiny Quaxwell, Shadow Quaxwell, Mirage Quaxwell, Chrome Quaxwell, Negative Quaxwell, Retro Quaxwell, Shiny Quaquaval, Ghostly Quaquaval, Dark Quaquaval, Shadow Quaquaval, Mirage Quaquaval, Chrome Quaquaval, Negative Quaquaval, Retro Quaquaval, Shiny Lechonk, Metallic Lechonk, Chrome Lechonk, Shiny Oinkologne, Metallic Oinkologne, Ghostly Oinkologne, Dark Oinkologne, Shadow Oinkologne, Chrome Oinkologne, Negative Oinkologne, Retro Oinkologne, Shadow Tarountula, Negative Tarountula, Retro Tarountula, Shiny Spidops, Ghostly Spidops, Shadow Spidops, Mirage Spidops, Chrome Spidops, Negative Spidops, Retro Spidops, ");

// prettier-ignore
const normalLegends=["Articuno","Zapdos","Moltres","Mewtwo","Mega Mewtwo X","Mega Mewtwo Y","Raikou","Entei","Suicune","Lugia","Ho-oh","Regirock","Regice","Registeel","Latias","Mega Latias","Latios","Mega Latios","Kyogre","Primal Kyogre","Groudon","Primal Groudon","Rayquaza","Mega Rayquaza","Uxie","Mesprit","Azelf","Dialga","Palkia","Heatran","Regigigas","Giratina","Cresselia","Cobalion","Terrakion","Virizion","Tornadus","Thundurus","Reshiram","Zekrom","Landorus","Kyurem","Xerneas","Yveltal","Zygarde","Type: Null","Silvally","Tapu Koko","Tapu Lele","Tapu Bulu","Tapu Fini","Cosmog","Cosmoem","Solgaleo","Lunala","Necrozma","Zacian","Zamazenta","Eternatus","Kubfu","Urshifu","G-Max Urshifu","Regieleki","Regidrago","Glastrier","Spectrier","Calyrex","Enamorus","Wo-Chien","Chien-Pao","Ting-Lu","Chi-Yu","Koraidon","Miraidon"];
// prettier-ignore
const mythicalLegends=["Mew","Celebi","Jirachi","Deoxys","Phione","Manaphy","Darkrai","Shaymin","Arceus","Victini","Keldeo","Meloetta","Genesect","Diancie","Mega Diancie","Hoopa","Volcanion","Magearna","Marshadow","Zeraora","Meltan","Melmetal","G-Max Melmetal","Zarude",]
// prettier-ignore
const ultrabeastLegends=["Nihilego","Buzzwole","Pheromosa","Xurkitree","Celesteela","Kartana","Guzzlord","Poipole","Naganadel","Stakataka","Blacephalon",];
// prettier-ignore
const paradoxLegends=["Great Tusk","Scream Tail","Brute Bonnet","Flutter Mane","Slither Wing","Sandy Shocks","Iron Treads","Iron Bundle","Iron Hands","Iron Jugulis","Iron Moth","Iron Thorns","Roaring Moon","Iron Valiant","Koraidon","Miraidon","Walking Wake","Iron Leaves",];
const legends = [
  ...normalLegends
  // ...mythicalLegends,
  // ...ultrabeastLegends,
  // ...paradoxLegends,
];

let uniqueCounter = 0;
let legendsCounter = 0;
let uniquesContainer = [];
let legendsContainer = [];

for (let want of wants) {
  let isLegend = false;
  for (let legend of legends) {
    if (want.includes(legend)) isLegend = true;
  }
  if (!isLegend) {
    uniquesContainer.push(want);
    uniqueCounter += 1;
  } else {
    legendsContainer.push(want);
    legendsCounter += 1;
  }
}

function getNumbers(container) {
  const count = {
    Metallic: 0,
    Ghostly: 0,
    Dark: 0,
    Shadow: 0,
    Mirage: 0,
    Shiny: 0,
    Chrome: 0,
    Negative: 0,
    Retro: 0,
    Normal: 0
  };
  container.forEach((pokemon) => {
    const firstName = pokemon.split(' ')[0];
    switch (firstName) {
      case 'Metallic':
        count['Metallic'] += 1;
        break;
      case 'Ghostly':
        count['Ghostly'] += 1;
        break;
      case 'Dark':
        count['Dark'] += 1;
        break;
      case 'Shadow':
        count['Shadow'] += 1;
        break;
      case 'Mirage':
        count['Mirage'] += 1;
        break;
      case 'Shiny':
        count['Shiny'] += 1;
        break;
      case 'Chrome':
        count['Chrome'] += 1;
        break;
      case 'Negative':
        count['Negative'] += 1;
        break;
      case 'Retro':
        count['Retro'] += 1;
        break;
      case 'Normal':
        count['Normal'] += 1;
        break;
    }
  });
  return count;
}

// function pbcopy(data) {
//   var proc = require('child_process').spawn('pbcopy');
//   proc.stdin.write(data);
//   proc.stdin.end();
// }

const getClassesOfUniques = getNumbers(uniquesContainer);
const getClassesOfLegends = getNumbers(legendsContainer);

let list = `TOTAL = ${legendsContainer.length + uniquesContainer.length}`;
list += '\n====================\n';
list += `— ${legendsContainer.length} LEGENDS —\n`;
list += `${getClassesOfLegends['Metallic']} Metallic`;
list += ` + ${getClassesOfLegends['Normal']} Normal\n`;
list += `${getClassesOfLegends['Ghostly']} Ghostly`;
list += ` + ${getClassesOfLegends['Dark']} Dark\n`;
list += `${getClassesOfLegends['Shadow']} Shadow`;
list += ` + ${getClassesOfLegends['Mirage']} Mirage\n`;
list += `${getClassesOfLegends['Shiny']} Shiny`;
list += ` + ${getClassesOfLegends['Chrome']} Chrome\n`;
list += `${getClassesOfLegends['Negative']} Negative + `;
list += `${getClassesOfLegends['Retro']} Retro`;
list += '\n--------------------\n';
list += legendsContainer.sort().join('\n');
list += '\n====================\n';
list += `— ${uniquesContainer.length} NON-LEGENDS —\n`;
list += `${getClassesOfUniques['Normal']} Normal`;
list += ` + ${getClassesOfUniques['Metallic']} Metallic\n`;
list += `${getClassesOfUniques['Ghostly']} Ghostly`;
list += ` + ${getClassesOfUniques['Dark']} Dark\n`;
list += `${getClassesOfUniques['Shadow']} Shadow`;
list += ` + ${getClassesOfUniques['Mirage']} Mirage\n`;
list += `${getClassesOfUniques['Shiny']} Shiny`;
list += ` + ${getClassesOfUniques['Chrome']} Chrome\n`;
list += `${getClassesOfUniques['Negative']} Negative + `;
list += `${getClassesOfUniques['Retro']} Retro`;
list += '\n--------------------\n';
list += uniquesContainer.sort().join('\n');

// pbcopy(list);
console.log(list);
