{
  // Define constants
  const altSpirits = ['Galarian', 'Alolan', 'Hisuian'];
  const priorities = [
    'hasSpeDef',
    'hasSpeAtk',
    'hasSpe',
    'hasAtkDef',
    'hasDef',
    'hasAtk',
    'hasNoStats'
  ];

  const master = {};

  // Helper function to retrieve the base name of a Pokémon
  const getBaseName = (name) => {
    let pokemonName = name.split(' ');

    if (altSpirits.includes(pokemonName[0])) {
      pokemonName.shift();
    }

    if (altSpirits.includes(pokemonName[1])) {
      pokemonName = [pokemonName[0], ...pokemonName.slice(2)];
    }

    const bracketStartIdx = pokemonName.findIndex((word) => word.includes('('));
    if (bracketStartIdx !== -1) {
      pokemonName = pokemonName.slice(0, bracketStartIdx);
    }

    return pokemonName.join(' ');
  };

  // Helper function to check if a Pokémon's experience is 1 million or more
  const checkPokemonEXP = (pokemon) => {
    const exp = Number(
      pokemon
        .querySelector('.info')
        .textContent.match(/Exp:\s?([\d,]+)/)[1]
        .replaceAll(',', '')
    );
    return exp >= 1000000;
  };

  // Helper function to get the stat for a given element
  const getStat = (el) => {
    const speEl = !!el.querySelector('i.sbtn-spe');
    const atkEl = !!el.querySelector('i.sbtn-atk');
    const defEl = !!el.querySelector('i.sbtn-def');

    if (speEl && atkEl && defEl) return 'hasThreeStats';
    if (speEl && defEl) return 'hasSpeDef';
    if (speEl && atkEl) return 'hasSpeAtk';
    if (atkEl && defEl) return 'hasAtkDef';
    if (speEl) return 'hasSpe';
    if (atkEl) return 'hasAtk';
    if (defEl) return 'hasDef';
    return 'hasNoStats';
  };

  // Helper function to click on a DOM element after a random delay
  let totalClicksRequested = 0;
  let totalClicksCompleted = 0;
  let delayLog = 0;
  const clickAfterRandomDelay = (element) => {
    if (!(element instanceof Element)) {
      console.error('Provided object is not a DOM element.');
      return;
    }

    totalClicksRequested++;
    const delay = delayLog + Math.floor(Math.random() * 4000) + 1000;

    setTimeout(() => {
      element.click();
      totalClicksCompleted++;
      delayLog += delay;
      console.log(
        `Click completed. ${totalClicksCompleted}/${totalClicksRequested} clicks done.`
      );
    }, delay);
  };

  // Helper function to identify the highest priority for a given Pokémon
  const getHighestPriority = (name) => {
    for (const priority of priorities) {
      if (master[name][priority]) return priority;
    }
    return null;
  };

  // Main logic
  const pokemons = document.querySelectorAll('.container > div');

  for (const el of pokemons) {
    const name = getBaseName(el.querySelector('.name').dataset.name);
    if (!master[name]) master[name] = {};

    if (!checkPokemonEXP(el)) {
      const stat = getStat(el);
      master[name][stat] = (master[name][stat] || 0) + 1;
    }
  }

  let retained = {};

  for (let i = pokemons.length - 1; i >= 0; i--) {
    const el = pokemons[i];
    const name = getBaseName(el.querySelector('.name').dataset.name);
    const currentStat = getStat(el);

    if (currentStat === 'hasThreeStats') continue;

    const highestPriority = getHighestPriority(name);

    if (!retained[name] && currentStat === highestPriority) {
      retained[name] = true;
      continue;
    }

    clickAfterRandomDelay(el.querySelector('.trade'));
    master[name][currentStat]--;
    if (master[name][currentStat] === 0) delete master[name][currentStat];
  }
}
