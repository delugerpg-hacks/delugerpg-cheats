/* eslint-disable userscripts/filename-user */
/* eslint-disable no-console */
/* eslint-disable userscripts/filename-user */
/* eslint-disable userscripts/no-invalid-metadata */
{
  // Mouseover event to add a border on hover
  document.addEventListener('mouseover', function (event) {
    event.target.style.border = '2px solid red';
  });

  // Mouseout event to remove the border when the mouse leaves the element
  document.addEventListener('mouseout', function (event) {
    event.target.style.border = '';
  });

  // Click event to count and log the specific elements
  document.addEventListener('click', function (event) {
    const clickedElement = event.target;

    // Find all elements with the specified class and data attribute
    const potentialElements = clickedElement.querySelectorAll(
      '.fa-exclamation-circle.cursor.pokeid'
    );

    // Filter elements to include only those with the specified color
    const specificElements = Array.from(potentialElements).filter(
      (el) => el.style.color === 'rgb(0, 56, 176)'
    ); // Color in RGB format

    // Count and log the number of specific elements
    const count = specificElements.length;
    console.log('Count of specific elements with matching color:', count);
  });
}
