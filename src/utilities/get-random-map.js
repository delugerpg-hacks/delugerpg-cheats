// Group the maps by their names
const groupedMaps = {
  overworld: [
    '/map/overworld1',
    '/map/overworld2',
    '/map/overworld3',
    '/map/overworld4',
    '/map/overworld5',
    '/map/overworld6',
    '/map/overworld7',
    '/map/overworld8',
    '/map/overworld9',
    '/map/overworld10',
    '/map/overworld11',
    '/map/overworld12',
    '/map/overworld13',
    '/map/overworld14',
    '/map/overworld15',
    '/map/overworld16',
    '/map/overworld17',
    '/map/overworld18',
    '/map/overworld19',
    '/map/overworld20',
    '/map/overworld21',
    '/map/overworld22',
    '/map/overworld23',
    '/map/overworld24',
    '/map/overworld25'
  ],
  snowcave: [
    '/map/snowcave1',
    '/map/snowcave2',
    '/map/snowcave3',
    '/map/snowcave4'
  ],
  volcano: ['/map/volcano1', '/map/volcano2', '/map/volcano3', '/map/volcano4'],
  unownruins: ['/map/unownruins1', '/map/unownruins2'],
  pkmntower: [
    '/map/pkmntower1',
    '/map/pkmntower2',
    '/map/pkmntower3',
    '/map/pkmntower3'
  ],
  powerplant: [
    '/map/powerplant1',
    '/map/powerplant2',
    '/map/powerplant3',
    '/map/powerplant3'
  ],
  pkmnmansion: [
    '/map/pkmnmansion1',
    '/map/pkmnmansion2',
    '/map/pkmnmansion3',
    '/map/pkmnmansion4'
  ],
  rockcave: [
    '/map/rockcave1',
    '/map/rockcave2',
    '/map/rockcave3',
    '/map/rockcave4'
  ]
};

// Function to select a random map from each group
function getRandomMap() {
  const randomMaps = Object.values(groupedMaps).map((group) => {
    const randomIndex = Math.floor(Math.random() * group.length);
    return group[randomIndex];
  });
  const randomIndex = Math.floor(Math.random() * randomMaps.length);
  return randomMaps[randomIndex];
}

// Example usage:
const randomMaps = getRandomMap();
console.log(getRandomMap());
