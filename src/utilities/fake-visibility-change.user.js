// ==UserScript==
// @name         Fake Visibility
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.delugerpg.com/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=delugerpg.com
// @grant        none
// ==/UserScript==

// Immediately-invoked function expression (IIFE) to execute the code immediately and ensure no global variables are leaked.
(function () {
  'use strict'; // Enforce stricter parsing and error handling in JavaScript.

  // Override the document's visibilityState property to always report "visible".
  Object.defineProperty(document, 'visibilityState', {
    value: 'visible',
    writable: true // Allow this property to be changed later if needed.
  });

  // Override the document's hidden property to always report "false" (meaning the page is always visible).
  Object.defineProperty(document, 'hidden', {
    value: false,
    writable: true // Allow this property to be changed later if needed.
  });

  // Dispatch a visibilitychange event to inform any event listeners that the document's visibility state has changed.
  document.dispatchEvent(new Event('visibilitychange'));
})();
