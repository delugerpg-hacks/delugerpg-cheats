/* eslint-disable userscripts/filename-user */
// eslint-disable-next-line userscripts/no-invalid-metadata
{
  // Constants for fetch request configurations
  const FETCH_URL = 'https://www.delugerpg.com/quicktrade/submit';
  const COMMON_HEADERS = {
    'User-Agent':
      'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:122.0) Gecko/20100101 Firefox/122.0',
    Accept:
      'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Content-Type': 'application/x-www-form-urlencoded',
    'Upgrade-Insecure-Requests': '1',
    'Sec-Fetch-Dest': 'document',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-Site': 'same-origin',
    'Sec-Fetch-User': '?1'
  };
  const COMMON_BODY =
    'qtopt_lvl=1&qtadd_type=on&qtopt_type%5B%5D=shiny&qtopt_type%5B%5D=chrome&qtopt_type%5B%5D=retro&qtadd_stats=on&qtopt_stats=spe&qtadd_legend=on&qtadd_nlegend=on&qtopt_exp=1&qtopt_poke=&pid=';

  /**
   * Function to add a Pokemon ID to QuickTrade.
   * @param {string} pid - Pokemon ID.
   * @returns {Promise} - Returns a promise from the fetch API.
   */
  const addToQuickTrade = async (pid) => {
    return await fetch(FETCH_URL, {
      credentials: 'include',
      headers: COMMON_HEADERS,
      referrer: 'https://www.delugerpg.com/',
      body: COMMON_BODY + pid,
      method: 'POST',
      mode: 'cors'
    });
  };

  // Variable to keep track of the previously selected element
  let prevSelectedElement = null;

  /**
   * Event handler for mouseover event. Highlights the hovered row.
   * @param {Event} event - Mouseover event.
   */
  const hoverHandle = (event) => {
    const targetElement = event.target.closest('.row[data-pkid');

    // If the hovered element is the document body or the previously selected element, exit the function.
    if (
      targetElement === document.body ||
      (prevSelectedElement && prevSelectedElement === targetElement)
    ) {
      return;
    }

    // If there was a previously selected element, remove its border.
    if (prevSelectedElement) {
      prevSelectedElement.style.border = 'none';
      prevSelectedElement = null;
    }

    // If a valid target element is found, update its border and set it as the previous selected element.
    if (targetElement) {
      prevSelectedElement = targetElement;
      prevSelectedElement.style.border = '10px solid red';
    }
  };

  /**
   * Event handler for click event. Initiates the addToQuickTrade process if a valid row is clicked.
   * @param {Event} event - Click event.
   */
  const clickHandle = async (event) => {
    const targetElement = event.target.closest('.row[data-pkid');
    const id = targetElement && targetElement.id.substring(4);

    // If a valid Pokemon ID is found, add it to QuickTrade.
    if (id) {
      addToQuickTrade(id).then(() => console.log('Added.'));
    } else {
      alert('Eh?');
    }
  };

  // Attach event listeners to the document body for mouseover and click events.
  document.body.addEventListener('mouseover', hoverHandle, false);
  document.body.addEventListener('click', clickHandle, false);
}
