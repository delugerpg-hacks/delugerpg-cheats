const randomString = () => {
  let result = '';
  const CHARS =
    '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  const LEN = 6;
  for (let i = LEN; i > 0; --i)
    result += CHARS[Math.floor(Math.random() * CHARS.length)];
  return result;
};
randomString();
