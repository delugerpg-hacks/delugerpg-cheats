// ==UserScript==
// @name         Duplicate Removal (REVERSE)
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  For evolution, we need reverse priority
// @author       You
// @match        https://www.delugerpg.com/pokemon/duplicates
// @icon         https://www.google.com/s2/favicons?sz=64&domain=delugerpg.com
// @grant        none
// ==/UserScript==

// Duplicate removal revierse as in keep special class on trade and LQ in box

(function () {
  'use strict';

  let totalClicksRequested = 0;
  let totalClicksCompleted = 0;
  const clickQueue = [];

  function dups() {
    const altSpirits = ['Galarian', 'Alolan', 'Hisuian'];
    const priorities = [
      'hasSpe',
      'hasSpeAtk',
      'hasSpeDef',
      'hasAtkDef',
      'hasDef',
      'hasAtk',
      'hasNoStats'
    ];
    const master = {};

    const getBaseName = (name) => {
      let pokemonName = name.split(' ');

      if (altSpirits.includes(pokemonName[0])) {
        pokemonName.shift();
      }

      if (altSpirits.includes(pokemonName[1])) {
        pokemonName = [pokemonName[0], ...pokemonName.slice(2)];
      }

      const bracketStartIdx = pokemonName.findIndex((word) =>
        word.includes('(')
      );
      if (bracketStartIdx !== -1) {
        pokemonName = pokemonName.slice(0, bracketStartIdx);
      }

      return pokemonName.join(' ');
    };

    const checkPokemonEXP = (pokemon) => {
      const exp = Number(
        pokemon
          .querySelector('.info')
          .textContent.match(/Exp:\s?([\d,]+)/)[1]
          .replaceAll(',', '')
      );
      return exp >= 1000000;
    };

    const getStat = (el) => {
      const speEl = !!el.querySelector('i.sbtn-spe');
      const atkEl = !!el.querySelector('i.sbtn-atk');
      const defEl = !!el.querySelector('i.sbtn-def');

      if (speEl && atkEl && defEl) return 'hasThreeStats';
      if (speEl && defEl) return 'hasSpeDef';
      if (speEl && atkEl) return 'hasSpeAtk';
      if (atkEl && defEl) return 'hasAtkDef';
      if (speEl) return 'hasSpe';
      if (atkEl) return 'hasAtk';
      if (defEl) return 'hasDef';
      return 'hasNoStats';
    };

    const clickAfterRandomDelay = (element) => {
      if (!(element instanceof Element)) {
        console.error('Provided object is not a DOM element.');
        return;
      }
      totalClicksRequested++;
      clickQueue.push(element);
    };

    const getHighestPriority = (name) => {
      for (const priority of priorities) {
        if (master[name][priority]) return priority;
      }
      return null;
    };

    const pokemons = document.querySelectorAll('.container > div');

    for (const el of pokemons) {
      const name = getBaseName(el.querySelector('.name').dataset.name);
      if (!master[name]) master[name] = {};

      if (!checkPokemonEXP(el)) {
        const stat = getStat(el);
        master[name][stat] = (master[name][stat] || 0) + 1;
      }
    }

    let retained = {};

    for (let i = pokemons.length - 1; i >= 0; i--) {
      const el = pokemons[i];
      const name = getBaseName(el.querySelector('.name').dataset.name);
      const currentStat = getStat(el);

      if (currentStat === 'hasThreeStats') continue;

      const highestPriority = getHighestPriority(name);

      if (!retained[name] && currentStat === highestPriority) {
        retained[name] = true;
        continue;
      }

      clickAfterRandomDelay(el.querySelector('.trade'));
      master[name][currentStat]--;
      if (master[name][currentStat] === 0) delete master[name][currentStat];
    }

    processQueue();
  }

  function processQueue() {
    const minDelay = 100;
    const maxDelay = 800;

    if (totalClicksCompleted < totalClicksRequested) {
      const delay =
        Math.floor(Math.random() * (maxDelay - minDelay)) + minDelay;
      setTimeout(() => {
        const element = clickQueue[totalClicksCompleted];
        element.click();
        totalClicksCompleted++;
        console.log(
          `Click completed. ${totalClicksCompleted}/${totalClicksRequested} clicks done.`
        );
        processQueue();
      }, delay);
    } else {
      const pageReloadDelay = 2500; // 5 seconds delay before refreshing.
      setTimeout(() => {
        console.log('All clicks processed. Refreshing page...');
        document.location.reload();
      }, pageReloadDelay);
    }
  }

  dups();
})();
