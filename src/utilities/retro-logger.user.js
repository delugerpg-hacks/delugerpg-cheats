// ==UserScript==
// @name         Retro Existense Count Logger
// @namespace    http://tampermonkey.net/
// @version      2024-01-01
// @description  Retro pokemons
// @author       You
// @match        https://www.delugerpg.com/pokedex/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=delugerpg.com
// @grant        GM_getValue
// @grant        GM_setValue
// ==/UserScript==

(async function () {
  'use strict';

  const hasPokemons = [
    'Dewpider',
    'Sunkern',
    'Sewaddle',
    'Cufant',
    'Clauncher',
    'Clobbopus',
    'Oranguru',
    'Carnivine',
    'Bruxish',
    'Bunnelby',
    'Pansage',
    'Wooloo',
    'Spheal',
    'Shelmet',
    'Indeedee',
    'Umbreon',
    'Jigglypuff',
    'Shuckle',
    'Mantyke',
    'Dracovish',
    'Stufful',
    'Volbeat',
    'Relicanth',
    'Swablu',
    'Woobat',
    'Whismur',
    'Lillipup',
    'Lileep',
    'Drifloon',
    'Glameow',
    'Oricorio (Baile Style)',
    'Absol',
    'Jolteon',
    'Timburr',
    'Illumise',
    'Druddigon',
    'Phanpy',
    'Ferroseed',
    'Psyduck',
    'Pansear',
    'Golett',
    'Ekans',
    'Machop',
    'Komala',
    'Grimer',
    'Falinks',
    'Torkoal',
    'Diglett',
    'Porygon',
    'Ditto',
    'Klefki',
    'Inkay',
    'Hoppip',
    'Mimikyu (Busted Form)',
    'Togedemaru',
    'Anorith',
    'Stunfisk',
    'Drampa',
    'Roggenrola',
    'Dunsparce',
    'Eevee',
    'Chimchar',
    'Rolycoly',
    'Chimecho',
    'Unova Cap Pikachu',
    'Wailmer',
    'Gourgeist (Large)',
    'Arctozolt',
    'Horsea'
  ];

  let retroCount = 0;

  const pokemonName = document.querySelector('h1#pititle').textContent.trim();

  if (hasPokemons.includes(pokemonName)) {
    window.close();
  }

  const tracker = await GM_getValue('pokemons', {});

  tracker[pokemonName] = {
    count: null,
    hasForms: null
  };

  const getRetroCount = document
    .querySelectorAll('.dextabbody > *')[3]
    .querySelectorAll('td')[10]
    .textContent.trim();

  if (getRetroCount !== '-') {
    retroCount = parseInt(getRetroCount);
  }

  tracker[pokemonName].count = retroCount;

  const otherForms = document.querySelectorAll('#pinfovars > .pokebox');

  if (otherForms.length) {
    tracker[pokemonName].hasForms = true;
  } else {
    tracker[pokemonName].hasForms = false;
  }

  const total = Object.keys(tracker).length;

  tracker.total = total - 1;

  await GM_setValue('pokemons', tracker);

  setTimeout(window.close, 2000);
})();
