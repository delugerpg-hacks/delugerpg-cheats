// ==UserScript==
// @name         Store ID Evolution
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.delugerpg.com/pokemon/evolve/*
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM_deleteValue
// ==/UserScript==

(async function () {
  'use strict';
  try {
    const url = document.location.href;
    const urlSplit = url.split('/');
    const getID = urlSplit[urlSplit.length - 1];
    let ids = await GM_getValue('ids', '[]');

    // Check if ids is a string, and if so, parse it into an array
    if (typeof ids === 'string') {
      ids = JSON.parse(ids);
    }

    const evolved = document.querySelector('img.imgtolevel').src;
    if (evolved === 'https://i.dstatic.com/images/pokeball-n.png') {
      if (!ids.includes(getID)) {
        ids.push(getID);
        await GM_setValue('ids', JSON.stringify(ids));
      }
    }
    window.close();
  } catch (error) {
    // console.error('An error occurred:', error);
  }
})();
