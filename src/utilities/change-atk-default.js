{
  let numChanged = 0;
  let numToChange = 0;
  let fetchDelay = 2500;

  const fetchHeaders = {
    'User-Agent':
      'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/119.0',
    'Accept-Language': 'en-US,en;q=0.5',
    'X-Requested-With': 'XMLHttpRequest',
    'Sec-Fetch-Dest': 'empty',
    'Sec-Fetch-Mode': 'cors',
    'Sec-Fetch-Site': 'same-origin'
  };

  const fetchPokemonInfo = async (pokemonName, pokemonId) => {
    pokemonName = removeClassPrefix(pokemonName);

    const url = `https://www.delugerpg.com/api/pokedex/info/${pokemonName}`;

    try {
      const response = await fetch(url, {
        credentials: 'include',
        headers: {
          ...fetchHeaders,
          Accept: 'application/json, text/javascript, */*; q=0.01'
        },
        referrer: 'https://www.delugerpg.com/',
        method: 'GET',
        mode: 'cors'
      });
      const data = await response.json();
      await changePokemonAttack(pokemonId, pokemonName, data.attack);
    } catch (error) {
      console.error(`Error fetching data for ${pokemonName}:`, error);
    }
  };

  const changePokemonAttack = async (id, name, attacks) => {
    const body = `id=${id}&attack1=${attacks.a1.name}&attack2=${attacks.a2.name}&attack3=${attacks.a3.name}&attack4=${attacks.a4.name}`;

    try {
      const response = await fetch(
        'https://www.delugerpg.com/ajax/pokemon/changeattack/confirm',
        {
          credentials: 'include',
          headers: {
            ...fetchHeaders,
            Accept: '*/*',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
          },
          referrer: 'https://www.delugerpg.com/',
          body,
          method: 'POST',
          mode: 'cors'
        }
      );

      if (response.ok) {
        numChanged++;
        console.log(`✓ Changed ${numChanged} — ${name}`);
      }
    } catch (error) {
      console.error(`Error changing attack for ${name}:`, error);
    }
  };

  const removeClassPrefix = (name) => {
    const classes = [
      'Retro',
      'Shiny',
      'Chrome',
      'Dark',
      'Metallic',
      'Ghostly',
      'Mirage',
      'Shadow',
      'Negative'
    ];
    const firstName = name.split(' ')[0];
    return classes.includes(firstName)
      ? name.split(' ').slice(1).join(' ')
      : name;
  };

  document.querySelectorAll('.container .row').forEach((pokemon) => {
    let outputted = false;
    let hasProcessed = false;
    pokemon.querySelectorAll('.attk > div').forEach(() => {
      if (!hasProcessed) {
        hasProcessed = true;
        const hasThreeStats = ['spe', 'atk', 'def'].every((stat) =>
          pokemon.querySelector(`i.sbtn-${stat}`)
        );

        if (!outputted && hasThreeStats) {
          numToChange++;
          setTimeout(() => {
            fetchPokemonInfo(
              pokemon.querySelector('.name').textContent,
              pokemon.id.substring(4)
            );
            outputted = true;
          }, fetchDelay);
          fetchDelay += 2500;
        }
      }
    });
  });

  console.log(`➔ To change ${numToChange} pokemons.`);
}
