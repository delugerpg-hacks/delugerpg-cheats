/* eslint-disable userscripts/filename-user */
/* eslint-disable userscripts/no-invalid-metadata */
/* eslint-disable userscripts/filename-user */

const USER_AGENT =
  'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:122.0) Gecko/20100101 Firefox/122.0';

// Function to remove a trade
const removeTrade = async (link) => {
  return await fetch(link, {
    credentials: 'include',
    headers: {
      'User-Agent': USER_AGENT,
      Accept: 'application/json, text/javascript, */*; q=0.01',
      'Accept-Language': 'en-US,en;q=0.5',
      'X-Requested-With': 'XMLHttpRequest',
      'Sec-Fetch-Dest': 'empty',
      'Sec-Fetch-Mode': 'cors',
      'Sec-Fetch-Site': 'same-origin'
    },
    referrer: 'https://www.delugerpg.com/',
    method: 'GET',
    mode: 'cors'
  });
};

// Function to add a trade
const addTrade = async (id) => {
  const pokeId = id.substring(4);
  return await fetch('https://www.delugerpg.com/ajax/pokemon/trade', {
    credentials: 'include',
    headers: {
      'User-Agent': USER_AGENT,
      'Accept-Language': 'en-US,en;q=0.5',
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      'X-Requested-With': 'XMLHttpRequest',
      'Sec-Fetch-Dest': 'empty',
      'Sec-Fetch-Mode': 'cors',
      'Sec-Fetch-Site': 'same-origin'
    },
    referrer: 'https://www.delugerpg.com/',
    body: 'id=' + pokeId,
    method: 'POST',
    mode: 'cors'
  });
};

let prevSelectedEl = null;

// Handle hover event
const hoverHandle = (event) => {
  if (prevSelectedEl) {
    prevSelectedEl.style.border = 'none';
  }
  prevSelectedEl = event.currentTarget;
  prevSelectedEl.style.border = '10px solid red';
};

// Handle click event
const clickHandle = async (event) => {
  const id = event.currentTarget.id || undefined;
  if (id) {
    alert('Doing');
    const link = document.querySelector('.remove').href;
    removeTrade(link).then(() => addTrade(id).then(() => alert('Added.')));
  } else {
    alert('Eh?');
  }
};

// Attach event listeners to direct children of .container
const attachEventListeners = () => {
  const containerChildren = document.querySelectorAll('.container > *');
  containerChildren.forEach((child) => {
    child.addEventListener('mouseover', hoverHandle, false);
    child.addEventListener('click', clickHandle, false);
  });
};

// Escape function to detach handlers and cancel selection
const escHandle = (event) => {
  if (event.key === 'Escape') {
    if (prevSelectedEl) {
      prevSelectedEl.style.border = 'none';
      prevSelectedEl = null;
    }
    const containerChildren = document.querySelectorAll('.container > *');
    containerChildren.forEach((child) => {
      child.removeEventListener('mouseover', hoverHandle, false);
      child.removeEventListener('click', clickHandle, false);
    });
    document.body.removeEventListener('keydown', escHandle, false);
    alert('Script functionality disabled.');
  }
};

document.body.addEventListener('keydown', escHandle, false);
attachEventListeners();
