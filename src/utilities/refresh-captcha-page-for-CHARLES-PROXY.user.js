// ==UserScript==
// @name         New Userscript
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.delugerpg.com/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=delugerpg.com
// @grant        none
// ==/UserScript==

(function () {
  if (document.location.href !== 'https://www.delugerpg.com/unlock/verify') {
    document.location.href = 'https://www.delugerpg.com/unlock/verify';
  }
  if (
    document.querySelector('#col2').textContent.trim() === 'Nothing to See Here'
  ) {
    console.log('20s');
    setTimeout(() => {
      window.location.reload();
    }, 20 * 1000);
  } else {
    console.log('2min');
    setTimeout(
      () => {
        window.location.reload();
      },
      3 * 60 * 1000
    );
  }
})();
