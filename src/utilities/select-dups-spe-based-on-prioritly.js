{
  // DO NOT RUN ALPHABETICALLY, RUN NORMALLY.
  let allPokemons = {};
  let pageForwardUrl = '';
  let pageBackwardUrl = '';
  let dupCounter = 0;
  let selectedDupCounter = 0;
  const POKEMONS_TO_SELECT = prompt('how many', 250);
  let movingBackward = 0;
  const SECONDS = 1250;

  const removeUniques = () => {
    const pokemons = Object.keys(allPokemons);
    pokemons.forEach((pokemon) => {
      if (allPokemons[pokemon].dups === 0) {
        delete allPokemons[pokemon];
      }
    });
  };

  const checkPokemonEXP = (pokemon) => {
    const exp = Number(
      pokemon
        .querySelector('.info')
        .textContent.trim()
        .match(/Exp:\s?([\d,]+)/)[1]
        .replaceAll(',', '')
    );
    if (exp >= 1000000) return true;
    return false;
  };

  const deleteFadeover = () => {
    if (document.querySelector('.modal-backdrop.fade')) {
      document
        .querySelectorAll('.modal-backdrop.fade')
        .forEach((fadeover) => fadeover.remove());
    }
  };

  const clickPokemon = (pokemon, name) => {
    allPokemons[name].total -= 1;
    console.log(
      name,
      allPokemons[name].total,
      allPokemons[name].dups,
      allPokemons[name].ts,
      allPokemons[name].spe,
      allPokemons[name].speatk,
      allPokemons[name].spedef
    );
    if (!pokemon.classList.contains('selected'))
      setTimeout(() => pokemon.click(), 100);
  };

  const getPokemons = () => {
    if (pageForwardUrl !== document.location.href) {
      pageForwardUrl = document.location.href;
      if (dupCounter >= POKEMONS_TO_SELECT) {
        return false;
      }
      const pokemons = document.querySelectorAll('.container > .row');
      for (const pokemon of pokemons) {
        if (dupCounter >= POKEMONS_TO_SELECT) break;
        const speEl = !!pokemon.querySelector('i.sbtn-spe');
        const atkEl = !!pokemon.querySelector('i.sbtn-atk');
        const defEl = !!pokemon.querySelector('i.sbtn-def');
        const hasAtk =
          !!pokemon.querySelector('i.sbtn-atk') && !(speEl || defEl);
        const hasDef =
          !!pokemon.querySelector('i.sbtn-def') && !(speEl || atkEl);
        const hasSpe =
          !!pokemon.querySelector('i.sbtn-spe') && !(defEl || atkEl);
        const hasAtkDef = hasAtk && defEl && !speEl;
        const hasNoStat = !hasAtk && !defEl && !speEl;
        const hasExp = checkPokemonEXP(pokemon);

        const hasSpeAtk = speEl && atkEl && !defEl;
        const hasSpeDef = speEl && defEl && !atkEl;
        const hasThreeStats = hasAtk && defEl && speEl;

        const name = pokemon.querySelector('div.name > h4').textContent.trim();

        if (
          Object.keys(allPokemons).includes(name) &&
          hasSpe &&
          !hasExp &&
          !hasThreeStats
        ) {
          allPokemons[name].total += 1;
          if (hasSpe && !hasExp) {
            dupCounter += 1;
            allPokemons[name].dups += 1;
            allPokemons[name].spe += 1;
          } else if (hasSpeAtk && !hasExp) {
            dupCounter += 1;
            allPokemons[name].dups += 1;
            allPokemons[name].speatk += 1;
          } else if (hasSpeDef && !hasExp) {
            dupCounter += 1;
            allPokemons[name].dups += 1;
            allPokemons[name].spedef += 1;
          } else if (hasThreeStats && !hasExp) {
            // dupCounter += 1;
            // allPokemons[name].dups += 1;
            allPokemons[name].ts += 1;
          }
        } else if (hasSpe && !hasExp && !hasThreeStats) {
          allPokemons[name] = {
            total: 1,
            dups: 0,
            ts: 0,
            spe: 0,
            speatk: 0,
            spedef: 0
          };
          if (hasSpe && !hasExp && !hasThreeStats) {
            allPokemons[name].spe = 1;
          } else if (hasSpeAtk && !hasExp) {
            allPokemons[name].speatk = 1;
          } else if (hasSpeDef && !hasExp) {
            allPokemons[name].spedef = 1;
          } else if (hasThreeStats && !hasExp) {
            allPokemons[name].ts = 1;
          }
        }
      }
    }
  };

  const selectPokemons = () => {
    if (pageBackwardUrl !== document.location.href) {
      pageBackwardUrl = document.location.href;
      if (selectedDupCounter >= POKEMONS_TO_SELECT) {
        return false;
      }
      const pokemons = document.querySelectorAll('.container > .row');
      for (const pokemon of pokemons) {
        if (selectedDupCounter >= POKEMONS_TO_SELECT) break;
        const speEl = !!pokemon.querySelector('i.sbtn-spe');
        const atkEl = !!pokemon.querySelector('i.sbtn-atk');
        const defEl = !!pokemon.querySelector('i.sbtn-def');
        const hasAtk =
          !!pokemon.querySelector('i.sbtn-atk') && !(speEl || defEl);
        const hasDef =
          !!pokemon.querySelector('i.sbtn-def') && !(speEl || atkEl);
        const hasSpe =
          !!pokemon.querySelector('i.sbtn-spe') && !(defEl || atkEl);
        const hasAtkDef = hasAtk && defEl && !speEl;
        const hasNoStat = !hasAtk && !defEl && !speEl;
        const hasExp = checkPokemonEXP(pokemon);

        const hasSpeAtk = speEl && atkEl && !defEl;
        const hasSpeDef = speEl && defEl && !atkEl;
        const hasThreeStats = hasAtk && defEl && speEl;

        const name = pokemon.querySelector('div.name > h4').textContent.trim();
        if (
          Object.keys(allPokemons).includes(name) &&
          allPokemons[name].dups > 0 &&
          hasSpe &&
          !hasExp &&
          !hasThreeStats
        ) {
          if (hasSpe && !hasExp) {
            selectedDupCounter += 1;
            allPokemons[name].dups -= 1;
            allPokemons[name].spe -= 1;
            clickPokemon(pokemon, name);
          } else if (hasSpeAtk && !hasExp && allPokemons[name].spe === 0) {
            selectedDupCounter += 1;
            allPokemons[name].dups -= 1;
            allPokemons[name].speatk -= 1;
            clickPokemon(pokemon, name);
          } else if (
            hasSpeDef &&
            !hasExp &&
            allPokemons[name].spe === 0 &&
            allPokemons[name].speatk === 0
          ) {
            selectedDupCounter += 1;
            allPokemons[name].dups -= 1;
            allPokemons[name].spedef -= 1;
            clickPokemon(pokemon, name);
          }
          // else if (
          //   hasThreeStats
          //   && !hasExp
          //   && allPokemons[name].spe === 0
          //   && allPokemons[name].speatk === 0
          //   && allPokemons[name].spedef === 0
          // ) {
          //   selectedDupCounter += 1;
          //   allPokemons[name].dups -= 1;
          //   allPokemons[name].ts -= 1;
          //   clickPokemon(pokemon, name);
          // }
        }
      }
    }
  };

  const pageBackward = (minPage = 1) => {
    deleteFadeover();
    const currPage = Number(
      document.querySelector('#pagesel').textContent.trim()
    );
    const prevPageBtn = document.querySelector(
      '.pagenum.pagebutton i.fa-chevron-left'
    )
      ? document.querySelector('.pagenum.pagebutton i.fa-chevron-left')
      : null;
    if (
      currPage >= minPage &&
      selectedDupCounter < POKEMONS_TO_SELECT &&
      prevPageBtn
    ) {
      let firstPage = false;
      if (
        prevPageBtn &&
        !firstPage &&
        selectedDupCounter < POKEMONS_TO_SELECT
      ) {
        selectPokemons();
        console.log(selectedDupCounter);
        prevPageBtn.click();
      } else if (
        !prevPageBtn &&
        !firstPage &&
        selectedDupCounter <= POKEMONS_TO_SELECT
      ) {
        firstPage = true;
        selectPokemons();
      }
    } else {
      selectPokemons();
      clearInterval(movingBackward);
    }
  };

  let preventFromSearchingAllPages = true;
  const pageForward = (maxPage = 999) => {
    deleteFadeover();
    const currPage = Number(
      document.querySelector('#pagesel').textContent.trim()
    );
    const nextPageBtn = document.querySelector(
      '.pagenum.pagebutton i.fa-chevron-right'
    )
      ? document.querySelector('.pagenum.pagebutton i.fa-chevron-right')
      : null;
    if (dupCounter > 0 && preventFromSearchingAllPages) {
      preventFromSearchingAllPages = false;
      endAtPage = currPage - 1;
    }
    if (currPage < maxPage && dupCounter < POKEMONS_TO_SELECT && nextPageBtn) {
      let lastPage = false;
      if (nextPageBtn && !lastPage && dupCounter < POKEMONS_TO_SELECT) {
        getPokemons();
        console.log(dupCounter);
        nextPageBtn.click();
      } else if (
        !nextPageBtn &&
        !lastPage &&
        dupCounter <= POKEMONS_TO_SELECT
      ) {
        lastPage = true;
        getPokemons();
      }
    } else {
      clearInterval(movingForward);
      removeUniques(allPokemons);
      pageBackward();
      // movingBackward = setInterval(pageBackward, SECONDS, endAtPage);
      movingBackward = setInterval(pageBackward, SECONDS);
    }
  };

  const movingForward = setInterval(pageForward, SECONDS);
}
