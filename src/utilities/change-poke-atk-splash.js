{
  let text = '';
  let changed = 0;
  let toChange = 0;

  const checkPokemonEXP = (pokemon) => {
    const exp = Number(
      pokemon
        .querySelector('.info')
        .textContent.trim()
        .match(/Exp:\s?([\d,]+)/)[1]
        .replaceAll(',', '')
    );
    if (exp >= 25 * 1000000) return true;
    return false;
  };

  const getAttack = async (pokename) => {
    let url = `https://www.delugerpg.com/api/pokedex/info/${pokename}`;

    fetch(url, {
      credentials: 'include',
      headers: {
        'User-Agent':
          'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/119.0',
        Accept: 'application/json, text/javascript, */*; q=0.01',
        'Accept-Language': 'en-US,en;q=0.5',
        'X-Requested-With': 'XMLHttpRequest',
        'Alt-Used': 'www.delugerpg.com',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin'
      },
      referrer: 'https://www.delugerpg.com/',
      method: 'GET',
      mode: 'cors'
    })
      .then((response) => response.json())
      .then((data) => {
        let attacks = data.attack;
        console.log(attacks); // log the attacks to the console
        // you can now use 'attacks' in your code
      });
  };

  const changeAttack = async (id, name) => {
    const res = await fetch(
      'https://www.delugerpg.com/ajax/pokemon/changeattack/confirm',
      {
        credentials: 'include',
        headers: {
          'User-Agent':
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:105.0) Gecko/20100101 Firefox/105.0',
          Accept: '*/*',
          'Accept-Language': 'en-US,en;q=0.5',
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
          'X-Requested-With': 'XMLHttpRequest',
          'Alt-Used': 'www.delugerpg.com',
          'Sec-Fetch-Dest': 'empty',
          'Sec-Fetch-Mode': 'cors',
          'Sec-Fetch-Site': 'same-origin'
        },
        referrer: 'https://www.delugerpg.com/',
        body: `id=${id}&attack1=Splash&attack2=Splash&attack3=Splash&attack4=Splash`,
        method: 'POST',
        mode: 'cors'
      }
    );
    if (res) console.log(`✓ Changed ${++changed} — ${name}`);
  };

  let duration = 2500;

  document.querySelectorAll('.container .row').forEach((pokemon) => {
    let outputted = false;
    let DONE_ONCE = false;
    pokemon.querySelectorAll('.attk > div').forEach((atk) => {
      if (!DONE_ONCE) {
        DONE_ONCE = true;
        const speEl = !!pokemon.querySelector('i.sbtn-spe');
        const atkEl = !!pokemon.querySelector('i.sbtn-atk');
        const defEl = !!pokemon.querySelector('i.sbtn-def');
        const hasNoStat = !speEl && !atkEl && !defEl;
        const hasThreeStat = speEl && atkEl && defEl;
        //////
        const name = pokemon
          .querySelector('.nnfield.editable')
          .textContent.trim();
        ////////////////
        const condition = checkPokemonEXP(pokemon) || hasThreeStat;
        /////////////
        if (atk.textContent.trim() !== 'Splash' && !outputted && condition) {
          // text += `id=${pokemon.id.substring(
          //   4
          // )}&attack1=Splash&attack2=Splash&attack3=Splash&attack4=Splash\n`;
          ++toChange;
          setTimeout(() => {
            changeAttack(pokemon.id.substring(4), name);
            outputted = true;
          }, duration);
          duration += 2500;
        }
      }
    });
  });

  console.log(`➔ To change ${toChange} pokemons.`);
}
