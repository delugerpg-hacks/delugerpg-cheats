// ==UserScript==
// @name         Navigation Enhancements
// @version      0
// @match        https://www.delugerpg.com/*
// @grant        none
// ==/UserScript==

// Convert the NodeList of all input elements on the page to an array.
const inputs = [...document.querySelectorAll('input')];

// Remove focus from any active input or control on the page.
// Useful for cases where an input might automatically gain focus when a page opens.
document.activeElement.blur();

// Define a function to move to the next page when the right arrow key is pressed.
const moveForward = (event) => {
  // Check if the event is not from a composing input (like an IME) and if the pressed key is the right arrow.
  if (event.isComposing || event.code === 'ArrowRight') {
    // Find the forward button on the page using the FontAwesome right chevron icon as an identifier.
    const forwardBtn = document
      .querySelector('.fa-chevron-right')
      .closest('.pagenum.pagebutton');
    // If the forward button exists, simulate a click on it.
    if (forwardBtn) forwardBtn.click();
  }
};

// Define a function to move to the previous page when the left arrow key is pressed.
const moveBackward = (event) => {
  // Check if the event is not from a composing input (like an IME) and if the pressed key is the left arrow.
  if (event.isComposing || event.code === 'ArrowLeft') {
    // Find the backward button on the page using the FontAwesome left chevron icon as an identifier.
    const backwardBtn = document
      .querySelector('.fa-chevron-left')
      .closest('.pagenum.pagebutton');
    // If the backward button exists, simulate a click on it.
    if (backwardBtn) backwardBtn.click();
  }
};

// Define a function to center the main Pokémon container on the screen when the 'f' key is pressed.
const focusContainer = (event) => {
  // Check if the event is not from a composing input and if the pressed key is 'f'.
  // Also, ensure that the currently focused element isn't an input field.
  if (
    (event.isComposing || event.code === 'KeyF') &&
    !inputs.includes(document.activeElement)
  ) {
    // Find the main Pokémon container.
    const container = document.querySelector('#monboxes .container');
    const container2 = document.querySelector('#viewpokes');
    // If the first container is found, smoothly scroll it to the center of the screen.
    if (container)
      container.scrollIntoView({
        behavior: 'smooth',
        block: 'center'
      });
    // If the first container isn't found, but the second one is, smoothly scroll it to the center of the screen.
    else if (container2)
      container2.scrollIntoView({
        behavior: 'smooth',
        block: 'center'
      });
  }
};

// Attach event listeners to the document for keydown events.
// Allow navigation between pages with arrow keys.
document.addEventListener('keydown', moveForward);
document.addEventListener('keydown', moveBackward);
// Allow focusing the Pokémon box by pressing the 'f' key.
document.addEventListener('keydown', focusContainer);
