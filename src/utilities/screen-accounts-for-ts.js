console.clear();
/**
 * Iterate over user ids to find TS
 * This works! found 4-5 TSs this way!
 */

/**
 * the code below will change page to next user ID according to their serial
 */
(function () {
  const name = document.location.href;
  const id = name.split('/');
  if (name.includes('/page/')) {
    const next = Number(id[id.length - 3]) + 1;
    document.location.href =
      'https://www.delugerpg.com/allpokemon/user/' + next;
  } else {
    const next = Number(id[id.length - 1]) + 1;
    document.location.href =
      'https://www.delugerpg.com/allpokemon/user/' + next;
  }
})();

if (document.querySelector('.pages-container > div')) {
  console.log('HAS MORE THAN 1 PAGE!');
  console.warn('HAS MORE THAN 1 PAGE!');
  console.log('HAS MORE THAN 1 PAGE!');
  console.warn('HAS MORE THAN 1 PAGE!');
  console.log('HAS MORE THAN 1 PAGE!');
  console.warn('HAS MORE THAN 1 PAGE!');
  console.log('HAS MORE THAN 1 PAGE!');
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

/**
 * Find all TS on a user's /allpokemons/ page and delete element which aren't so only TS will left
 * GUIDE:- if there happen to be more than one page then change page and only run the fn below
 */

const legends = [
  'Arceus',
  'Articuno',
  'Azelf',
  'Blacephalon',
  'Buzzwole',
  'Calyrex',
  'Celebi',
  'Celesteela',
  'Cobalion',
  'Cosmoem',
  'Cosmog',
  'Cresselia',
  'Darkrai',
  'Deoxys',
  'Dialga',
  'Diancie',
  'Enamorus',
  'Entei',
  'Eternatus',
  'G-Max Melmetal',
  'G-Max Urshifu',
  'Genesect',
  'Giratina',
  'Glastrier',
  'Groudon',
  'Guzzlord',
  'Heatran',
  'Ho-oh',
  'Hoopa',
  'Jirachi',
  'Kartana',
  'Keldeo',
  'Kubfu',
  'Kyogre',
  'Kyurem',
  'Landorus',
  'Latias',
  'Latios',
  'Lugia',
  'Lunala',
  'Magearna',
  'Manaphy',
  'Marshadow',
  'Mega Diancie',
  'Mega Latias',
  'Mega Latios',
  'Mega Mewtwo X',
  'Mega Mewtwo Y',
  'Mega Rayquaza',
  'Melmetal',
  'Meloetta',
  'Meltan',
  'Mesprit',
  'Mew',
  'Mewtwo',
  'Moltres',
  'Naganadel',
  'Necrozma',
  'Nihilego',
  'Palkia',
  'Pheromosa',
  'Phione',
  'Poipole',
  'Primal Groudon',
  'Primal Kyogre',
  'Raikou',
  'Rayquaza',
  'Regice',
  'Regidrago',
  'Regieleki',
  'Regigigas',
  'Regirock',
  'Registeel',
  'Reshiram',
  'Shaymin',
  'Silvally',
  'Solgaleo',
  'Spectrier',
  'Stakataka',
  'Suicune',
  'Tapu Bulu',
  'Tapu Fini',
  'Tapu Koko',
  'Tapu Lele',
  'Terrakion',
  'Thundurus',
  'Tornadus',
  'Type: Null',
  'Urshifu',
  'Uxie',
  'Victini',
  'Virizion',
  'Volcanion',
  'Xerneas',
  'Xurkitree',
  'Yveltal',
  'Zacian',
  'Zamazenta',
  'Zapdos',
  'Zarude',
  'Zekrom',
  'Zeraora',
  'Zygarde'
];

(function () {
  const pokemons = document.querySelectorAll('#viewpokes > div');
  const hasAllStats = (element) => {
    const hasSpe = !!element.querySelector('i.sbtn-spe');
    const hasAtk = !!element.querySelector('i.sbtn-atk');
    const hasDef = !!element.querySelector('i.sbtn-def');
    return hasSpe && hasAtk && hasDef;
  };
  const isDesiredLegend = (element) => {
    let is = false;
    const wantedClass = ['Shiny', 'Retro', 'Chrome'];
    const hasSpe = !!element.querySelector('i.sbtn-spe');
    const name = element.querySelector('.vpname > h4').textContent.trim();
    const fname = name.split(' ')[0];
    legends.forEach((legend) => {
      if (name.includes(legend) && wantedClass.includes(fname) && hasSpe) {
        is = true;
      }
    });
    return is;
  };
  pokemons.forEach((pokemon) => {
    if (!hasAllStats(pokemon) && !isDesiredLegend(pokemon)) {
      pokemon.remove();
    } else if (hasAllStats(pokemon)) {
      console.info(
        `FOUND TRIPLE STAT: ${pokemon
          .querySelector('.vpname > h4')
          .textContent.trim()}`
      );
      console.log('***** TS * FOUND TRIPLE STATs ** TS *****');
      console.warn('***** TS * FOUND TRIPLE STATs ** TS *****');
      console.error('***** TS * FOUND TRIPLE STATs ** TS *****');
      console.log('***** TS * FOUND TRIPLE STATs ** TS *****');
      console.log('***** TS * FOUND TRIPLE STATs ** TS *****');
      console.log('***** TS * FOUND TRIPLE STATs ** TS *****');
      console.warn('***** TS * FOUND TRIPLE STATs ** TS *****');
      console.error('***** TS * FOUND TRIPLE STATs ** TS *****');
      console.warn('***** TS * FOUND TRIPLE STATs ** TS *****');
      console.error('***** TS * FOUND TRIPLE STATs ** TS *****');
      console.warn('***** TS * FOUND TRIPLE STATs ** TS *****');
      console.error('***** TS * FOUND TRIPLE STATs ** TS *****');
    } else if (isDesiredLegend(pokemon)) {
      console.info(
        `FOUND SPE LEGEND: ${pokemon
          .querySelector('.vpname > h4')
          .textContent.trim()}`
      );
      console.log('=== DUDE GOT +SPE SHINY/CHROME/RETRO LEGGY ===');
      console.warn('=== DUDE GOT +SPE SHINY/CHROME/RETRO LEGGY ===');
      console.log('=== DUDE GOT +SPE SHINY/CHROME/RETRO LEGGY ===');
      console.warn('=== DUDE GOT +SPE SHINY/CHROME/RETRO LEGGY ===');
      console.log('=== DUDE GOT +SPE SHINY/CHROME/RETRO LEGGY ===');
      console.warn('=== DUDE GOT +SPE SHINY/CHROME/RETRO LEGGY ===');
    } else {
      console.error('EDGE CASE!');
    }
  });
})();
