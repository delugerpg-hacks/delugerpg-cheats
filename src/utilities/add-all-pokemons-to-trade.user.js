// ==UserScript==
// @name         Add All Pokemons to Trade
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.delugerpg.com/multiselect/+trade
// @icon         https://www.google.com/s2/favicons?sz=64&domain=delugerpg.com
// @grant        none
// ==/UserScript==

function selectAll() {
  {
    const selectedMons = {};
    let clickCounter = 0;
    const TO_SELECT = 250; // keep it more than the selection to acc for errors
    const SELECT_LIMIT = TO_SELECT;
    const DELAY = 1200;

    let TRIGGER = false;

    const doDuplicatePokemons = () => {
      const getPokemons = document.querySelectorAll('.container > .row');
      getPokemons.forEach((pokemon) => {
        if (clickCounter >= SELECT_LIMIT) {
          clearInterval(goingForward);
          setTimeout(byeConfirm, 2000);
        }
        if (document.querySelector('.modal-backdrop.fade')) {
          document.querySelector('.modal-backdrop.fade').remove();
        }

        const condition = true;
        if (condition) {
          pokemon.click();
          clickCounter += 1;
        }
      });
    };

    const forward = (page = 999) => {
      try {
        if (document.querySelector('.modal-backdrop.fade')) {
          document.querySelector('.modal-backdrop.fade').remove();
        }
        const currentPage = document.querySelector('#pagesel')
          ? document.querySelector('#pagesel').textContent.trim()
          : null;
        if (
          document.querySelector('.pagenum.pagebutton i.fa-chevron-right') &&
          currentPage < page &&
          !TRIGGER
        ) {
          doDuplicatePokemons();
          if (clickCounter === TO_SELECT) TRIGGER = true;
          document
            .querySelector('.pagenum.pagebutton i.fa-chevron-right')
            .click();
        } else {
          clearInterval(goingForward);
          doDuplicatePokemons();
          console.clear();
        }
      } catch (e) {
        clearInterval(goingForward);
        console.log(e);
      }
    };

    const goingForward = setInterval(forward, DELAY);
  }
}

function byeConfirm() {
  (function () {
    document.querySelector('#msbuttoncount').click();
    setTimeout(() => {
      document.querySelector('#confirm-select').click();
    }, 2500);
  })();
}

(function () {
  'use strict';
  if (
    document.querySelector('.notify_done > .btn') &&
    document.querySelector('.notify_done > .btn').textContent.trim() ===
      'Return to Pokemon Page'
  ) {
    document.location.href = 'https://www.delugerpg.com/multiselect/+trade';
  } else {
    selectAll();
  }
})();
