// ==UserScript==
// @name         DelugeRPG - Trade Enhancer
// @namespace    DelugeRPG_Trade_Enhancements
// @description  Enhancements for the Pokémon trading platform on delugerpg.com
// @match        https://www.delugerpg.com/trade/lookup/pokemon/*
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM_deleteValue
// ==/UserScript==

(function () {
  'use strict';

  // Closes the window if no Pokémon are listed
  if (document.querySelectorAll('#monboxes > .container > *').length === 0) {
    window.close();
    return;
  }

  // Opens all pages of trades in new tabs
  if (!document.location.href.includes('/page')) {
    const pages = [...document.querySelectorAll('.pagenum')];
    const lastPageNumber =
      pages.length > 2 ? Number(pages[pages.length - 2].textContent.trim()) : 1;
    for (let i = 2; i <= lastPageNumber; i++) {
      window.open(`${document.location.href}/page/${i}`, '_blank');
    }
  }

  // Converts the user's last login date to UTC and checks if it's within X days ago
  function isWithinRange(loginDate, daysAgo) {
    const today = new Date();
    const pastDate = new Date(today.setDate(today.getDate() - daysAgo));
    const userDate = new Date(loginDate);

    // Remove timezone offset to compare just the date parts
    userDate.setMinutes(userDate.getMinutes() - userDate.getTimezoneOffset());
    pastDate.setMinutes(pastDate.getMinutes() - pastDate.getTimezoneOffset());

    return userDate <= pastDate;
  }

  // Fetch user details from the DelugeRPG API
  async function getUserDetails(userId) {
    const response = await fetch(
      `https://www.delugerpg.com/ajax/profile/uid/${userId}`
    );
    const data = await response.json();
    return {
      banned: Boolean(data.status),
      id: Number(data.profileid),
      user: data.username,
      date: isWithinRange(data.lastonline, 2) // Additional user details can be added here
    };
  }

  // Iterate through the Pokémon listings and process according to user status
  async function processListings() {
    const pokemons = document.querySelectorAll('#monboxes > .container > *');
    const blacklist = JSON.parse(GM_getValue('blacklist', '{}'));
    const whitelist = JSON.parse(GM_getValue('whitelist', '{}'));
    const temporary = JSON.parse(GM_getValue('temporary', '{}'));

    for (const poke of pokemons) {
      const userId = poke.querySelector('.username').id.slice(2);
      if (!blacklist[userId]) {
        const userDetails = await getUserDetails(userId);
        if (userDetails && !userDetails.banned && userDetails.date) {
          whitelist[userId] = userDetails.user;
          GM_setValue('whitelist', JSON.stringify(whitelist));
          poke.remove();
        } else if (userDetails.banned) {
          blacklist[userId] = userDetails.user;
          GM_setValue('blacklist', JSON.stringify(blacklist));
        } else {
          temporary[userId] = userDetails.user;
          GM_setValue('temporary', JSON.stringify(temporary));
        }
      } else {
        poke.remove();
      }
    }

    // Close the window if no Pokémon are left after processing
    if (document.querySelectorAll('#monboxes > .container > *').length === 0) {
      window.close();
    }
  }

  // Run the process when the script loads
  processListings();
})();
