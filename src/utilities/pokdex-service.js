// ==UserScript==
// @name            Pokedex Service
// @namespace       Simply find the pokemons the user to whole dex is compared doesnt have
// @match           https://www.delugerpg.com/compare/pokedex/*
// @grant           GM_setValue
// @grant           GM_getValue
// @grant           GM_deleteValue
// ==/UserScript==

/**
 * Compare pokedex to another person and list the pokes he doesnt have but u do
 * and lists them
 */

// (Chrome|Shadow|Negative|Mirage|Shiny|Retro|Ghostly|Metallic|Dark).*?$
// Without line breaks:
// \s?(Chrome|Shadow|Negative|Mirage|Shiny|Retro|Ghostly|Metallic|Dark).*?,

(async function () {
  const prevStore = await GM_getValue('pokestore', '');
  let placeholder = prevStore;

  const main = setInterval(async () => {
    try {
      if (document.querySelector('.pagenum.pagebutton i.fa-chevron-right')) {
        placeholder += find();
        await GM_setValue('pokestore', placeholder);
        setTimeout(
          document
            .querySelector('.pagenum.pagebutton i.fa-chevron-right')
            .click(),
          1500
        );
      } else {
        clearInterval(main);
        document.write(prevStore);
        await GM_deleteValue('pokestore');
      }
    } catch (e) {
      clearInterval(main);
      await GM_deleteValue('pokestore');
    }
  }, 2500);

  const find = function () {
    const pokeIHaveUserDoesnt = '.dc1y2n';
    const pokeBothDoesntHave = '.dc1n2n';
    const connected = document.querySelectorAll(
      `${pokeIHaveUserDoesnt},${pokeBothDoesntHave}`
    );
    console.log(connected.length);
    if (connected.length < 1) {
      console.log('1');
      return false;
    }

    const getClassName = (val) => {
      switch (val) {
        case 1:
          return '';
        case 2:
          return 'Shiny';
        case 3:
          return 'Metallic';
        case 4:
          return 'Ghostly';
        case 5:
          return 'Dark';
        case 6:
          return 'Shadow';
        case 7:
          return 'Mirage';
        case 8:
          return 'Chrome';
        case 9:
          return 'Negative';
        case 10:
          return 'Retro';
      }
    };
    let counter = 0;
    let store = {};
    for (let i = 2; i < connected.length; i++) {
      // PS i = 2 as first 2 are description of the pokeballs
      const row = connected[i].closest('.dexrow');
      const classes = row.querySelectorAll('.types');
      let classCounter = 0;
      classes.forEach((type) => {
        classCounter += 1;
        if (
          type.querySelector(pokeIHaveUserDoesnt) ||
          type.querySelector(pokeBothDoesntHave)
        ) {
          counter += 1;
          store[
            `${getClassName(classCounter)} ${row
              .querySelector('.pokename')
              .textContent.trim()}`
          ] = true;
        }
      });
    }

    let temp = '';
    Object.keys(store).forEach((val) => {
      temp += val + ', ';
    });
    return temp;
  };
})();
