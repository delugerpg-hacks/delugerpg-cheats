// ==UserScript==
// @name         Change Attack
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://m.delugerpg.com/team
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        none
// ==/UserScript==

(function () {
  'use strict';

  // Your code here..
  let changed = 0;
  let toChange = 0;

  const attacks = [
    // 'Subzero+Slammer',
    'Inferno+Overdrive',
    'V-create',
    'V-create',
    'V-create'
    // 'Prismatic+Laser',
    // 'Focus+Punch',
    // 'Freeze+Shock',
    // 'Roar+Of+Time'
  ];

  const changeAttack = async (id, name) => {
    const res = await fetch(
      'https://m.delugerpg.com/ajax/pokemon/changeattack/confirm',
      {
        credentials: 'include',
        headers: {
          'User-Agent':
            'Mozilla/5.0 (Android 12; Mobile; rv:109.0) Gecko/109.0 Firefox/109.0',
          Accept: '*/*',
          'Accept-Language': 'en-US,en;q=0.5',
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
          'X-Requested-With': 'XMLHttpRequest',
          'Alt-Used': 'm.delugerpg.com',
          'Sec-Fetch-Dest': 'empty',
          'Sec-Fetch-Mode': 'cors',
          'Sec-Fetch-Site': 'same-origin'
        },
        referrer: 'https://m.delugerpg.com/',
        body: `id=${id}&attack1=${attacks[0]}&attack2=${attacks[1]}&attack3=${attacks[2]}&attack4=${attacks[3]}`,
        method: 'POST',
        mode: 'cors'
      }
    );
    if (res) console.log(`✓ Changed ${++changed} — ${name}`);
  };

  let duration = 2500;

  document.querySelectorAll('[id^="mem_"]').forEach((pokemon) => {
    ++toChange;
    const name = pokemon.querySelector('.name').textContent.trim();
    const id = pokemon.querySelector('.name').id.substring(4);
    setTimeout(() => {
      changeAttack(id, name);
      outputted = true;
    }, duration);
    duration += 2500;
  });

  console.log(`➔ To change ${toChange} pokemons.`);
})();
