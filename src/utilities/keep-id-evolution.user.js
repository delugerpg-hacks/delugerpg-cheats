// ==UserScript==
// @name         KEEP ID Evolution
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.delugerpg.com/pokemon/evolve/*
// @match        https://www.delugerpg.com/pokemon/evolve/changeattacks
// @grant        none
// ==/UserScript==

(function () {
  'use strict';

  // Adjust this delay multiplier as needed (e.g., 1000 milliseconds = 1 second)
  const delayMultiplier = 100;

  function main() {
    // Add a delay at the beginning
    setTimeout(function () {
      // Step 1: Check if .infobox > h2 contains specific text
      const infoboxHeader = document.querySelector('.infobox > h2');

      // Step 2: Check if .imgtolevel is there and has the correct src
      const imgToLevel = document.querySelector('.imgtolevel');

      if (
        (!imgToLevel ||
          imgToLevel.getAttribute('src') !==
            'https://i.dstatic.com/images/pokeball-n.png') &&
        (!infoboxHeader ||
          !infoboxHeader.textContent.includes('FOUR attacks for the evolved'))
      ) {
        window.close();
      } else {
        // Step 3: Check if the src is the pokeball image
        if (
          !imgToLevel ||
          imgToLevel.getAttribute('src') !==
            'https://i.dstatic.com/images/pokeball-n.png' ||
          !infoboxHeader ||
          !infoboxHeader.textContent.includes('FOUR attacks for the evolved')
        ) {
          // Step 4: Find the Evolve button
          const evolveButton = document.querySelector(
            'input.btn.btn-primary[value="Evolve"]'
          );
          if (evolveButton && !evolveButton.hasAttribute('disabled')) {
            // Step 5: Click the Evolve button
            evolveButton.click();

            // Delay before finding the second Evolve button
            setTimeout(function () {
              // Step 6: Find the second Evolve button
              const secondEvolveButton = document.querySelector(
                'input.btn.btn-primary[value="Evolve"]'
              );
              if (
                secondEvolveButton &&
                !secondEvolveButton.hasAttribute('disabled')
              ) {
                // Step 7: Click the second Evolve button
                secondEvolveButton.click();

                // Delay before finding and checking the .notify_done element
                setTimeout(function () {
                  // Step 8: Find and check the .notify_done element
                  const notifyDone = document.querySelector('.notify_done');
                  if (
                    notifyDone &&
                    notifyDone.textContent.includes('evolved into')
                  ) {
                    window.close();
                  }
                }, 2 * delayMultiplier); // Wait for 2 seconds before checking .notify_done
              }
            }, 2 * delayMultiplier); // Wait for 2 seconds before checking the second Evolve button
          } else {
            window.close();
          }
        }
      }
    }, 5 * delayMultiplier); // Wait for 5 seconds at the beginning
  }

  main();
})();
