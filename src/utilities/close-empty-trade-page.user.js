// ==UserScript==
// @name            Close Empty Trades
// @namespace       Simply find the pokemons the user to whole dex is compared doesnt have
// @match           https://www.delugerpg.com/trade/lookup/pokemon/*
// @grant           GM_setValue
// @grant           GM_getValue
// @grant           GM_deleteValue
// ==/UserScript==

// Base URL for the website.
const BASE_URL = 'https://www.delugerpg.com';

// Helper function: A wrapper around querySelectorAll for shorter usage.
function querySelectorAll(selector) {
  return document.querySelectorAll(selector);
}

// Function to close the window if there are no Pokémon elements.
function closeIfEmpty() {
  if (!querySelectorAll('#monboxes > .container > *').length) window.close();
}

// Function to open all trade pages if not on a specific page and pagination exists.
function openAllTrades() {
  const isOnPage = document.location.href.includes('/page');
  const pages = querySelectorAll('.pagenum');

  if (!isOnPage && pages.length > 0) {
    const lastPage = Number(pages[pages.length - 2].textContent.trim());
    for (let i = 2; i <= lastPage; i++) {
      window.open(`${document.location.href}/page/${i}`, '_blank');
    }
  }
}

// Function to sanitize numerical values given different formats.
function sanitizeNumber(value, multiplier = 1) {
  if (value.includes('k')) {
    return parseInt(value) * 1000;
  } else if (value.includes('B')) {
    return parseInt(value) * 1000000000;
  } else if (value.includes('M')) {
    return parseInt(value) * 1000000;
  } else {
    return parseInt(value.replaceAll(',', '')) * multiplier;
  }
}

// Function to fetch user details based on the provided ID.
async function fetchUserDetails(id) {
  const response = await fetch(`${BASE_URL}/ajax/profile/uid/${id}`);
  const data = await response.json();
  return {
    banned: Boolean(data.status), // Check if user is banned.
    id: Number(data.profileid),
    user: data.username,
    battles:
      sanitizeNumber(data.bwins) +
      (data.blosses ? sanitizeNumber(data.blosses) : 0),
    exp: sanitizeNumber(data.ttlexp),
    alt: Boolean(Number(data.spare)), // Check if user is an alternate account.
    join: removeTimezoneOffset(new Date(data.joindate)).toISOString(),
    online: removeTimezoneOffset(new Date(data.lastonline)).toISOString()
  };
}

// Function to adjust the date by removing the timezone offset.
function removeTimezoneOffset(date) {
  const userTimezoneOffset = date.getTimezoneOffset() * 60000;
  return new Date(date.getTime() - userTimezoneOffset);
}

// Main function to check and categorize Pokémon users as 'pro' or 'noob'.
async function noobCheck() {
  const pokemons = querySelectorAll('#monboxes > .container > *');
  const pro = await JSON.parse(GM_getValue('pro', JSON.stringify({})));
  const noob = await JSON.parse(GM_getValue('noob', JSON.stringify({})));
  const temp = await JSON.parse(GM_getValue('temp', JSON.stringify({})));

  await GM_getValue('firstTemp', new Date().toISOString());
  await GM_setValue('lastTemp', new Date().toISOString());

  for (const poke of pokemons) {
    const user = poke.querySelector('.username').textContent.trim();
    const userId = poke.querySelector('.username').id.substring(2);
    if (pro[userId]) {
      poke.remove();
    } else if (!pro[userId] && !temp[userId]) {
      const { snipe, newb } = await isSnipeable(userId);
      if (!snipe) {
        pro[userId] = user;
        noob[userId] = newb ? user : null;
        await GM_setValue('pro', JSON.stringify(pro));
        await GM_setValue('noob', JSON.stringify(noob));
        poke.remove();
      } else {
        temp[userId] = user;
        await GM_setValue('temp', JSON.stringify(temp));
      }
    }
    closeIfEmpty();
    console.log(user, userId);
  }
}

// Function to determine if a user's Pokémon is snipeable and if the user is a 'newb'.
async function isSnipeable(id) {
  const res = await fetchUserDetails(id);
  const DAYS_INACTIVE = 4;
  const lastOnlineMark = new Date(
    new Date().setDate(new Date().getDate() - DAYS_INACTIVE)
  );

  let snipe = true;
  let newb =
    res.battles <= 1000 && res.exp <= 25 * 1000000 && !res.alt && !res.banned;

  if (new Date(res.online).getTime() < lastOnlineMark.getTime() || !newb) {
    snipe = false;
  }

  return { snipe, newb };
}

// Initiate functions.
closeIfEmpty();
openAllTrades();
noobCheck();
