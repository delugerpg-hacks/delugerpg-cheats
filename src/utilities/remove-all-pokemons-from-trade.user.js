// ==UserScript==
// @name         Remove All Pokemons From Trade
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.delugerpg.com/multiselect/-trade
// @icon         https://www.google.com/s2/favicons?sz=64&domain=delugerpg.com
// @grant        none
// ==/UserScript==

function selectAll() {
  {
    const selectedMons = {};
    let clickCounter = 0;
    const TO_SELECT = 250; // keep it more than the selection to acc for errors
    const SELECT_LIMIT = TO_SELECT;
    const DELAY = 1200;

    const checkForExp = (element) => {
      const exp = Number(
        element
          .querySelector('.info')
          .textContent.trim()
          .match(/Exp:\s?([\d,]+)/)[1]
          .replaceAll(',', '')
      );
      if (exp >= 1000000) {
        return true;
      } else {
        return false;
      }
    };

    const checkForHundredLevel = (element) => {
      const level = Number(
        element
          .querySelector('.info')
          .textContent.trim()
          .match(/Level:\s?([\d,]+)/)[1]
          .replaceAll(',', '')
      );
      if (level === 100) {
        return true;
      } else {
        return false;
      }
    };

    const selectThisClass = (name, spirit) => {
      const SPIRIT_LIST = [
        'Normal',
        'Shiny',
        'Metallic',
        'Ghostly',
        'Dark',
        'Shadow',
        'Mirage',
        'Chrome',
        'Negative',
        'Retro'
      ];

      const firstWord = name.split(' ')[0];

      if (!SPIRIT_LIST.includes(firstWord)) name = 'Normal ' + name;

      if (Array.isArray(spirit) && !spirit.includes(name.split(' ')[0]))
        return false;
      return true;
    };

    // prettier-ignore
    const normalLegends = ["Articuno", "Zapdos", "Moltres", "Mewtwo", "Mega Mewtwo X", "Mega Mewtwo Y", "Raikou", "Entei", "Suicune", "Lugia", "Ho-oh", "Regirock", "Regice", "Registeel", "Latias", "Mega Latias", "Latios", "Mega Latios", "Kyogre", "Primal Kyogre", "Groudon", "Primal Groudon", "Rayquaza", "Mega Rayquaza", "Uxie", "Mesprit", "Azelf", "Dialga", "Palkia", "Heatran", "Regigigas", "Giratina", "Cresselia", "Cobalion", "Terrakion", "Virizion", "Tornadus", "Thundurus", "Reshiram", "Zekrom", "Landorus", "Kyurem", "Xerneas", "Yveltal", "Zygarde", "Type: Null", "Silvally", "Tapu Koko", "Tapu Lele", "Tapu Bulu", "Tapu Fini", "Cosmog", "Cosmoem", "Solgaleo", "Lunala", "Necrozma", "Zacian", "Zamazenta", "Eternatus", "Kubfu", "Urshifu", "G-Max Urshifu", "Regieleki", "Regidrago", "Glastrier", "Spectrier", "Calyrex", "Enamorus", "Wo-Chien", "Chien-Pao", "Ting-Lu", "Chi-Yu", "Koraidon", "Miraidon"];
    // prettier-ignore
    const mythicalLegends = ["Mew", "Celebi", "Jirachi", "Deoxys", "Phione", "Manaphy", "Darkrai", "Shaymin", "Arceus", "Victini", "Keldeo", "Meloetta", "Genesect", "Diancie", "Mega Diancie", "Hoopa", "Volcanion", "Magearna", "Marshadow", "Zeraora", "Meltan", "Melmetal", "G-Max Melmetal", "Zarude", ]
    // prettier-ignore
    const ultrabeastLegends = ["Nihilego", "Buzzwole", "Pheromosa", "Xurkitree", "Celesteela", "Kartana", "Guzzlord", "Poipole", "Naganadel", "Stakataka", "Blacephalon", ];
    // prettier-ignore
    const paradoxLegends = ["Great Tusk", "Scream Tail", "Brute Bonnet", "Flutter Mane", "Slither Wing", "Sandy Shocks", "Iron Treads", "Iron Bundle", "Iron Hands", "Iron Jugulis", "Iron Moth", "Iron Thorns", "Roaring Moon", "Iron Valiant", "Koraidon", "Miraidon", "Walking Wake", "Iron Leaves", ];
    const legends = [
      ...normalLegends,
      ...mythicalLegends
      // ...ultrabeastLegends,
      // ...paradoxLegends,
    ];

    let TRIGGER = false;

    const doDuplicatePokemons = () => {
      const getPokemons = document.querySelectorAll('.container > .row');
      getPokemons.forEach((pokemon) => {
        if (clickCounter >= SELECT_LIMIT) {
          clearInterval(goingForward);
          setTimeout(byeConfirm, 2000);
        }
        if (document.querySelector('.modal-backdrop.fade')) {
          document.querySelector('.modal-backdrop.fade').remove();
        }

        const speEl = !!pokemon.querySelector('i.sbtn-spe');
        const atkEl = !!pokemon.querySelector('i.sbtn-atk');
        const defEl = !!pokemon.querySelector('i.sbtn-def');
        const hasExp = checkForExp(pokemon);
        const isLevelHundred = checkForHundredLevel(pokemon);
        const hasNoStat = !speEl && !atkEl && !defEl;
        const hasThreeStat = speEl && atkEl && defEl;
        const hasStat = speEl || atkEl || defEl;
        const doesntHave = !!pokemon.querySelector('i.not');
        const name = pokemon.querySelector('.name').textContent.trim();
        const isLegend = () => {
          for (const legend of legends) {
            if (name.includes(legend)) return true;
          }
          return false;
        };
        const isRetroLegend = () => {
          const wantedSpirites = ['Retro']; // probability: 5 1 5 5 5 10 10 10 10
          for (const legend of legends) {
            if (name.includes(legend) && name.includes(wantedSpirites[0]))
              return true;
          }
          return false;
        };

        const isSelectedMultiple = (selectedXtimes = 0, poke) => {
          if (selectedXtimes >= poke) return false;
          return true;
        };

        if (selectedMons.hasOwnProperty(name)) selectedMons[name] += 1;
        else selectedMons[name] = 1;

        const condition = true;
        if (condition) {
          pokemon.click();
          clickCounter += 1;
        }
      });
    };

    const forward = (page = 999) => {
      try {
        if (document.querySelector('.modal-backdrop.fade')) {
          document.querySelector('.modal-backdrop.fade').remove();
        }
        const currentPage = document.querySelector('#pagesel')
          ? document.querySelector('#pagesel').textContent.trim()
          : null;
        if (
          document.querySelector('.pagenum.pagebutton i.fa-chevron-right') &&
          currentPage < page &&
          !TRIGGER
        ) {
          doDuplicatePokemons();
          if (clickCounter === TO_SELECT) TRIGGER = true;
          document
            .querySelector('.pagenum.pagebutton i.fa-chevron-right')
            .click();
        } else {
          clearInterval(goingForward);
          doDuplicatePokemons();
          // console.clear();
        }
      } catch (e) {
        clearInterval(goingForward);
        // console.log(e);
      }
    };

    const goingForward = setInterval(forward, DELAY);
  }
}

function byeConfirm() {
  (function () {
    document.querySelector('#msbuttoncount').click();
    setTimeout(() => {
      document.querySelector('#confirm-select').click();
    }, 2500);
  })();
}

(function () {
  'use strict';
  if (
    document.querySelector('.notify_done > .btn') &&
    document.querySelector('.notify_done > .btn').textContent.trim() ===
      'Return to Trade Page'
  ) {
    document.location.href = 'https://www.delugerpg.com/multiselect/-trade';
  } else {
    selectAll();
  }
})();
