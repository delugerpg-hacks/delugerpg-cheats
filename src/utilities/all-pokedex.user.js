// ==UserScript==
// @name         All pokedex
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.delugerpg.com/pokede*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=delugerpg.com
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM_deleteValue
// ==/UserScript==

(async function () {
  'use strict';
  const old = JSON.parse(await GM_getValue('pokes', JSON.stringify([])));
  const row = document.querySelectorAll('.dexrow');
  let temp = [];
  row.forEach((poke) => {
    const name = poke.querySelector('.pokename').textContent.trim();
    temp.push(name);
  });
  GM_setValue('pokes', JSON.stringify([...old, ...temp])).then((r) => {
    console.log('ssddf', r);
    window.close();
  });
  console.info('Doing...');
})();
