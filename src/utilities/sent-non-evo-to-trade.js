{
  // Headers used for the fetch request to the trade API
  const FETCH_HEADERS = {
    'User-Agent':
      'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:106.0) Gecko/20100101 Firefox/106.0',
    Accept: 'application/json, text/javascript, */*; q=0.01',
    'Accept-Language': 'en-US,en;q=0.5',
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'X-Requested-With': 'XMLHttpRequest',
    'Sec-Fetch-Dest': 'empty',
    'Sec-Fetch-Mode': 'cors',
    'Sec-Fetch-Site': 'same-origin',
    'Sec-GPC': '1'
  };

  /**
   * Sends a trade request for a given Pokemon ID
   * @param {string} id - The Pokemon ID
   * @returns {Promise} - The fetch promise
   */
  function did(id) {
    return fetch('https://www.delugerpg.com/ajax/pokemon/trade', {
      credentials: 'include',
      headers: FETCH_HEADERS,
      referrer: 'https://www.delugerpg.com/',
      body: `id=${id}`,
      method: 'POST',
      mode: 'cors'
    }).catch((error) => {
      console.error('Error sending trade:', error);
    });
  }

  /**
   * Sends trade requests for each Pokemon on the page that doesn't have the 'evolve' class
   */
  function send() {
    // Select all Pokemon elements on the page
    const pokes = document.querySelectorAll('.container > *');
    let delay = 4000; // Initial delay before the first trade request is sent

    pokes.forEach((poke) => {
      // Check if the Pokemon can evolve
      const hasEvolve = !!poke.querySelector('.evolve');
      // Extract Pokemon ID from the element's ID
      const iid = poke.id.substring(4);
      if (!hasEvolve) {
        setTimeout(() => {
          did(iid);
        }, delay);
        delay += 2000; // Increase the delay for the next trade request
      }
    });
  }

  // Flag to check if the script should continue running on the next page
  let runOnceOnLastPageFlag = true;

  /**
   * Main function that sends trade requests and navigates to the next page
   */
  function fn() {
    // Check if there's a button to go to the next page
    const nextPageButton = document.querySelector(
      '.pagenum.pagebutton i.fa-chevron-right'
    );
    const hasNextButton = !!nextPageButton;

    if (runOnceOnLastPageFlag) {
      send();

      setTimeout(() => {
        try {
          nextPageButton.click();
        } catch (e) {
          // Stop the script if there's an error (likely because there's no next page)
          runOnceOnLastPageFlag = false;
          clearInterval(looper);
        }
      }, 2000);
    } else if (!hasNextButton && runOnceOnLastPageFlag) {
      runOnceOnLastPageFlag = false;
      fn();
      clearInterval(looper);
    }
  }

  // Run the main function initially
  fn();

  // Set an interval to continue running the main function every 3 seconds
  const looper = setInterval(fn, 3000);
}
