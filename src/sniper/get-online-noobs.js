// ==UserScript==
// @name         Snipe online
// @namespace    snipe.online
// @version      1.0.0
// @match        https://*.delugerpg.com/online*
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM_deleteValue
// ==/UserScript==

const blacklist = JSON.parse(
  // prettier-ignore
  await GM_getValue('blacklist', JSON.stringify({"eeeeemman": true, "Bladefury_": true, "Renato2020": true, "Nick32881": true, "Punisher": true, "Ushijima.Wakatoshi": true, "Pugs": true, "Aung": true, "AlanGaribaldi": true, "jeiws2": true, "Lucanja": true, "Ifrit_009": true, "hirai_momo45": true, "Brock102030": true, "pancracio": true, "1003753753": true, "LAXUS_DREYAR": true, "Lace": true, "Andylin257": true, "_LOBO_": true, "GinCombi": true, "wali": true, "No_Trading": true, "Jumprz": true, "mrklowee": true, "Yozora.": true, "Yeat": true, "amaterasi": true, "Zenoking": true, "re4p3r": true, "rodel2929": true, "PAINFUL": true, "Yunnix2": true, "Hoffman42": true, "DrKaZe": true, "bananakid51": true, "zeroXDpunk": true, "oily": true, "Thirdy2222": true, "makiziggs": true, "Larishu": true, "SIGNAL": true, "heartless666": true, "dat": true, "Lantis": true, "NightBR": true, "marsrevilo": true, "realtg2thenext": true, "m1ld": true, "xermangame": true, "948119123": true}))
);
const whitelist = JSON.parse(
  // prettier-ignore
  await GM_getValue('whitelist', JSON.stringify({ "RatoBabaca": true, "Yuri10": true, "Accer": true, "BigBlankHayden": true, "OkitaSouji": true, "OswaldoV": true, "anime_fan60": true, "plants12": true, "Bheep70": true, "khenmasterz": true, "XxJunior11": true, "Diogofranco1": true, "Aryan_Dev": true, "K1ng_6a1ch": true, "CMR": true, "kevinzach": true, "theZucchini2020": true, "visa123": true, "heydude22": true, "Override": true, "Catr": true, "3301jm": true, "emmantriku_15": true, "ithallo1111": true, "Kylepog0101": true, "klintona": true, "Gnomiopequeno": true, "paulabapo": true, "Karma-.": true, "SpoiledGee": true, "Oromis": true, "Jacks10": true, "H5z3r": true, "victorawaken": true, "Olalo": true, "JustDoNot": true, "Omior": true, "unknown_guy": true, "HydraSage8113": true, "PokeDeex": true, "NoxNevermore": true, "hsn_dve": true, "pancho2.o0": true, "Hego_she": true, "HMr_stuffH": true, "good723": true, "Fabimm": true, "Roseannnavales028": true, "sarion55": true, "Babar": true, "Tellonthem_13": true, "Shakier": true, "jomerrr": true, "jokenz": true, "tommmy1234": true, "UnicornMermaid": true, "Tboy_8": true, "KingMasterVoltsy": true, "Kevynzxk": true, "NickWRLD999": true, "hawk99": true, "Bernas": true, "nikzo": true, "cediefoxxy": true, "Redemption": true, "EL_XD_SUPREME": true, "ByakkoTG": true, "Samo1219": true, "phobiaspace": true, "Shidou23": true, "bulbisaur": true, "etherian": true, "pescley159": true, "MrClover": true, "khenmasterzzz": true}))
);

const addToFound = [];
const prevBlacklistLength = Object.keys(blacklist).length;
const prevFoundLength = Object.keys(whitelist).length;

// const openNewBackgroundTab = (url) => {
//   var a = document.createElement('a');
//   a.href = url;
//   var evt = document.createEvent('MouseEvents');
//   //the tenth parameter of initMouseEvent sets ctrl key
//   evt.initMouseEvent(
//     'click',
//     true,
//     true,
//     window,
//     0,
//     0,
//     0,
//     0,
//     0,
//     true,
//     false,
//     false,
//     false,
//     0,
//     null
//   );
//   a.dispatchEvent(evt);
// };

const getDetails = (url, user) => {
  const domUsername = user.textContent.trim();
  if (!blacklist[domUsername] && !whitelist[domUsername]) {
    return new Promise(async (resolve) => {
      fetch(url)
        .then((response) => response.json())
        .then((data) =>
          resolve({
            id: Number(data.profileid),
            user: data.username,
            battles: Number(data.bwins) + Number(data.blosses),
            exp: Number(data.ttlexp.replaceAll(',', '')),
            alt: !!Number(data.spare),
            online: Boolean(data.uonline)
          })
        );
    });
  }
  return false;
};

const isNoob = ({ battles, exp, alt }) =>
  battles < 1000 && exp < 20000000 && !alt;

let STOP = 0;
let LOL = 0;
let duration = 0;

const link = 'https://www.delugerpg.com/ajax/profile/uid/';
const check = () => {
  const users = document.querySelectorAll('.username');
  for (const user of users) {
    if (STOP > 20) {
      break;
    }
    const domUsername = user.textContent.trim();

    if (!blacklist[domUsername] && !whitelist[domUsername]) {
      console.log('doing');

      STOP += 1;
      const interval = (Math.random() * (10 + 7) - 7) * 1000;
      duration += interval;
      setTimeout(async () => {
        const userId = user.id.substring(2);
        const url = link + userId;
        const userData = await getDetails(url, user);

        if (userData) {
          const noob = isNoob(userData);
          if (noob) {
            const urlTO = `https://www.delugerpg.com/allpokemon/user/${userData.id}/filter/@defspe`;
            console.info(
              ++LOL,
              'crx',
              domUsername,
              Math.round(Math.random() * 1000 + 1),
              urlTO
            );
            whitelist[userData.user] = true;
            window.open(urlTO, '_blank');
            // openNewBackgroundTab(urlTO);
          } else {
            blacklist[userData.user] = true;
          }
          if (Object.keys(blacklist).length > prevBlacklistLength) {
            const res = await GM_setValue(
              'blacklist',
              JSON.stringify(blacklist)
            );
            res ? console.log('BLACKLIST', blacklist) : null;
            // : console.error('BLACKLIST', userData.user);
          }
          if (Object.keys(whitelist).length > prevFoundLength) {
            const res = await GM_setValue(
              'whitelist',
              JSON.stringify(whitelist)
            );
            res ? console.log('WHITELIST', whitelist) : null;
            //console.error('WHITELIST', userData.user);
          }
        }
      }, duration);
    } else {
      console.log('ugh');
    }
  }
};

check();
