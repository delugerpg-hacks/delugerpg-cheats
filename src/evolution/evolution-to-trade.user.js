// ==UserScript==
// @name         BETA Store ID
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.delugerpg.com/pokemon/page/*
// @match        https://www.delugerpg.com/pokemon/order/dex_asc/page/*
// @match        https://www.delugerpg.com/pokemon/order/dex_desc/page/*
// @match        https://www.delugerpg.com/pokemon/order/exp_desc/page/*
// @match        https://www.delugerpg.com/pokemon/order/name_asc/page/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=delugerpg.com
// @grant        none
// ==/UserScript==

const DELAY_MULTIPLIER = 4; // Adjust this constant to control the delay speed

async function waitForElement(selector) {
  return new Promise((resolve) => {
    const checkInterval = setInterval(() => {
      const element = document.querySelector(selector);
      if (element) {
        clearInterval(checkInterval);
        resolve(element);
      }
    }, 100);
  });
}

async function clickButton(selector, value) {
  await waitForElement(selector);
  const potentialButtons = document.querySelectorAll(selector);
  for (const btn of potentialButtons) {
    const val = btn.value.trim();
    const altBtn = btn.style.display === 'none';
    if (val === value && !altBtn) {
      btn.click();
    }
  }
}

async function clickTradeButton(poke) {
  await waitForElement('.trade');
  poke.querySelector('.trade').click();
  await setTimeoutPromised(1000 * DELAY_MULTIPLIER);
}

async function clickEvolveButton(poke) {
  await waitForElement('.evolve');
  poke.querySelector('.evolve').click();
  await setTimeoutPromised(2000 * DELAY_MULTIPLIER);
}

async function handleEvolution(poke) {
  const modal = await waitForElement('#DModal');
  const hasModal = modal.classList.contains('in');
  if (hasModal) {
    const notHaveEvolution = modal.querySelector('.imgtolevel').src;
    const have =
      notHaveEvolution === 'https://i.dstatic.com/images/pokeball-y.png';
    modal.querySelector('#closer').click();
    if (have) {
      await clickTradeButton(poke);
    }
  }
}

async function sendPokes() {
  const pokes = document.querySelectorAll('.container > *');
  for (const poke of pokes) {
    const hasEvolve = !!poke.querySelector('.evolve');
    if (!hasEvolve) {
      await clickTradeButton(poke);
    } else {
      window.open(poke.querySelector('.evolve').href);
    }
  }
}

async function nextPageExists() {
  const nextPageButton = await waitForElement(
    '.pagenum.pagebutton i.fa-chevron-right'
  );
  return !!nextPageButton;
}

async function main() {
  let runOnceOnLastPageFlag = true;
  const looper = setInterval(async () => {
    const hasNextButton = await nextPageExists();

    if (runOnceOnLastPageFlag) {
      await sendPokes();
      await setTimeoutPromised(2000 * DELAY_MULTIPLIER);
      try {
        const nextPageButton = await waitForElement(
          '.pagenum.pagebutton i.fa-chevron-right'
        );
        nextPageButton.click();
      } catch (e) {
        runOnceOnLastPageFlag = false;
        clearInterval(looper);
      }
    } else if (!hasNextButton && runOnceOnLastPageFlag) {
      runOnceOnLastPageFlag = false;
      clearInterval(looper);
    }
  }, 3000 * DELAY_MULTIPLIER);
}

async function setTimeoutPromised(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

main();
