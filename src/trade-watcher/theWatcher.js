// ==UserScript==
// @name        The watcher - delugerpg.com
// @namespace   Violentmonkey Scripts
// @match       https://www.delugerpg.com/trade/lookup
// @grant       none
// @version     1.0
// @author      -
// @description 03/09/2022, 10:53:40
// ==/UserScript==

(function watch() {
  let DOM = {};

  const currentTime = new Date().toString().slice(0, -31) + ' IST';

  const getPokemons = document.querySelectorAll('.container > div');
  getPokemons.forEach((pokemonContainer) => {
    pokemonContainer
      .querySelectorAll('a')
      .forEach((link) => (link.href = link)); // needed to fix links

    if (pokemonContainer.classList.contains('tabonline')) {
      pokemonContainer.classList.remove('tabonline');
      pokemonContainer.classList.add('taboffline');
    }
    const el = document.createElement('span');
    el.classList.add('custom-utc-time');
    el.style.fontSize = '10px';
    el.textContent = currentTime;
    const el2 = document.createElement('a');
    el2.href =
      'https://www.delugerpg.com/ajax/pokemon/info/' +
      pokemonContainer.dataset.pkid;
    el2.textContent = 'View Info';
    el2.target = '_blank';
    pokemonContainer.querySelector('.opts').appendChild(el2);
    pokemonContainer.querySelector('.opts').appendChild(el);
    DOM[pokemonContainer.id] = pokemonContainer.outerHTML;
  });

  console.save = function (data, filename) {
    if (!data) {
      console.error('Console.save: No data');
      return;
    }

    if (!filename)
      filename =
        'wA4Fhe1vUt2g7N ' +
        new Date().toString().slice(0, -31) +
        ' IST' +
        '.json';

    if (typeof data === 'object') {
      data = JSON.stringify(data, undefined, 4);
    }

    var blob = new Blob([data], {
        type: 'text/json'
      }),
      e = document.createEvent('MouseEvents'),
      a = document.createElement('a');

    a.download = filename;
    a.href = window.URL.createObjectURL(blob);
    a.dataset.downloadurl = ['text/json', a.download, a.href].join(':');
    e.initMouseEvent(
      'click',
      true,
      false,
      window,
      0,
      0,
      0,
      0,
      0,
      false,
      false,
      false,
      false,
      0,
      null
    );
    a.dispatchEvent(e);
  };
  console.save(DOM);
})();

setInterval(
  () => (document.location.href = 'https://www.delugerpg.com/trade/lookup'),
  15 * 60000
); // session lasts 30 mins

function startTimer(duration, display) {
  let start = Date.now();
  let diff;
  let minutes;
  let seconds;
  function timer() {
    // get the number of seconds that have elapsed since
    // startTimer() was called
    diff = duration - (((Date.now() - start) / 1000) | 0);

    // does the same job as parseInt truncates the float
    minutes = (diff / 60) | 0;
    seconds = diff % 60 | 0;

    minutes = minutes < 10 ? '0' + minutes : minutes;
    seconds = seconds < 10 ? '0' + seconds : seconds;

    display.textContent = minutes + ':' + seconds;

    if (diff <= 0) {
      // add one second so that the count down starts at the full duration
      // example 05:00 not 04:59
      start = Date.now() + 1000;
    }
  }
  // we don't want to wait a full second before the timer starts
  timer();
  setInterval(timer, 1000);
}

(function () {
  let mins = 60 * 15;
  let display = document.querySelector('div.title:nth-child(8) h1');
  startTimer(mins, display);
})();
