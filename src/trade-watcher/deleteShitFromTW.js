/**
 * Delete normal or shitty pokemons
 */

(function () {
  const hasAnyStat = (element) =>
    !!element.querySelectorAll('.istat > *').length;
  const hasAllStats = (element) =>
    element.querySelectorAll('.istat > *').length === 3;
  const hasSpeedStat = (element) => {
    if (!element.querySelectorAll('.istat > *').length) return false;
    const stats = element.querySelectorAll('.istat > *');
    let has = false;
    stats.forEach((stat) => {
      if (stat.classList.contains('sbtn-spe')) has = true;
    });
    return has;
  };
  const isNormal = (element) => {
    const name = element.querySelector('.nnfield').textContent.trim();
    const classes = [
      'Retro',
      'Shiny',
      'Chrome',
      'Dark',
      'Metallic',
      'Ghostly',
      'Mirage',
      'Shadow',
      'Negative'
    ];
    const firstName = name.split(' ')[0];
    return !classes.find((a) => a === firstName);
  };
  const isPreferedClass = (element) => {
    const name = element.querySelector('.nnfield').textContent.trim();
    const classes = ['Retro', 'Shiny', 'Chrome'];
    const firstName = name.split(' ')[0];
    return classes.find((a) => a === firstName);
  };
  const isLegend = (element) => {
    const legends = [
      'Arceus',
      'Articuno',
      'Azelf',
      'Blacephalon',
      'Buzzwole',
      'Calyrex',
      'Celebi',
      'Celesteela',
      'Cobalion',
      'Cosmoem',
      'Cosmog',
      'Cresselia',
      'Darkrai',
      'Deoxys',
      'Dialga',
      'Diancie',
      'Enamorus',
      'Entei',
      'Eternatus',
      'G-Max Melmetal',
      'G-Max Urshifu',
      'Genesect',
      'Giratina',
      'Glastrier',
      'Groudon',
      'Guzzlord',
      'Heatran',
      'Ho-oh',
      'Hoopa',
      'Jirachi',
      'Kartana',
      'Keldeo',
      'Kubfu',
      'Kyogre',
      'Kyurem',
      'Landorus',
      'Latias',
      'Latios',
      'Lugia',
      'Lunala',
      'Magearna',
      'Manaphy',
      'Marshadow',
      'Mega Diancie',
      'Mega Latias',
      'Mega Latios',
      'Mega Mewtwo X',
      'Mega Mewtwo Y',
      'Mega Rayquaza',
      'Melmetal',
      'Meloetta',
      'Meltan',
      'Mesprit',
      'Mew',
      'Mewtwo',
      'Moltres',
      'Naganadel',
      'Necrozma',
      'Nihilego',
      'Palkia',
      'Pheromosa',
      'Phione',
      'Poipole',
      'Primal Groudon',
      'Primal Kyogre',
      'Raikou',
      'Rayquaza',
      'Regice',
      'Regidrago',
      'Regieleki',
      'Regigigas',
      'Regirock',
      'Registeel',
      'Reshiram',
      'Shaymin',
      'Silvally',
      'Solgaleo',
      'Spectrier',
      'Stakataka',
      'Suicune',
      'Tapu Bulu',
      'Tapu Fini',
      'Tapu Koko',
      'Tapu Lele',
      'Terrakion',
      'Thundurus',
      'Tornadus',
      'Type: Null',
      'Urshifu',
      'Uxie',
      'Victini',
      'Virizion',
      'Volcanion',
      'Xerneas',
      'Xurkitree',
      'Yveltal',
      'Zacian',
      'Zamazenta',
      'Zapdos',
      'Zarude',
      'Zekrom',
      'Zeraora',
      'Zygarde'
    ];
    const name = element.querySelector('.nnfield').textContent.trim();
    return legends.some((legend) => name.includes(legend));
  };

  const hasExp = (element) => {
    const exp = Number(
      element
        .querySelector('.info')
        .textContent.trim()
        .match(/Exp:\s?([\d,]+)/)[1]
        .replaceAll(',', '')
    );
    if (exp >= 10000000) {
      return true;
    } else {
      return false;
    }
  };

  const getPokemons = document.querySelectorAll('.container > div');

  let counter = 0;
  const totalNumberElm = document.querySelector('body > h2');
  let total = Number(totalNumberElm.textContent.trim());
  for (const pokemon of getPokemons) {
    // Delete normal pokemons
    if (
      !(hasSpeedStat(pokemon) && isPreferedClass(pokemon)) &&
      !hasAllStats(pokemon) &&
      !hasExp(pokemon)
    ) {
      /**
       * The condition above will delete all pokemons that aren't prefered class
       * with +spe OR aren't TS. Also, FIND is there a way to practice these ^
       * boolean statements? for some reason I used || where I should've used
       * && and has to encode perefered class and speed condition coz i was not
       * able to understand otherwise
       */
      counter += 1;
      pokemon.remove();
    }

    // Delete that aren't legend with stats
    // if (!isLegend(pokemon) || !hasStats(pokemon)) {
    //   counter += 1;
    //   pokemon.remove();
    // }
  }

  if (total - counter < total) {
    totalNumberElm.textContent = total - counter;
    console.log(`Deleted: ${counter}.`);
  }
})();
