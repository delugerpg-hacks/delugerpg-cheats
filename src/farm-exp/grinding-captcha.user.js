// ==UserScript==
// @name         Grinding Captcha
// @namespace    captcha.grind
// @match        https://*.delugerpg.com/*
// @grant        none
// @version      1.2
// @author       rosavant
// @description  Listens to captchas that occur during grinding. Solves them with the help of Recaptcha Solver addon
// @run-at       document-end
// ==/UserScript==

// Define constants for the site URL and time intervals
const SUBDOMAIN = document.location.host.split('.')[0];
const WEBSITE = `https://${SUBDOMAIN}.delugerpg.com`;
const SECOND = 1000;
const MINUTE = 60 * SECOND;
const RECAPTCHA_CHECK_INTERVAL = 4500;
const RELOAD_INTERVAL = 9 * MINUTE;
const MAIN_INTERVAL = 15 * MINUTE;
const RECURR_INTERVAL = 30 * SECOND;

// Execute the main function every 15 minutes
setInterval(main, MAIN_INTERVAL);
main();

// The main function which handles the unlock page logic
function main() {
  // If the current page isn't the 'unlock' page, exit the function
  if (!document.location.pathname.includes('unlock')) return;

  // Remove overlays that might be preventing interaction
  removeOverlays();

  // Handle specific actions and checks related to the unlock page
  handleUnlockPage();
}

// Function to remove any overlay elements
function removeOverlays() {
  document.querySelectorAll('body > div').forEach((element) => {
    const el = element.querySelector('div:nth-child(1)') || null;
    const isOverlay = el
      ? el.style.opacity === '0.05' &&
        el.style.backgroundColor === 'rgb(255, 255, 255)'
      : false;
    if (isOverlay) el.remove();
  });
}

// Function to handle the logic specific to the unlock page
function handleUnlockPage() {
  // Set an interval to repeatedly check the status of the unlock page
  const checkUnlockStatus = setInterval(() => {
    if (!document.location.pathname.includes('unlock')) {
      clearInterval(checkUnlockStatus);
      return;
    }

    // Check for specific content to determine next actions
    const col2Content = document.querySelector('#col2').textContent.trim();

    // If 'Nothing to See Here' is found, navigate to the intro page
    if (col2Content === 'Nothing to See Here') {
      document.location.href = `${WEBSITE}/intro`;
    }
    // If a recaptcha solver element is found, handle the captcha
    else if (document.querySelector('.ReCaptcha_solver')) {
      handleRecaptcha();
    }
    // If neither conditions are met, check again after a delay
    else {
      setTimeout(() => {
        if (document.location.pathname.includes('unlock')) {
          checkUnlockStatus;
        }
      }, 4 * MINUTE);
    }
  }, RECURR_INTERVAL);

  // Schedule a page reload after 9 minutes
  setTimeout(() => {
    window.location.reload();
  }, RELOAD_INTERVAL);
}

// Function to handle the captcha logic
function handleRecaptcha() {
  const doCaptcha = setInterval(() => {
    const recaptchaSolver = document.querySelector('.ReCaptcha_solver');
    if (recaptchaSolver) {
      const status = recaptchaSolver.textContent.trim();
      // If the captcha is solved or if there's a response, proceed to click the button
      if (status === 'SOLVED' || !!grecaptcha.getResponse()) {
        clearInterval(doCaptcha);
        setTimeout(
          () => clickButton('form > .btn-primary', 'Return to Game'),
          RECAPTCHA_CHECK_INTERVAL
        );
      }
      // If there's an error from the captcha provider, reload the page
      else if (status === 'Error from 2captcha.com') {
        document.location.reload();
      }
    }
  }, RECAPTCHA_CHECK_INTERVAL);
}

// Function to programmatically click a button based on a selector and its value
function clickButton(selector, value) {
  setTimeout(() => {
    const potentialButtons = document.querySelectorAll(selector);
    for (const btn of potentialButtons) {
      const val = btn.value.trim();
      const altBtn = btn.style.length;
      if (val === value && !altBtn) {
        btn.click();
      }
    }
  }, SECOND);
}
