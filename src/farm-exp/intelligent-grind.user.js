// ==UserScript==
// @name         Intelligent Grind
// @version      2024-04-14
// @description  Grind on mobile or desktop like a human
// @namespace    https://gitlab.com/rosavant/delugerpg-hacks/delugerpg-cheats
// @author       rosavant
// @match        https://*.delugerpg.com/battle/user*
// @match        https://*.delugerpg.com/battle/trainer*
// @match        https://*.delugerpg.com/unlock*
// @match        https://*.delugerpg.com/battle/gym*
// @match        https://*.delugerpg.com/battle/challenge*
// @match        https://*.delugerpg.com/intro
// @homepageURL  https://gitlab.com/rosavant
// @homepage     https://gitlab.com/rosavant
// @icon         https://www.google.com/s2/favicons?sz=64&domain=delugerpg.com
// @updateURL    https://gitlab.com/rosavant/delugerpg-bots/raw/master/src/farm-exp/intelligent-grind.user.js
// @downloadURL  https://gitlab.com/rosavant/delugerpg-bots/raw/master/src/farm-exp/intelligent-grind.user.js
// @grant        GM_openInTab
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM_deleteValue
// @run-at       document-end
// ==/UserScript==

const SUBDOMAIN = document.location.host.split('.')[0];
const WEBSITE = `https://${SUBDOMAIN}.delugerpg.com`; // website's hostname
const BATTLE_USERNAME = '382'; // battle username, can be trainer number or a username
const Z_ATTACK = 'Inferno Overdrive'; // 'Inferno Overdrive'; // Which attack to use? 1, 2, 3 or 4?
const DEFAULT_ATTACK = 'V-create';
const TEAM_MEMBER_HEALTH_LEFT = 200; // Using < var and using Math.floor
const IS_TRAINING_SINGLE_POKEMON = false; // bullet-proofing single pokemon train
const STOP_AT_EXP = 1000 * 1000 * 10;
const STOP_AT_TOTAL_EXP = 999999999;
const IS_INVERSE = true;
const lowerLimit = Math.random() * (0.75 - 0.5) + 0.5;
const upperLimit = Math.random() * (3 - 1) + 1;
const DELAY_MULTIPLIER = weightedRandom(lowerLimit, undefined, upperLimit);
const longBattleRefresh = 5 * 60 * 1000;

const TRY_SKIPPING_POKE_SELECTION = true;
let SIMPLE_ATTACK = false;
let BATTLE_URI = '/battle/trainer/'; // path for trainer battle
// let BATTLE_URI = '/battle/user/username'; // path for trainer battle

BATTLE_URI = IS_INVERSE
  ? BATTLE_URI + '/' + BATTLE_USERNAME + '/inverse'
  : BATTLE_URI + '/' + BATTLE_USERNAME;

const btnNumber = IS_INVERSE ? 1 : 0;

const TRAINER_DIRECTORY = {
  // --- 2ND WALLACE (VIP TRAINER; WITH 6 POKEMON)
  // Inverse Battle; Chrome class for additional HP; Pure Fire-Type; at least +spe
  // If Normal Ludicolo, use Water-Type attack . If Negative Ludicolo, use Flying-Type attack (Preferably Z-move to save 1 click)
  // Use V-Create/Eruption/Mind Blown/Eruption/Shell Trap to his other pokemon
  Wallace: {
    Default: 'V-create',
    'Ghostly Ludicolo': 'Inferno Overdrive',
    'Dark Ludicolo': 'Inferno Overdrive',
    Ludicolo: 'Water Spout',
    'Negative Ludicolo': 'Inferno Overdrive',
    'Negative Milotic': 'Frenzy Plant',
    'Negative Gyarados': 'Bolt Strike',
    'Negative Wailord': 'Frenzy Plant',
    'Negative Whiscash': 'Frenzy Plant',
    'Negative Tentacruel': 'Prismatic Laser'
  },
  // --- 2ND LANCE (VIP TRAINER; WITH 6 POKEMON)
  // Inverse Battle; Chrome class for additional HP; Dual Steel & Fairy / Rock & Fairy; at least +spe
  // Use V-Create/Eruption/Mind Blown/Eruption/Shell Trap to all of his pokemon
  Lance: {},
  // --- GIOVANNI (STRONG TRAINERS)
  // Normal Battle; Chrome class for additional HP; Dual Steel & Flying / Steel & Bug / Steel & Grass; at least +spe
  // Use Freeze Shock/Ice Burn
  Giovanni: {},
  // --- SILVER (ULTIMATE TRAINER)
  // Inverse Battle; Chrome class for additional HP; Dual Bug & FlyingType; at least +spe
  // Use Psychic-Type or Ice-Type attack for Weavile (Preferably Z-move to save 1 click)
  // Use Megahorn to his other pokemon
  Silver: {
    //     Weavile: 'Ice Burn',
    'Negative Crobat': 'Ice Burn',
    'Mega Alakazam': 'Focus Punch',
    'Negative Mega Gengar': 'Precipice Blades',
    'Dark Weavile': 'Subzero Slammer',
    'Shadow Magnezone': 'Megahorn',
    'Mirage Mega Gengar': 'Megahorn',
    'Chrome Crobat': 'Megahorn',
    'Negative Mega Alakazam': 'Megahorn',
    Typhlosion: 'Megahorn'
  },
  // --- STEVEN (VIP TRAINER)
  // Inverse Battle; Chrome class for additional HP; Pure Fire-Type; at least +spe
  // If Normal Calydol, use Fighting-Type or Psychic-Type attack. If Negative Calydol, use Water-Type or Grass-Type attack (Preferably Z-move to save 1 click)
  // Use Explosion to his other pokemon
  Steven: {
    Claydol: 'Focus Punch',
    'Negative Claydol': 'Hydro Vortex',
    Default: 'Explosion',
    'Shiny Skarmory': 'Frenzy Plant',
    'Metallic Claydol': 'Shattered Psyche',
    'Ghostly Aggron': 'Explosion',
    'Dark Cradily': 'Explosion',
    'Shadow Armaldo': 'Explosion',
    'Mirage Metagross': 'Explosion'
  },
  // Ultimate trainer N
  N: {
    Carracosta: 'V-create',
    Vanilluxe: 'Freeze Shock',
    Archeops: 'V-create',
    Zoroark: 'Prismatic Laser',
    Reshiram: 'V-create',
    Zekrom: 'V-create',
    'Negative Carracosta': 'Focus Punch',
    'Negative Vanilluxe': 'V-create',
    'Negative Archeops': 'Freeze Shock',
    'Negative Zoroark': 'Focus Punch',
    'Negative Reshiram': 'Roar Of Time',
    'Negative Zekrom': 'Freeze Shock'
  },
  Ash: {
    'Negative Ash-greninja': 'Frenzy Plant',
    'Mega Charizard X': 'Frenzy Plant',
    'Shiny Noivern': 'Frenzy Plant',
    'Metallic Mega Sceptile': 'Frenzy Plant',
    'Ghostly Noctowl': 'Frenzy Plant',
    'Dark Alola Cap Pikachu': 'Gigaton Hammer'
  }
};

function weightedRandom(min, fixed = 1, max) {
  // console.log(`weightedRandom result.`); // Debug log for random delay calculation
  return (
    (min * 10 -
      1 +
      Math.round(
        (max * 10 - (min * 10 - 1)) /
          (Math.random() * (max * 10 - (min * 10 - 1)) + fixed)
      )) /
    10
  );
}

let toggle = false; // KEEP AS IS prevent refresh overflow

if (document.location.pathname === '/intro') {
  document.location.href = `${WEBSITE}${BATTLE_URI}`;
}

async function clickButton(selector, value) {
  // // console.log(`Attempting to click button: ${value}`);
  setTimeout(async () => {
    const res = await isLoading();
    if (!res) {
      const potentialButtons = document.querySelectorAll(selector);
      for (const btn of potentialButtons) {
        const val = btn.value.trim();
        const altBtn = btn.style.length;
        if (val === value && !altBtn) {
          // // console.log(`Clicking button: ${value}`);
          btn.click();
        }
      }
    } else {
      // // console.log(`Button click delayed due to loading: ${value}`);
    }
  }, 500 * DELAY_MULTIPLIER);
}

function hasButton(selector, value) {
  const potentialButtons = document.querySelectorAll(selector);
  for (const btn of potentialButtons) {
    const val = btn.value.trim();
    const altBtn = btn.style.length;
    if (val === value && !altBtn) {
      return true;
    }
  }
  return false;
}

function isButtonDisabled(selector, value) {
  const potentialButtons = document.querySelectorAll(selector);
  for (const btn of potentialButtons) {
    const val = btn.value.trim();
    const altBtn = btn.style.length;
    if (val === value && btn.disabled && !altBtn) {
      return true;
    }
  }
  return false;
}

async function isLoading(maxWaitTime = 20000) {
  // console.log('isLoading check started'); // Debug log
  const startTime = Date.now();

  while (Date.now() - startTime < maxWaitTime) {
    // console.log('Checking loading state...');
    const currentLoadingState = checkLoadingConditions(); // Simplified loading state check

    removeBackdropFade();

    // console.log(`currentLoadingState: ${currentLoadingState}`);
    if (!currentLoadingState) {
      // console.log('No loading detected, breaking loop.');
      break;
    }

    await new Promise((resolve) =>
      setTimeout(resolve, 1000 * DELAY_MULTIPLIER)
    );
    const elapsedTime = Date.now() - startTime;
    // console.log(`Elapsed Time: ${elapsedTime}ms`);
    // console.log(`Max Time: ${maxWaitTime}ms`);

    if (elapsedTime > 5000 * DELAY_MULTIPLIER && elapsedTime < maxWaitTime) {
      removeModal();
    } else if (elapsedTime > maxWaitTime) {
      // console.log('Max wait time exceeded, proceeding with caution...');
      break;
    }
  }

  // console.log('Exiting isLoading after loop completion.');
  return false;
}

function removeBackdropFade() {
  // Remove modal backdrop if present
  if (document.querySelector('.modal-backdrop.fade')) {
    document.querySelectorAll('.modal-backdrop.fade').forEach((backdrop) => {
      backdrop.remove();
    });
  }
}
function removeModal() {
  // Remove modal backdrop if present
  const modal = document.querySelector('#waitingmodal')
    ? document.querySelector('#waitingmodal')
    : null;
  if (modal && modal.style.display === 'block') {
    document.querySelector('#waitingmodal').remove();
  }
}

function checkLoadingConditions() {
  if (hasButton('.btn-battle-action', 'Please Wait...')) {
    return true;
  } else if (
    document.querySelector('#waitingmodal') &&
    document.querySelector('#waitingmodal').style.display === 'block'
  ) {
    return true;
  } else if (
    document.querySelector('.fade.in') &&
    document.querySelector('.fade.in').style.display === 'block'
  ) {
    return true;
  } else if (isButtonDisabled('.btn-battle-action', 'Attack')) {
    return true;
  } else if (isButtonDisabled('.btn-battle-action', 'Start Battle')) {
    return true;
  } else if (isButtonDisabled('.btn-battle-action', 'Continue')) {
    return true;
  } else if (document.querySelector('body').classList.contains('modal-open')) {
    return true;
  }

  removeBackdropFade();

  // Add other conditions as needed
  return false;
}

function getAttackNumber(selector) {
  const attackElements = document.querySelectorAll(
    'form > #user > .attklist > ul > li'
  );
  for (const attackElement of attackElements) {
    const input = attackElement.querySelector('input');
    const attack = attackElement.textContent.trim().toLowerCase();
    selector = typeof selector === 'string' ? selector.toLowerCase() : selector;
    if (
      attack === selector ||
      (Number(input.value) === selector && !input.disabled)
    ) {
      const selector = input.id;
      const value = input.value;
      return { selector: '#' + selector, value };
    } else if (
      attack === DEFAULT_ATTACK ||
      (Number(input.value) === DEFAULT_ATTACK && !input.disabled)
    ) {
      const selector = input.id;
      const value = input.value;
      return { selector: '#attkopt' + selector, value };
    }
  }
  return false;
}

async function clickAttack(move, goBack = false, delay = 250) {
  // console.log(`clickAttack called with move: ${move}, delay: ${delay}`); // Debug log
  const res = await isLoading();
  if (!res) {
    const atk = getAttackNumber(move);
    if (atk) {
      // console.log('Attack found:', atk); // Debug log
      const atkInput = document.querySelector(
        `${atk.selector}[value='${atk.value}']`
      );
      if (atkInput && !atkInput.selected && !atkInput.disabled) {
        atkInput.closest('li').click();
        await new Promise((resolve) => setTimeout(resolve, delay)); // Wait for delay
      } else {
        // console.log('No attack matched, using default attack'); // Debug log
        await handleDefaultAttack();
      }
    } else {
      // console.log('No attack matched, using default attack'); // Debug log
      await handleDefaultAttack();
    }
  }

  async function handleDefaultAttack() {
    const nextAtk = getAttackNumber(DEFAULT_ATTACK);
    if (nextAtk) {
      const nextAtkInput = document.querySelector(
        `${nextAtk.selector}[value='${nextAtk.value}']`
      );
      if (nextAtkInput && !nextAtkInput.selected && !nextAtkInput.disabled) {
        nextAtkInput.closest('li').click();
        await new Promise((resolve) => setTimeout(resolve, delay)); // Wait for delay
      }
    } else if (goBack) {
      return false;
    }
  }
}

async function selectAttackIntelligently() {
  if (SIMPLE_ATTACK) {
    clickAttack(Z_ATTACK);
  } else {
    const getOpponentName = document
      .querySelector('#opponent > .pokemon')
      .textContent.trim()
      .split("'")[0];

    if (TRAINER_DIRECTORY[getOpponentName]) {
      const getPokemonName = document
        .querySelector('#opponent > .pokemon')
        .textContent.trim()
        .split('\n')[0]
        .split("'")[1]
        .substring(2);

      let matchedAttack = null;

      // Sort keys by length in descending order to check specific names first
      const keys = Object.keys(TRAINER_DIRECTORY[getOpponentName]).sort(
        (a, b) => b.length - a.length
      );

      for (const key of keys) {
        if (getPokemonName.includes(key)) {
          matchedAttack = TRAINER_DIRECTORY[getOpponentName][key];
          break;
        }
      }

      if (matchedAttack) {
        clickAttack(matchedAttack);
      } else if (TRAINER_DIRECTORY[getOpponentName].Default) {
        clickAttack(TRAINER_DIRECTORY[getOpponentName].Default);
      } else {
        SIMPLE_ATTACK = true;
        clickAttack(Z_ATTACK);
      }
    } else {
      SIMPLE_ATTACK = true;
      clickAttack(Z_ATTACK);
    }
  }
}

async function doContinue() {
  // // console.log('doContinue function called');
  const cont = setInterval(async () => {
    if (document.querySelector('#teamleft')) {
      await isBattleLost();
      if (!(await isLoading())) {
        removeBackdropFade();
        // Dynamic check for loading completion
        if (hasButton('.btn-battle-action', 'Start Battle')) {
          clickButton('.btn-battle-action', 'Start Battle');
        } else if (
          hasButton('.btn-battle-action', 'Skip Pokemon Selection') &&
          TRY_SKIPPING_POKE_SELECTION
        ) {
          clickButton('.btn-battle-action', 'Skip Pokemon Selection');
        } else {
          clickButton('.btn-battle-action', 'Continue');
        }
      } else {
        // // console.log('Still waiting for loading to complete...');
        removeBackdropFade();
      }
    } else {
      clearInterval(cont);
    }
  }, 1000 * DELAY_MULTIPLIER); // Adjust the interval as needed
}

// And in doAttack:
async function doAttack() {
  // console.log('doAttack function called'); // Debug log
  const attackOpponent = setInterval(
    async () => {
      // console.log('Inside doAttack interval'); // Debug log
      if (document.querySelector('#attack')) {
        await isBattleLost();
        const opponentHealthBar = document.querySelector(
          '#opponent .prog-bar > div'
        );

        if (opponentHealthBar) {
          const opponentHealth = opponentHealthBar.style.width.slice(0, -1);
          const res = await isLoading();
          if (!res) {
            // console.log('Ready to attack. Opponent health:', opponentHealth); // Debug log
            if (
              opponentHealth > 0 &&
              hasButton('.btn-battle-action', 'Attack')
            ) {
              selectAttackIntelligently();
              clickButton('.btn-battle-action', 'Attack');
            } else {
              // console.log('Opponent defeated or button not found'); // Debug log
              SIMPLE_ATTACK = false;
              clearInterval(attackOpponent);
              if (
                hasButton('.btn-battle-action', 'Skip Pokemon Selection') &&
                TRY_SKIPPING_POKE_SELECTION
              ) {
                clickButton('.btn-battle-action', 'Skip Pokemon Selection');
              } else {
                if (TRY_SKIPPING_POKE_SELECTION) {
                  setTimeout(clickButton, 0, '.btn-battle-action', 'Continue');
                } else {
                  clickButton('.btn-battle-action', 'Continue');
                }
              }
            }
          }
        }
      } else {
        // console.log('Attack option not available, clearing interval'); // Debug log
        clearInterval(attackOpponent);
      }
    },
    Math.floor(
      Math.random() * (1000 * DELAY_MULTIPLIER - 500 * DELAY_MULTIPLIER) +
        500 * DELAY_MULTIPLIER
    )
  );
}

async function isBattleLost() {
  return new Promise((resolve) => {
    let number = 0;
    if (document.querySelector('#teamright .prog-bar div')) {
      document
        .querySelectorAll('#teamright .prog-bar div')
        .forEach((health) => {
          number += Number(health.style.width.slice(0, -1));
        });

      const totalPokemonTeam = document.querySelectorAll(
        '#teamright .prog-bar div'
      ).length;
      if (number <= TEAM_MEMBER_HEALTH_LEFT && !toggle) {
        toggle = true;
        const url = `${WEBSITE}${BATTLE_URI}`;
        window.open(url, '_self');
      }
    } else if (
      IS_TRAINING_SINGLE_POKEMON &&
      document.querySelector('#user .prog-bar div')
    ) {
      const health = document.querySelector('#user .prog-bar div');
      number = Number(health.style.width.slice(0, -1));
      if (number < 1 && !toggle) {
        toggle = true;
        const url = `${WEBSITE}${BATTLE_URI}`;
        window.open(url, '_self');
      }
    }
    resolve();
  });
}

async function main() {
  // // console.log('main function called');
  if (document.querySelector('#teamleft')) {
    await isBattleLost();
    doContinue();
  } else if (document.querySelector('#attack')) {
    await isBattleLost();
    doAttack();
  }
}

main();
setInterval(() => {
  main();
}, 2500 * DELAY_MULTIPLIER);

setInterval(async () => {
  if (document.querySelector('.notify_done')) {
    const test = document
      .querySelector('.notify_done')
      .textContent.trim()
      .substring(0, 15);

    if (test === 'Congratulations') {
      const findPokeWithMostExp = () => {
        const hold = { poke: '', exp: 0 };
        document.querySelectorAll('#viewpokes .vpname').forEach((poke) => {
          const exp = Number(
            poke.textContent
              .trim()
              .match(/Exp:[\s\t]+?([\d,?]+)/)[1]
              .replaceAll(',', '')
          );
          if (exp > hold.exp) {
            hold.poke = poke;
            hold.exp = exp;
          }
        });
        return hold.exp;
      };

      const calculationsForLog = () => {
        const totalPokesUsed =
          document.querySelectorAll('#viewpokes .vpname').length;
        const text = document.querySelector('.notify_done').textContent.trim();
        const earnedMoney = Number(
          text.match(/won\s([\d\,]+)/)[1].replaceAll(',', '')
        );
        const getPartialExp = Number(
          text.match(/gained\s([\d\,]+)\sexp/)[1].replaceAll(',', '')
        );
        const earnedExp = totalPokesUsed * getPartialExp;
        return { earnedMoney, earnedExp };
      };

      const highestExpInTeam = findPokeWithMostExp();
      const getBattleLogs = calculationsForLog();

      const getExpGenerated = Number(await GM_getValue('expDone', 0));
      const getMoneyGenerated = Number(await GM_getValue('moneyDone', 0));
      const totalExpGenerated = getExpGenerated + getBattleLogs.earnedExp;
      const totalMoneyGenerated = getMoneyGenerated + getBattleLogs.earnedMoney;
      Promise.all([
        GM_setValue('expDone', totalExpGenerated),
        GM_setValue('moneyDone', totalMoneyGenerated)
      ]).then(async () => {
        if (totalExpGenerated >= STOP_AT_TOTAL_EXP) {
          GM_openInTab(
            `https://www.google.com/search?client=firefox-b-d&q=Made ${totalExpGenerated}%21`
          );
          window.close();
        }

        if (highestExpInTeam < STOP_AT_EXP) {
          document
            .querySelectorAll('.notify_done > .btn-primary')
            [btnNumber].click();
        } else {
          GM_openInTab(
            `https://www.google.com/search?client=firefox-b-d&q=POKEMON+at+${highestExpInTeam}%21`
          );
          window.close();
        }
      });
    }
  } else if (document.querySelector('.notify_error')) {
    const text = document.querySelector('.notify_error').textContent.trim();
    let toggle = false;
    if (
      text === 'You are allowed only one battle every 10 seconds.' &&
      !toggle
    ) {
      toggle = true;
      const url = `${WEBSITE}${BATTLE_URI}`;
      GM_openInTab(url);
      window.close();
    } else if (!toggle) {
      window.location.href = `${WEBSITE}/home/`;
    }
  }
}, 2000 * DELAY_MULTIPLIER);

setInterval(() => {
  document.location.href = `${WEBSITE}${BATTLE_URI}`;
}, longBattleRefresh);

setInterval(function nah() {
  if (document.location.path === '/unlock') {
    if (
      document.querySelector('#col2').textContent.trim() ===
      'Nothing to See Here'
    ) {
      document.location.href = `${WEBSITE}${BATTLE_URI}`;
    } else {
      setTimeout(() => {
        if (document.location.pathname.includes('/unlock')) {
          nah();
        }
      }, 246000);
    }
  }
}, 30000);
