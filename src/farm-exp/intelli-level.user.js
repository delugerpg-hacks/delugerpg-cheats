// ==UserScript==
// @name         Intelligent LVL 100 Grind
// @version      1.1.3
// @description  Grind on mobile or desktop like a human
// @namespace    https://gitlab.com/rosavant/delugerpg-hacks/delugerpg-cheats
// @author       rosavant
// @match        https://*.delugerpg.com/battle/user*
// @match        https://*.delugerpg.com/battle/trainer*
// @match        https://*.delugerpg.com/unlock*
// @match        https://*.delugerpg.com/battle/gym*
// @match        https://*.delugerpg.com/battle/challenge*
// @match        https://*.delugerpg.com/intro
// @homepageURL  https://gitlab.com/rosavant
// @homepage     https://gitlab.com/rosavant
// @grant        GM_openInTab
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM_deleteValue
// @run-at       document-end
// ==/UserScript==

/**
 * NOTE –
 * This script needs Firefox's 'dom.allow_scripts_to_close_windows' setting set to true in the about:config section
 * That will allow the script to close the page when EXP checkpoint has been reached this preventing overtrainninng
 */

/**
 * Flamescape's DelugeRPG bot => https:/-mirror.org/scripts/show/139850
 * Thanks ^ for reminding me about the hidden submit buttons
 * "The 'lgmt' fake form with look-alike submit button"
 */
const LVL_LVL = 40;
const SUBDOMAIN = document.location.host.split('.')[0];
const WEBSITE = `https://${SUBDOMAIN}.delugerpg.com`; // website's hostname
// const BATTLE_USERNAME = '5147' //'5147'; // '5147'; // battle username, can be trainer number or a username
const BATTLE_USERNAME = '703'; //'5147'; // '5147'; // battle username, can be trainer number or a username
// const Z_ATTACK = 'Subzero Slammer'; // Which attack to use? 1, 2, 3 or 4?
const Z_ATTACK = 'Inferno Overdrive'; // Which attack to use? 1, 2, 3 or 4?
const DEFAULT_ATTACK = 'V-create';
const TEAM_MEMBER_HEALTH_LEFT = 200; // Using < var and using Math.floor
const IS_TRAINING_SINGLE_POKEMON = false; // bullet-proofing single pokemon train
const STOP_AT_EXP = 24930000;
// const STOP_AT_EXP = 979000;
const STOP_AT_TOTAL_EXP = 75000000;
// const STOP_AT_EXP = 930000;
// const STOP_AT_EXP = 24900000;
// const STOP_AT_EXP = 14930000;
// const STOP_AT_EXP = 10000000;
// const STOP_AT_EXP = 29970000;
// const STOP_AT_EXP = 5000000;
// const STOP_AT_EXP = 4930000;
const IS_INVERSE = true;
// const DELAY_MULTIPLIER = weightedRandom(0.9, undefined, 1.5);
const DELAY_MULTIPLIER = weightedRandom(
  // Math.random() * (1 - 0.8) + 0.8,
  0.9,
  undefined,
  Math.random() * (1.3 - 1) + 1
);

const TRY_SKIPPING_POKE_SELECTION = false;
let SIMPLE_ATTACK = false;
let BATTLE_URI = '/battle/trainer/'; // path for trainer battle
// let BATTLE_URI = '/battle/user/username/'; // path for user battle

BATTLE_URI = IS_INVERSE
  ? BATTLE_URI + '/' + BATTLE_USERNAME + '/inverse'
  : BATTLE_URI + '/' + BATTLE_USERNAME;

const btnNumber = IS_INVERSE ? 1 : 0;

const TRAINER_DIRECTORY = {
  // --- 2ND WALLACE (VIP TRAINER; WITH 6 POKEMON)
  // Inverse Battle; Chrome class for additional HP; Pure Fire-Type; at least +spe
  // If Normal Ludicolo, use Water-Type attack . If Negative Ludicolo, use Flying-Type attack (Preferably Z-move to save 1 click)
  // Use V-Create/Eruption/Mind Blown/Eruption/Shell Trap to his other pokemon
  Wallace: {
    // Default: 'V-create',
    Ludicolo: 'Water Spout',
    'Negative Ludicolo': 'Sky Attack',
    'Negative Milotic': 'Frenzy Plant',
    'Negative Gyarados': 'Bolt Strike',
    'Negative Wailord': 'Frenzy Plant',
    'Negative Whiscash': 'Frenzy Plant',
    'Negative Tentacruel': 'Prismatic Laser'
  },
  // --- 2ND LANCE (VIP TRAINER; WITH 6 POKEMON)
  // Inverse Battle; Chrome class for additional HP; Dual Steel & Fairy / Rock & Fairy; at least +spe
  // Use V-Create/Eruption/Mind Blown/Eruption/Shell Trap to all of his pokemon
  // Lance: {},
  // --- GIOVANNI (STRONG TRAINERS)
  // Normal Battle; Chrome class for additional HP; Dual Steel & Flying / Steel & Bug / Steel & Grass; at least +spe
  // Use Freeze Shock/Ice Burn
  Giovanni: {},
  // --- SILVER (ULTIMATE TRAINER)
  // Inverse Battle; Chrome class for additional HP; Dual Bug & FlyingType; at least +spe
  // Use Psychic-Type or Ice-Type attack for Weavile (Preferably Z-move to save 1 click)
  // Use Megahorn to his other pokemon
  Silver: {
    //     Weavile: 'Ice Burn',
    'Negative Crobat': 'Ice Burn',
    'Mega Alakazam': 'Focus Punch',
    'Negative Mega Gengar': 'Precipice Blades'
  },
  // --- STEVEN (VIP TRAINER)
  // Inverse Battle; Chrome class for additional HP; Pure Fire-Type; at least +spe
  // If Normal Calydol, use Fighting-Type or Psychic-Type attack. If Negative Calydol, use Water-Type or Grass-Type attack (Preferably Z-move to save 1 click)
  // Use Explosion to his other pokemon
  Steven: {
    Claydol: 'Focus Punch',
    'Negative Claydol': 'Hydro Vortex',
    Default: 'Explosion'
  },
  // Ultimate trainer N
  N: {
    Carracosta: 'V-create',
    //Vanilluxe: 'Freeze Shock',
    Archeops: 'V-create',
    Zoroark: 'Prismatic Laser',
    Reshiram: 'V-create',
    Zekrom: 'V-create',
    'Negative Carracosta': 'Focus Punch',
    'Negative Vanilluxe': 'V-create',
    'Negative Archeops': 'Freeze Shock',
    'Negative Zoroark': 'Focus Punch',
    'Negative Reshiram': 'Roar Of Time',
    'Negative Zekrom': 'Freeze Shock'
  }
};

/**
 * From https://stackoverflow.com/a/46774731
 * Generates random time with minimum having the most weight
 * @param {number} min lets say I want 0.5 as a multiplier, consider x ,so I'll do x * 10 theb min = (x * 10) - 1
 * @param {number} max lets say I want it to be 5, consider y, so I'll do y * 10 then max = (y*10) - min
 * @param {number} fixed should always be one make range by remind one from lower range and then subtracting lower range from max like explained above
 * @returns {number} time
 */
function weightedRandom(min, fixed = 1, max) {
  return (
    (min * 10 -
      1 +
      Math.round(
        (max * 10 - (min * 10 - 1)) /
          (Math.random() * (max * 10 - (min * 10 - 1)) + fixed)
      )) /
    10
  );
}

let toggle = false; // KEEP AS IS prevent refresh overflow

// Easier battle
if (document.location.pathname === '/intro') {
  document.location.href = `${WEBSITE}${BATTLE_URI}`;
}

const deadPokemons = await GM_getValue('deadPokemons', 0);
let pokemonCounter = 0;
let pokemonAlive = {};
function checkDead() {
  const pokemonOrderNo = Number(document.querySelector('.batsel input').value);
  const pokemonName = document
    .querySelector('.batsel .batname')
    .textContent.trim();
  // console.log(pokemonOrderNo, pokemonName);
  pokemonAlive[pokemonOrderNo] = pokemonName;
  // console.log(pokemonAlive);
}

/**
 * Mimic button click
 * @param {string} selector targetted button's class
 * @param {string} value targetted button's value
 */
async function clickButton(selector, value) {
  setTimeout(async () => {
    const res = await isLoading(0);
    if (!res) {
      const potentialButtons = document.querySelectorAll(selector);
      for (const btn of potentialButtons) {
        const val = btn.value.trim();
        const altBtn = btn.style.length;
        if (val === value && !altBtn) {
          btn.click();
        }
      }
    }
  }, 500 * DELAY_MULTIPLIER);
}

/**
 * Check if a button with a specific class and value exists
 * @param {string} selector targetted button's class
 * @param {string} value targetted button's value
 * @returns boolean
 */
function hasButton(selector, value) {
  const potentialButtons = document.querySelectorAll(selector);
  for (const btn of potentialButtons) {
    const val = btn.value.trim();
    const altBtn = btn.style.length;
    if (val === value && !altBtn) {
      return true;
    }
  }
  return false;
}

/**
 * Check if a button with a specific class and value is in disabled state
 * @param {string} selector targetted button's class
 * @param {string} value targetted button's value
 * @returns boolean
 */
function isButtonDisabled(selector, value) {
  const potentialButtons = document.querySelectorAll(selector);
  for (const btn of potentialButtons) {
    const val = btn.value.trim();
    const altBtn = btn.style.length;
    if (val === value && btn.disabled && !altBtn) {
      return true;
    }
  }
  return false;
}

/**
 * Check if UI is in loading state, that is, if button is diabled, if the button
 * is in loading state, if there is a modal overlayed
 * @returns boolean
 */
async function isLoading() {
  let flag = false;

  // On "Please Wait..." button
  if (hasButton('.btn-battle-action', 'Please Wait...')) {
    flag = true;
  }
  // On display: block; waiting modal
  else if (
    document.querySelector('#waitingmodal') &&
    document.querySelector('#waitingmodal').style.display === 'block'
  ) {
    flag = true;
  }
  // on a popup that has faded in because of click spam or slow server
  else if (document.querySelector('.fade.in')) {
    flag = true;
  }
  // On disabled attack button
  else if (isButtonDisabled('.btn-battle-action', 'Attack')) {
    flag = true;
  }
  // On disabled start button
  else if (isButtonDisabled('.btn-battle-action', 'Start Battle')) {
    flag = true;
  }
  // On disabled continue button
  else if (isButtonDisabled('.btn-battle-action', 'Continue')) {
    flag = true;
  }
  // On body class "modal-open"
  else if (document.querySelector('body').classList.contains('modal-open')) {
    flag = true;
  }

  // Are modals being spammed?
  if (flag && document.querySelector('.modal-backdrop.fade')) {
    document.querySelectorAll('.modal-backdrop.fade').forEach((backdrop) => {
      backdrop.remove();
    });
  }

  if (flag) {
    setTimeout(() => {
      main();
    }, 2000 * DELAY_MULTIPLIER);
  } else if (!flag) {
    return flag;
  }
}

function getAttackNumber(selector) {
  const attackElements = document.querySelectorAll(
    'form > #user > .attklist > ul > li'
  );
  for (const attackElement of attackElements) {
    const input = attackElement.querySelector('input');
    const attack = attackElement.textContent.trim().toLowerCase();
    selector = typeof selector === 'string' ? selector.toLowerCase() : selector;
    if (
      attack === selector ||
      (Number(input.value) === selector && !input.disabled)
    ) {
      const selector = input.id;
      const value = input.value;
      return { selector: '#' + selector, value };
    } else if (
      attack === DEFAULT_ATTACK ||
      (Number(input.value) === DEFAULT_ATTACK && !input.disabled)
    ) {
      const selector = input.id;
      const value = input.value;
      return { selector: '#attkopt' + selector, value };
    }
  }
  return false;
}

async function clickAttack(move, goBack = false) {
  // setTimeout(async () => {
  const res = await isLoading(0);
  if (!res) {
    const atk = getAttackNumber(move);
    // not using clikcButton fn here because I'm finding the closest li whose selector I don't know
    const atkInput = atk
      ? document.querySelector(`${atk.selector}[value='${atk.value}']`)
      : null;
    // console.log(atk, atkInput); // need for checking
    if (atkInput) {
      if (!atkInput.selected && !atkInput.disabled) {
        // console.log('pep 1');
        atkInput.closest('li').click();
      } else if (goBack) {
        // console.log('pep 2');
        return false;
      } else if (atkInput.disabled) {
        // console.log('pep 3');
        const nextAtk = getAttackNumber(DEFAULT_ATTACK);
        const nextAtkInput = document.querySelector(
          `${nextAtk.selector}[value='${nextAtk.value}']`
        );
        if (!nextAtkInput.selected) {
          // console.log('pep 4');
          nextAtkInput.closest('li').click();
        }
      }
    } else {
      const nextAtk = getAttackNumber(DEFAULT_ATTACK);
      const nextAtkInput = document.querySelector(
        `${nextAtk.selector}[value='${nextAtk.value}']`
      );
      if (!nextAtkInput.selected) {
        // console.log('pep 4.1');
        nextAtkInput.closest('li').click();
      }
    }
  }
  // }, 500 * DELAY_MULTIPLIER);
}

async function selectAttackIntelligently() {
  if (SIMPLE_ATTACK) {
    // Logic for selecting Z-Move first and then normal ones, make sure the normal one is after the
    // Z-Move, so if Z-move is no.3 normal should be no.4
    clickAttack(Z_ATTACK);
  } else {
    const getOpponentName = document
      .querySelector('#opponent > .pokemon')
      .textContent.trim()
      .split("'")[0];
    if (TRAINER_DIRECTORY[getOpponentName]) {
      const getPokemonName = document
        .querySelector('#opponent > .pokemon')
        .textContent.trim()
        .split('\n')[0]
        .split("'")[1]
        .substring(2);
      const pokemonNameSplit = getPokemonName.split(' ');
      const spirits = [
        'Shiny',
        'Retro',
        'Chrome',
        'Mirage',
        'Negative',
        'Metallic',
        'Ghostly',
        'Dark',
        'Shadow'
      ];

      let pokemonName = '';

      if (
        pokemonNameSplit[0] !== 'Negative' &&
        spirits.includes(pokemonNameSplit[0])
      ) {
        pokemonName = pokemonNameSplit.slice(1).join(' ');
      } else {
        pokemonName = getPokemonName;
      }
      // console.log(pokemonNameSplit, getPokemonName, pokemonName);
      // prettier-ignore
      // console.log(getOpponentName, '|', pokemonName, '|', TRAINER_DIRECTORY[getOpponentName][pokemonName]); // needed for checking
      if (TRAINER_DIRECTORY[getOpponentName][pokemonName]) {
           // console.log('condition 1');
           clickAttack(TRAINER_DIRECTORY[getOpponentName][pokemonName]);
         } else if (TRAINER_DIRECTORY[getOpponentName].Default) {
           // console.log('condition 2');
           clickAttack(TRAINER_DIRECTORY[getOpponentName].Default);
         } else {
           SIMPLE_ATTACK = true;
           // console.log('condition 3');
           clickAttack(Z_ATTACK);
         }
    } else {
      SIMPLE_ATTACK = true;
      clickAttack(Z_ATTACK);
    }
  }
  // Check opponent's pokemon name
}

/**
 * To keep the battle in a continue state, for example, click buttons such as
 * "Start Battle", "Skip Pokemon Selection" or "Continue"
 */
async function doContinue() {
  // Clicking button
  const cont = setInterval(
    async () => {
      if (document.querySelector('#teamleft')) {
        await isBattleLost();
        // checkDead();
        const res = await isLoading(0);
        if (!res) {
          if (hasButton('.btn-battle-action', 'Start Battle')) {
            clickButton('.btn-battle-action', 'Start Battle');
          } else {
            if (
              hasButton('.btn-battle-action', 'Skip Pokemon Selection') &&
              TRY_SKIPPING_POKE_SELECTION
            ) {
              clickButton('.btn-battle-action', 'Skip Pokemon Selection');
            } else {
              clickButton('.btn-battle-action', 'Continue');
            }
          }
        }
      } else clearInterval(cont);
    },
    Math.floor(
      Math.random() * (1000 * DELAY_MULTIPLIER - 300 * DELAY_MULTIPLIER) +
        300 * DELAY_MULTIPLIER
    )
  );
}

async function doAttack() {
  const attackOpponent = setInterval(
    async () => {
      if (document.querySelector('#attack')) {
        await isBattleLost();
        const opponentHealthBar = document.querySelector(
          '#opponent .prog-bar > div'
        );

        if (opponentHealthBar) {
          const opponentHealth = opponentHealthBar.style.width.slice(0, -1);
          const res = await isLoading(0);
          if (!res) {
            if (
              opponentHealth > 0 &&
              hasButton('.btn-battle-action', 'Attack')
            ) {
              selectAttackIntelligently();
              clickButton('.btn-battle-action', 'Attack');
            } else {
              SIMPLE_ATTACK = false;
              clearInterval(attackOpponent);
              if (
                hasButton('.btn-battle-action', 'Skip Pokemon Selection') &&
                TRY_SKIPPING_POKE_SELECTION
              ) {
                clickButton('.btn-battle-action', 'Skip Pokemon Selection');
              } else {
                clickButton('.btn-battle-action', 'Continue');
              }
            }
          }
        }
      } else clearInterval(attackOpponent);
    },
    Math.floor(
      Math.random() * (1000 * DELAY_MULTIPLIER - 500 * DELAY_MULTIPLIER) +
        500 * DELAY_MULTIPLIER
    )
  );
}

async function isBattleLost() {
  return new Promise((resolve) => {
    let number = 0;
    if (document.querySelector('#teamright .prog-bar div')) {
      document
        .querySelectorAll('#teamright .prog-bar div')
        .forEach((health) => {
          // number += Math.floor(Number(health.style.width.slice(0, -1) / 100));
          // number +=
          //   Math.round(Number(health.style.width.slice(0, -1) / 100) * 10) / 10;
          number += Number(health.style.width.slice(0, -1));
        });

      const totalPokemonTeam = document.querySelectorAll(
        '#teamright .prog-bar div'
      ).length;
      if (number <= TEAM_MEMBER_HEALTH_LEFT && !toggle) {
        toggle = true;
        const url = `${WEBSITE}${BATTLE_URI}`;
        // Method 1 (works on mobile)
        window.open(url, '_self');
        // Method 2
        //document.location.href = url;
        // Method 3 (bad)
        // window.location.reload()
        // MEthod 4
        // GM_openInTab(url);
        // window.close();
      }
    } else if (
      IS_TRAINING_SINGLE_POKEMON &&
      document.querySelector('#user .prog-bar div')
    ) {
      const health = document.querySelector('#user .prog-bar div');
      number = Number(health.style.width.slice(0, -1));
      if (number < 1 && !toggle) {
        toggle = true;
        const url = `${WEBSITE}${BATTLE_URI}`;
        window.open(url, '_self');
      }
    }
    resolve();
  });
}

async function main() {
  if (document.querySelector('#teamleft')) {
    await isBattleLost();
    doContinue();
  } else if (document.querySelector('#attack')) {
    await isBattleLost();
    doAttack();
  }
}

main();
setInterval(() => {
  main();
}, 2500 * DELAY_MULTIPLIER);

////////////////////////////////////////////////////////
///////////////////////////////////////////////////////
//////////////////////////////////////////////////////

let FLAG_CHG = true;
let FLAG_CHG_2 = true;

setInterval(async () => {
  if (document.querySelector('.notify_done')) {
    const test = document
      .querySelector('.notify_done')
      .textContent.trim()
      .substring(0, 15);

    if (test === 'Congratulations') {
      const getTeam = () => {
        const teamp = [];
        document.querySelectorAll('#viewpokes .vpname').forEach((poke) => {
          const id = poke.id.substring(4);
          teamp.push(id);
        });
        return teamp;
      };

      (async function findPokeLvlHundred() {
        if (FLAG_CHG) {
          // console.log('executing');
          FLAG_CHG = false;
          let counterr = 0;
          let cnt = 0;
          let howManyTraining = 1;
          const whitelist = ['hFfswm', 'j5qwCt', 'RyRgty', 'sNtwzr'];
          let team = [];
          document
            .querySelectorAll('#viewpokes > div')
            .forEach(async (poke) => {
              const counter1 =
                document.querySelectorAll('#viewpokes > div').length;
              ++counterr;
              const level = Number(
                poke.textContent.trim().match(/Level:[\s\n]+?(\d+)/)[1]
              );
              const id = poke.id.substring(4);
              if (!whitelist.includes(id) && level >= LVL_LVL) {
                // FLAG_CHG_2 = false;
                // prettier-ignore
                let body = ''
                cnt += 1;
                // const team = JSON.parse(await GM_getValue('team', JSON.stringify(["BV3ZFn","mVtcfx","LZ5fzB","hy7K6Z","TLgKtn","VsXTW"])));
                // prettier-ignore
                const result = JSON.parse(await GM_getValue('todo', JSON.stringify([])));
                // const f = team.indexOf(id);
                // console.log(f, id);
                // if (f > -1) {
                if (cnt === 2) {
                  team = [result.shift(), result.shift()];
                  // if (howManyTraining === 0) {
                  //   body = `mem[]=${team[0]}&mem[]=${whitelist[0]}&mem[]=${whitelist[1]}&mem[]=${whitelist[2]}&mem[]=${whitelist[3]}&mem[]=${whitelist[4]}`;
                  // }
                  //  else if (howManyTraining === 1) {
                  body = `mem[]=${team[0]}&mem[]=${team[1]}&mem[]=${whitelist[0]}&mem[]=${whitelist[1]}&mem[]=${whitelist[2]}&mem[]=${whitelist[3]}`;
                  // }
                  // else if (howManyTraining === 2) {
                  //   body = `mem[]=${team[0]}&mem[]=${team[1]}&mem[]=${team[2]}&mem[]=${whitelist[0]}&mem[]=${whitelist[1]}&mem[]=${whitelist[2]}`;
                  // }
                  ++howManyTraining;

                  // console.log(team);
                  // team[f] = result.shift();
                  await GM_setValue('todo', JSON.stringify(result));
                  // const res3 = await
                  fetch('https://www.delugerpg.com/ajax/team/edit/save', {
                    credentials: 'include',
                    headers: {
                      'User-Agent':
                        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:106.0) Gecko/20100101 Firefox/106.0',
                      Accept: 'application/json, text/javascript, */*; q=0.01',
                      'Accept-Language': 'en-US,en;q=0.5',
                      'Content-Type':
                        'application/x-www-form-urlencoded; charset=UTF-8',
                      'X-Requested-With': 'XMLHttpRequest',
                      'Sec-Fetch-Dest': 'empty',
                      'Sec-Fetch-Mode': 'cors',
                      'Sec-Fetch-Site': 'same-origin',
                      'Sec-GPC': '1'
                    },
                    referrer: 'https://www.delugerpg.com/',
                    body: body,
                    method: 'POST',
                    mode: 'cors'
                  }).then(async () => {
                    // console.log('res3');
                    await GM_setValue('team', JSON.stringify(team));

                    // console.log('res2');
                    setTimeout(() => {
                      document
                        .querySelectorAll('.notify_done > .btn-primary')
                        [btnNumber].click();
                    }, 3000);
                  });
                  // if (res3) {

                  // }
                  // } else {
                  //   console.log('meh');
                  // }
                }
              }
              if (counterr === counter1) {
                setTimeout(() => {
                  document
                    .querySelectorAll('.notify_done > .btn-primary')
                    [btnNumber].click();
                }, 3000);
              }
            });
        }
      })();
    }
  } else if (document.querySelector('.notify_error')) {
    const text = document.querySelector('.notify_error').textContent.trim();
    let toggle = false;
    if (
      text === 'You are allowed only one battle every 10 seconds.' &&
      !toggle
    ) {
      toggle = true;
      const url = `${WEBSITE}${BATTLE_URI}`;
      GM_openInTab(url);
      window.close();
    } else if (!toggle) {
      window.location.href = `${WEBSITE}/home/`;
    }
  }
}, 2000 * DELAY_MULTIPLIER);

////////////////////////////////////////////////////////
///////////////////////////////////////////////////////
//////////////////////////////////////////////////////

setInterval(() => {
  document.location.href = `${WEBSITE}${BATTLE_URI}`;
}, 200000);

////////////////////////////////////////////////////////
///////////////////////////////////////////////////////
//////////////////////////////////////////////////////

setInterval(function nah() {
  if (document.location.path === '/unlock') {
    if (
      document.querySelector('#col2').textContent.trim() ===
      'Nothing to See Here'
    ) {
      document.location.href = `${WEBSITE}${BATTLE_URI}`;
    } else {
      setTimeout(() => {
        if (document.location.pathname.includes('/unlock')) {
          nah();
        }
      }, 246000);
    }
  }
}, 30000);
