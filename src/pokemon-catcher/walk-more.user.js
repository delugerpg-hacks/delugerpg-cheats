// ==UserScript==
// @name         Walk More (Supplement Catching)
// @namespace    http://tampermonkey.net/
// @version      2024-04-14
// @description  try to take over the world!
// @author       You
// @match        https://*.delugerpg.com/map/*
// @match        https://www.delugerpg.com/map/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=delugerpg.com
// @updateURL    https://gitlab.com/rosavant/delugerpg-bots/raw/master/src/pokemon-catcher/walk-more.user.js
// @downloadURL  https://gitlab.com/rosavant/delugerpg-bots/raw/master/src/pokemon-catcher/walk-more.user.js
// @grant        none
// @run-at       document-end
// ==/UserScript==

(function () {
  const avatar = document.querySelector('#usersprite').parentElement;

  let topInitial = avatar.style.top;
  let leftInitial = avatar.style.left;

  let staticPosition = 0;

  const checkPosition = () => {
    const top = avatar.style.top;
    const left = avatar.style.left;

    if (top === topInitial && left === leftInitial) {
      staticPosition += 1;

      // console.log('Position is not changing.');

      if (staticPosition > 3) {
        // console.log('Reloading.');
        window.location.reload();
      }
    }

    topInitial = top;
    leftInitial = left;
  };

  setInterval(checkPosition, 10 * 1000);
})();
