// ==UserScript==
// @name         Pokemon Catcher
// @version      2024-04-14
// @description  Find desired pokemons on map
// @namespace    https://gitlab.com/rosavant/delugerpg-hacks/delugerpg-cheats
// @author       rosavant
// @match        https://*.delugerpg.com/map/*
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM_deleteValue
// @run-at       document-end
// @updateURL    https://gitlab.com/rosavant/delugerpg-bots/raw/master/src/pokemon-catcher/pokemon-catcher.user.js
// @downloadURL  https://gitlab.com/rosavant/delugerpg-bots/raw/master/src/pokemon-catcher/pokemon-catcher.user.js
// ==/UserScript==

// document.querySelector('*').textContent.trim().includes('Performance & security by Cloudflare')

const DELAY_MULTIPLIER = weightedRandom(2, undefined, 10);

const SUBDOMAIN = document.location.host.split('.')[0];
const WEBSITE = `https://${SUBDOMAIN}.delugerpg.com`; // website's hostname

(async function () {
  const res = await GM_getValue('toReload', 'yes');
  if (res && res === 'yes') {
    GM_setValue('toReload', 'no');
    // setTimeout(() => {
    //   window.location.reload();
    // }, 2500);
  }
})();

/**
 * From https://stackoverflow.com/a/46774731
 * Generates random time with minimum having the most weight
 * @param {number} min lets say I want 0.5 as a multiplier, consider x ,so I'll do x * 10 theb min = (x * 10) - 1
 * @param {number} max lets say I want it to be 5, consider y, so I'll do y * 10 then max = (y*10) - min
 * @param {number} fixed should always be one make range by remind one from lower range and then subtracting lower range from max like explained above
 * @returns {number} time
 */

function weightedRandom(min, fixed = 1, max) {
  // Check if the min and max values are valid
  if (min >= max) {
    // console.error('Invalid range: min should be less than max');
    return null;
  }

  // Scale up the min and max to allow for decimal precision
  let scaledMin = min * 10 - 1;
  let scaledMax = max * 10;

  // Calculate the scaled range
  let scaledRange = scaledMax - scaledMin;

  // Generate a weighted random number
  // The 'fixed' value is used to skew the distribution towards the lower end
  // The use of 'Math.random()' generates a value between 0 and 1
  // Multiplying this by the scaled range and adding the 'fixed' value skews the randomness
  let weightedRandom = Math.round(
    scaledRange / (Math.random() * scaledRange + fixed)
  );

  // Rescale to the original range and return
  return (scaledMin + weightedRandom) / 10;
}

function overworld() {
  const groupedMaps = {
    overworld: [
      '/map/overworld1',
      '/map/overworld2',
      '/map/overworld3',
      '/map/overworld4',
      '/map/overworld5',
      '/map/overworld6',
      '/map/overworld7',
      '/map/overworld8',
      '/map/overworld9',
      '/map/overworld10',
      '/map/overworld11',
      '/map/overworld12',
      '/map/overworld13',
      '/map/overworld14',
      '/map/overworld15',
      '/map/overworld16',
      '/map/overworld17',
      '/map/overworld18',
      '/map/overworld19',
      '/map/overworld20',
      '/map/overworld21',
      '/map/overworld22',
      '/map/overworld23',
      '/map/overworld24',
      '/map/overworld25'
    ],
    snowcave: [
      '/map/snowcave1',
      '/map/snowcave2',
      '/map/snowcave3',
      '/map/snowcave4'
    ],
    volcano: [
      '/map/volcano1',
      '/map/volcano2',
      '/map/volcano3',
      '/map/volcano4'
    ],
    unownruins: ['/map/unownruins1', '/map/unownruins2'],
    pkmntower: [
      '/map/pkmntower1',
      '/map/pkmntower2',
      '/map/pkmntower3',
      '/map/pkmntower3'
    ],
    powerplant: [
      '/map/powerplant1',
      '/map/powerplant2',
      '/map/powerplant3',
      '/map/powerplant3'
    ],
    pkmnmansion: [
      '/map/pkmnmansion1',
      '/map/pkmnmansion2',
      '/map/pkmnmansion3',
      '/map/pkmnmansion4'
    ],
    rockcave: [
      '/map/rockcave1',
      '/map/rockcave2',
      '/map/rockcave3',
      '/map/rockcave4'
    ]
  };

  // Function to select a random map from each group
  function getRandomMap() {
    const randomMaps = Object.values(groupedMaps).map((group) => {
      const randomIndex = Math.floor(Math.random() * group.length);
      return group[randomIndex];
    });
    const randomIndex = Math.floor(Math.random() * randomMaps.length);
    return randomMaps[randomIndex];
  }
  const gotoMapURL = `${WEBSITE}${getRandomMap()}`;
  document.location.href = gotoMapURL;
}

(function () {
  let flag = null;

  // console.clear(); // main

  function startTimer(duration, display) {
    let start = Date.now();
    let diff;
    let minutes;
    let seconds;
    function timer() {
      // get the number of seconds that have elapsed since
      // startTimer() was called
      diff = duration - (((Date.now() - start) / 1000) | 0);

      // does the same job as parseInt truncates the float
      minutes = (diff / 60) | 0;
      seconds = diff % 60 | 0;

      minutes = minutes < 10 ? '0' + minutes : minutes;
      seconds = seconds < 10 ? '0' + seconds : seconds;

      if (minutes == 0) {
        overworld();
      }

      if (seconds == 0) {
        // console.clear();
        // eslint-disable-next-line no-console
        console.log(minutes + ':' + seconds);
      }

      // console.log(minutes + ':' + seconds);

      if (diff <= 0) {
        // add one second so that the count down starts at the full duration
        // example 05:00 not 04:59
        start = Date.now() + 1000;
      }
    }
    // we don't want to wait a full second before the timer starts
    timer();
    setInterval(timer, 1000);
  }

  (function () {
    // Generate a random number between 8 and 2
    const randomMinutes = Math.floor(Math.random() * (45 - 2 + 1)) + 2;
    // REFRESH TIME
    // Convert the random minutes to seconds
    let mins = 60 * randomMinutes;
    startTimer(mins);
  })();

  setTimeout(
    () => {
      flag = true;
      setTimeout(
        () => {
          if (flag) {
            overworld();
          }
        },
        10 * 60 * 1000
      );
    },
    10 * 60 * 1000
  );

  // prettier-ignore
  const normalLegends=["Articuno","Zapdos","Moltres","Mewtwo","Mega Mewtwo X","Mega Mewtwo Y","Raikou","Entei","Suicune","Lugia","Ho-oh","Regirock","Regice","Registeel","Latias","Mega Latias","Latios","Mega Latios","Kyogre","Primal Kyogre","Groudon","Primal Groudon","Rayquaza","Mega Rayquaza","Uxie","Mesprit","Azelf","Dialga","Palkia","Heatran","Regigigas","Giratina","Cresselia","Cobalion","Terrakion","Virizion","Tornadus","Thundurus","Reshiram","Zekrom","Landorus","Kyurem","Xerneas","Yveltal","Zygarde","Type: Null","Silvally","Tapu Koko","Tapu Lele","Tapu Bulu","Tapu Fini","Cosmog","Cosmoem","Solgaleo","Lunala","Necrozma","Zacian","Zamazenta","Eternatus","Kubfu","Urshifu","G-Max Urshifu","Regieleki","Regidrago","Glastrier","Spectrier","Calyrex","Enamorus","Wo-Chien","Chien-Pao","Ting-Lu","Chi-Yu","Koraidon","Miraidon"];
  // prettier-ignore
  const mythicalLegends=["Mew","Celebi","Jirachi","Deoxys","Phione","Manaphy","Darkrai","Shaymin","Arceus","Victini","Keldeo","Meloetta","Genesect","Diancie","Mega Diancie","Hoopa","Volcanion","Magearna","Marshadow","Zeraora","Meltan","Melmetal","G-Max Melmetal","Zarude",]
  // prettier-ignore
  const ultrabeastLegends=["Nihilego","Buzzwole","Pheromosa","Xurkitree","Celesteela","Kartana","Guzzlord","Poipole","Naganadel","Stakataka","Blacephalon",];
  // prettier-ignore
  const paradoxLegends=["Great Tusk","Scream Tail","Brute Bonnet","Flutter Mane","Slither Wing","Sandy Shocks","Iron Treads","Iron Bundle","Iron Hands","Iron Jugulis","Iron Moth","Iron Thorns","Roaring Moon","Iron Valiant","Koraidon","Miraidon","Walking Wake","Iron Leaves",];
  const legends = [
    ...normalLegends,
    ...mythicalLegends
    // ...ultrabeastLegends,
    // ...paradoxLegends,
  ];

  async function clickButton(selector, value) {
    setTimeout(async () => {
      const potentialButtons = document.querySelectorAll(selector);
      for (const btn of potentialButtons) {
        const val = btn.value.trim();
        const altBtn = btn.style.length;
        if (val === value && altBtn === 0) {
          btn.click();
        }
      }
    }, 500);
  }

  function isCaptured() {
    let captured = false;
    if (
      document.querySelector('div#showpoke > img').src !==
      'https://i.dstatic.com/images/pokeball-n.png'
    ) {
      // pokeball-n means pokemon is not captures, ie black pokeball
      captured = true;
    }
    return captured;
  }

  function isDesired() {
    let desire = false;
    if (document.querySelector('a#dexy')) {
      const pokemonName = document.querySelector('a#dexy').textContent;

      const CATCH_UNIQUES = true;

      if (CATCH_UNIQUES) {
        // if (!isCaptured()) {
        if (
          pokemonName.includes('Retro')
          // && !isCaptured()
        ) {
          desire = true;
          if (pokemonName.includes('Unown') && isCaptured()) desire = false;
        }
        // line break -
        else if (
          pokemonName.includes('Shiny')
          // && !isCaptured()
        ) {
          desire = true;
          if (pokemonName.includes('Unown') && isCaptured()) desire = false;
        }
        // line break -
        else if (
          pokemonName.includes('Chrome')
          // && !isCaptured()
        ) {
          desire = true;
          if (pokemonName.includes('Unown') && isCaptured()) desire = false;
        }
        // line break -
        // else if (pokemonName.includes('Mirage') && !isCaptured()) {
        //   desire = true;
        //   if (pokemonName.includes('Unown') && isCaptured()) desire = false;
        // }
        // // line break -
        // else if (pokemonName.includes('Negative') && !isCaptured()) {
        //   desire = true;
        //   if (pokemonName.includes('Unown') && isCaptured()) desire = false;
        // }
        // // line break -
        // else if (pokemonName.includes('Metallic') && !isCaptured()) {
        //   desire = true;
        //   if (pokemonName.includes('Unown') && isCaptured()) desire = false;
        // }
        // // line break -
        // else if (pokemonName.includes('Ghostly') && !isCaptured()) {
        //   desire = true;
        //   if (pokemonName.includes('Unown') && isCaptured()) desire = false;
        // }
        // // line break -
        // else if (pokemonName.includes('Dark') && !isCaptured()) {
        //   desire = true;
        //   if (pokemonName.includes('Unown') && isCaptured()) desire = false;
        // }
        // // line break -
        // else if (pokemonName.includes('Shadow') && !isCaptured()) {
        //   desire = true;
        //   if (pokemonName.includes('Unown') && isCaptured()) desire = false;
        // }
        // // line break -
        // else if (pokemonName.includes('') && !isCaptured()) {
        //   desire = true;
        //   if (pokemonName.includes('Unown') && isCaptured()) desire = false;
        // }
        // the bracket below is for parent captured check
        // }
      }

      /// // L E G E N D S // ///
      const SIMPLE_LEGEND_FIND = true;

      if (!desire) {
        if (SIMPLE_LEGEND_FIND) {
          legends.forEach((legend) => {
            if (pokemonName.includes(legend)) {
              desire = true;
            }
          });
        } else {
          legends.forEach((legend) => {
            if (pokemonName.includes(legend)) {
              // prettier-ignore
              const wantedSpirites = ['Shiny', 'Retro', 'Chrome', 'Mirage', 'Negative', 'Metallic', 'Ghostly', 'Dark', 'Shadow']; // probability: 5 1 5 5 5 10 10 10 10
              const res = wantedSpirites.find((spirit) =>
                pokemonName.includes(spirit)
              );
              if (res) {
                desire = true;
              }
            }
          });
        }
      }

      const CATCH_FOR_DEX = false;
      // prettier-ignore
      const toCaptureForDex = [
        // put pokes to whitelist here
      ];
      if (CATCH_FOR_DEX) {
        toCaptureForDex.forEach((poke) => {
          if (pokemonName.includes(poke) && !isCaptured()) {
            desire = true;
          }
        });
      }

      return desire;
    }
  }

  function move() {
    const moveVector = [
      document.querySelector('a#move_n'), // moveNorth
      document.querySelector('a#move_nw'), // moveNorthWest
      document.querySelector('a#move_ne'), // moveNorthEast
      document.querySelector('a#move_e'), // moveEast
      document.querySelector('a#move_w'), // moveWest
      document.querySelector('a#move_s'), // moveSouth
      document.querySelector('a#move_sw'), // moveSouthWest
      document.querySelector('a#move_se') // moveSouthEast
    ]; // assuming moveVector is defined somewhere
    const maxAttempts = 16; // maximum number of attempts to find a suitable vector

    const findVector = () => {
      for (let attempt = 0; attempt < maxAttempts; attempt++) {
        const vector =
          moveVector[Math.floor(Math.random() * moveVector.length)];
        const inside = vector.querySelector('div');

        if (!inside.classList.contains('m-disable')) {
          return vector;
        }
      }
      return null; // return null if no suitable vector is found
    };

    const vector = findVector();
    if (vector) {
      vector.click();
    } else {
      // console.error('No suitable vector found');
      overworld();
    }
  }

  (function () {
    setTimeout(() => {
      const main = setInterval(
        async () => {
          const wanted = isDesired();
          if (wanted) {
            clearInterval(main);
            await GM_setValue('toReload', 'yes');
            clickButton('.btn-catch-action', 'Try to Catch It');
          } else {
            setTimeout(move, 200);
          }
        },
        Math.floor(
          Math.random() * (1000 * DELAY_MULTIPLIER + 200 * DELAY_MULTIPLIER) -
            200 * DELAY_MULTIPLIER
        )
      );
    }, 2000);
  })();
})();
