// ==UserScript==
// @name         Catching Captcha
// @namespace    captcha.catch
// @match        https://*.delugerpg.com/*
// @grant        none
// @version      1.0.1
// @author       rosavant
// @description  Listens to captchas that occur during pokemon catching. Solves them with the help of Recaptcha Solver addon
// @run-at       document-end
// @updateURL    https://gitlab.com/rosavant/delugerpg-bots/raw/master/src/pokemon-catcher/catching-captcha.user.js
// @downloadURL  https://gitlab.com/rosavant/delugerpg-bots/raw/master/src/pokemon-catcher/catching-captcha.user.js
// ==/UserScript==

const SUBDOMAIN = document.location.host.split('.')[0];
const WEBSITE = `https://${SUBDOMAIN}.delugerpg.com`;
const SECOND = 1000;
const MINUTE = 60 * SECOND;

function overworld() {
  const groupedMaps = {
    // overworld: [
    //   '/map/overworld1',
    //   '/map/overworld2',
    //   '/map/overworld3',
    //   '/map/overworld4',
    //   '/map/overworld5',
    //   '/map/overworld6',
    //   '/map/overworld7',
    //   '/map/overworld8',
    //   '/map/overworld9',
    //   '/map/overworld10',
    //   '/map/overworld11',
    //   '/map/overworld12',
    //   '/map/overworld13',
    //   '/map/overworld14',
    //   '/map/overworld15',
    //   '/map/overworld16',
    //   '/map/overworld17',
    //   '/map/overworld18',
    //   '/map/overworld19',
    //   '/map/overworld20',
    //   '/map/overworld21',
    //   '/map/overworld22',
    //   '/map/overworld23',
    //   '/map/overworld24',
    //   '/map/overworld25',
    // ],
    snowcave: [
      '/map/snowcave1',
      '/map/snowcave2',
      '/map/snowcave3',
      '/map/snowcave4'
    ],
    volcano: [
      '/map/volcano1',
      '/map/volcano2',
      '/map/volcano3',
      '/map/volcano4'
    ],
    // unownruins: ['/map/unownruins1', '/map/unownruins2'],
    pkmntower: [
      '/map/pkmntower1',
      '/map/pkmntower2',
      '/map/pkmntower3',
      '/map/pkmntower3'
    ],
    powerplant: [
      '/map/powerplant1',
      '/map/powerplant2',
      '/map/powerplant3',
      '/map/powerplant3'
    ],
    pkmnmansion: [
      '/map/pkmnmansion1',
      '/map/pkmnmansion2',
      '/map/pkmnmansion3',
      '/map/pkmnmansion4'
    ],
    rockcave: [
      '/map/rockcave1',
      '/map/rockcave2',
      '/map/rockcave3',
      '/map/rockcave4'
    ]
  };

  // Function to select a random map from each group
  function getRandomMap() {
    const randomMaps = Object.values(groupedMaps).map((group) => {
      const randomIndex = Math.floor(Math.random() * group.length);
      return group[randomIndex];
    });
    const randomIndex = Math.floor(Math.random() * randomMaps.length);
    return randomMaps[randomIndex];
  }
  const gotoMapURL = `${WEBSITE}${getRandomMap()}`;
  document.location.href = gotoMapURL;
}

main();
setInterval(main, 15 * MINUTE);

function main() {
  if (document.location.pathname.includes('unlock')) {
    (async function container() {
      async function clickButton(selector, value) {
        setTimeout(async () => {
          // There is an overlay over captcha's that prevent solving them programmatically so we will remove it
          document.querySelectorAll('body > div').forEach((element) => {
            const el = element.querySelector('div:nth-child(1)')
              ? element.querySelector('div:nth-child(1)')
              : null;
            const isOverlay = el
              ? el.style.opacity === '0.05' &&
                el.style.backgroundColor === 'rgb(255, 255, 255)'
              : false;
            if (isOverlay) el.remove();
          });
          const potentialButtons = document.querySelectorAll(selector);
          for (const btn of potentialButtons) {
            const val = btn.value.trim();
            const altBtn = btn.style.length;
            if (val === value && !altBtn) {
              btn.click();
            }
          }
        }, 1000);
      }

      setInterval(function recurr() {
        if (document.location.pathname.includes('unlock')) {
          if (
            document.querySelector('#col2').textContent.trim() ===
            'Nothing to See Here'
          ) {
            overworld();
          } else if (document.querySelector('.ReCaptcha_solver')) {
            const doCaptcha = setInterval(() => {
              const recaptchaSolver =
                document.querySelector('.ReCaptcha_solver');
              if (recaptchaSolver) {
                const status = recaptchaSolver.textContent.trim();
                if (status === 'SOLVED' || !!grecaptcha.getResponse()) {
                  clearInterval(doCaptcha);
                  setTimeout(
                    () => clickButton('form > .btn-primary', 'Return to Game'),
                    4500
                  );
                } else if (status === 'Error from 2captcha.com') {
                  document.location.reload();
                }
              }
            }, 4500);
          } else {
            setTimeout(() => {
              if (document.location.pathname.includes('unlock')) {
                recurr();
              }
            }, 4 * MINUTE);
          }
        }
      }, 30 * SECOND);

      setTimeout(() => {
        window.location.reload();
      }, 9 * MINUTE);
    })();
  }
}
