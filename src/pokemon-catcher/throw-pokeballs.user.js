// ==UserScript==
// @name         Throw Pokeballs
// @description  oo
// @namespace    catch.throw
// @version      2024-04-14
// @match        https://*.delugerpg.com/catch*
// @match        https://*.delugerpg.com/catch
// @match        https://*.delugerpg.com/catch/*/*
// @match        https://*.delugerpg.com/pokemon/release*
// @updateURL    https://gitlab.com/rosavant/delugerpg-bots/raw/master/src/pokemon-catcher/throw-pokeballs.user.js
// @downloadURL  https://gitlab.com/rosavant/delugerpg-bots/raw/master/src/pokemon-catcher/throw-pokeballs.user.js
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM_deleteValue
// ==/UserScript==

const DELAY_MULTIPLIER = weightedRandom(1.7, undefined, 2.5);
const ULTRA_BALL_PROBABILITY = 0.666; // prob < ULTRA_BALL_PROBABILITY ==> throw greatball
const CATCH_AND_RELEASE = 1;
// const CATCH_AND_RELEASE = false;
const SUBDOMAIN = document.location.host.split('.')[0];
const WEBSITE = `https://${SUBDOMAIN}.delugerpg.com`; // website's hostname

// TODO - change forEach includes to array contains?

/**
 * From https://stackoverflow.com/a/46774731
 * Generates random time with minimum having the most weight
 * @param {number} min lets say I want 0.5 as a multiplier, consider x ,so I'll do x * 10 theb min = (x * 10) - 1
 * @param {number} max lets say I want it to be 5, consider y, so I'll do y * 10 then max = (y*10) - min
 * @param {number} fixed should always be one make range by remind one from lower range and then subtracting lower range from max like explained above
 * @returns {number} time
 */

function weightedRandom(min, fixed = 1, max) {
  // Check if the min and max values are valid
  if (min >= max) {
    return null;
  }

  // Scale up the min and max to allow for decimal precision
  let scaledMin = min * 10 - 1;
  let scaledMax = max * 10;

  // Calculate the scaled range
  let scaledRange = scaledMax - scaledMin;

  // Generate a weighted random number
  // The 'fixed' value is used to skew the distribution towards the lower end
  // The use of 'Math.random()' generates a value between 0 and 1
  // Multiplying this by the scaled range and adding the 'fixed' value skews the randomness
  let weightedRandom = Math.round(
    scaledRange / (Math.random() * scaledRange + fixed)
  );

  // Rescale to the original range and return
  return (scaledMin + weightedRandom) / 10;
}

// prettier-ignore
const normalLegends=["Articuno","Zapdos","Moltres","Mewtwo","Mega Mewtwo X","Mega Mewtwo Y","Raikou","Entei","Suicune","Lugia","Ho-oh","Regirock","Regice","Registeel","Latias","Mega Latias","Latios","Mega Latios","Kyogre","Primal Kyogre","Groudon","Primal Groudon","Rayquaza","Mega Rayquaza","Uxie","Mesprit","Azelf","Dialga","Palkia","Heatran","Regigigas","Giratina","Cresselia","Cobalion","Terrakion","Virizion","Tornadus","Thundurus","Reshiram","Zekrom","Landorus","Kyurem","Xerneas","Yveltal","Zygarde","Type: Null","Silvally","Tapu Koko","Tapu Lele","Tapu Bulu","Tapu Fini","Cosmog","Cosmoem","Solgaleo","Lunala","Necrozma","Zacian","Zamazenta","Eternatus","Kubfu","Urshifu","G-Max Urshifu","Regieleki","Regidrago","Glastrier","Spectrier","Calyrex","Enamorus","Wo-Chien","Chien-Pao","Ting-Lu","Chi-Yu","Koraidon","Miraidon"];
// prettier-ignore
const mythicalLegends=["Mew","Celebi","Jirachi","Deoxys","Phione","Manaphy","Darkrai","Shaymin","Arceus","Victini","Keldeo","Meloetta","Genesect","Diancie","Mega Diancie","Hoopa","Volcanion","Magearna","Marshadow","Zeraora","Meltan","Melmetal","G-Max Melmetal","Zarude",]
// prettier-ignore
const ultrabeastLegends=["Nihilego","Buzzwole","Pheromosa","Xurkitree","Celesteela","Kartana","Guzzlord","Poipole","Naganadel","Stakataka","Blacephalon",];
// prettier-ignore
const paradoxLegends=["Great Tusk","Scream Tail","Brute Bonnet","Flutter Mane","Slither Wing","Sandy Shocks","Iron Treads","Iron Bundle","Iron Hands","Iron Jugulis","Iron Moth","Iron Thorns","Roaring Moon","Iron Valiant","Koraidon","Miraidon","Walking Wake","Iron Leaves",];

function isLegend(name) {
  const legends = [
    ...normalLegends,
    ...mythicalLegends
    // ...ultrabeastLegends,
    // ...paradoxLegends,
  ];
  let isOrNot = false;
  legends.forEach((legend) => {
    if (name.includes(legend.toLowerCase())) {
      isOrNot = true;
    }
  });
  // legends.forEach((legend) => {
  //   if (name.includes(legend)) {
  //     // const wantedSpirites = ['Shiny', 'Retro', 'Chrome'];
  //     // prettier-ignore
  //     // const wantedSpirites = ['Shiny', 'Retro', 'Chrome', 'Mirage', 'Negative']; // probability: 5 1 5 5 5
  //     // prettier-ignore
  //     // const wantedSpirites = ['Shiny', 'Retro', 'Chrome', 'Mirage', 'Negative', 'Metallic', 'Ghostly', 'Dark', 'Shadow']; // probability: 5 1 5 5 5 10 10 10 10
  //     // const wantedSpirites = ['Shiny', 'Retro', 'Chrome', 'Lugia (Shadow)']; // probability: 5 1 5 5 5 10 10 10 10
  //     const res = wantedSpirites.find((spirit) => name.includes(spirit));
  //     if (res) isOrNot = true;
  //   }
  // });
  return isOrNot;
}

function catchDex(name) {
  // prettier-ignore
  const toCaptureForDex = [
    // Insert pokemons here
     //"Retro G-Max Copperajah", "Ghostly Dracozolt", "Shadow Dracozolt", "Negative Dracozolt", "Retro Dracozolt"...
    // Gen 9
    // "Sprigatito","Floragato","Meowscarada","Fuecoco","Crocalor","Skeledirge","Quaxly","Quaxwell","Quaquaval","Lechonk","Oinkologne","Tarountula","Spidops","Nymble","Lokix","Pawmi","Pawmo","Pawmot","Tandemaus","Maushold","Fidough","Dachsbun","Smoliv","Dolliv","Arboliva","Squawkabilly","Nacli","Naclstack","Garganacl","Charcadet","Armarouge","Ceruledge","Tadbulb","Bellibolt","Wattrel","Kilowattrel","Maschiff","Mabosstiff","Shroodle","Grafaiai","Bramblin","Brambleghast","Toedscool","Toedscruel","Klawf","Capsakid","Scovillain","Rellor","Rabsca","Flittle","Espathra","Tinkatink","Tinkatuff","Tinkaton","Wiglett","Wugtrio","Bombirdier","Finizen","Palafin","Varoom","Revavroom","Cyclizar","Orthworm","Glimmet","Glimmora","Greavard","Houndstone","Flamigo","Cetoddle","Cetitan","Veluza","Dondozo","Tatsugiri","Annihilape","Clodsire","Farigiraf","Dudunsparce","Kingambit","Great Tusk","Scream Tail","Brute Bonnet","Flutter Mane","Slither Wing","Sandy Shocks","Iron Treads","Iron Bundle","Iron Hands","Iron Jugulis","Iron Moth","Iron Thorns","Frigibax","Arctibax","Baxcalibur","Gimmighoul","Gholdengo","Wo-Chien","Chien-Pao","Ting-Lu","Chi-Yu","Roaring Moon","Iron Valiant","Koraidon","Miraidon","Walking Wake","Iron Leaves"
    // ...ultrabeastLegends,
    // ...paradoxLegends
    ];
  let isOrNot = false;
  toCaptureForDex.forEach((poke) => {
    if (name.includes(poke)) {
      isOrNot = true;
    }
  });
  return isOrNot;
}

function overworld() {
  const groupedMaps = {
    overworld: [
      '/map/overworld1',
      '/map/overworld2',
      '/map/overworld3',
      '/map/overworld4',
      '/map/overworld5',
      '/map/overworld6',
      '/map/overworld7',
      '/map/overworld8',
      '/map/overworld9',
      '/map/overworld10',
      '/map/overworld11',
      '/map/overworld12',
      '/map/overworld13',
      '/map/overworld14',
      '/map/overworld15',
      '/map/overworld16',
      '/map/overworld17',
      '/map/overworld18',
      '/map/overworld19',
      '/map/overworld20',
      '/map/overworld21',
      '/map/overworld22',
      '/map/overworld23',
      '/map/overworld24',
      '/map/overworld25'
    ],
    snowcave: [
      '/map/snowcave1',
      '/map/snowcave2',
      '/map/snowcave3',
      '/map/snowcave4'
    ],
    volcano: [
      '/map/volcano1',
      '/map/volcano2',
      '/map/volcano3',
      '/map/volcano4'
    ],
    // unownruins: ['/map/unownruins1', '/map/unownruins2'],
    pkmntower: [
      '/map/pkmntower1',
      '/map/pkmntower2',
      '/map/pkmntower3',
      '/map/pkmntower3'
    ],
    powerplant: [
      '/map/powerplant1',
      '/map/powerplant2',
      '/map/powerplant3',
      '/map/powerplant3'
    ],
    pkmnmansion: [
      '/map/pkmnmansion1',
      '/map/pkmnmansion2',
      '/map/pkmnmansion3',
      '/map/pkmnmansion4'
    ],
    rockcave: [
      '/map/rockcave1',
      '/map/rockcave2',
      '/map/rockcave3',
      '/map/rockcave4'
    ]
  };

  // Function to select a random map from each group
  function getRandomMap() {
    const randomMaps = Object.values(groupedMaps).map((group) => {
      const randomIndex = Math.floor(Math.random() * group.length);
      return group[randomIndex];
    });
    const randomIndex = Math.floor(Math.random() * randomMaps.length);
    return randomMaps[randomIndex];
  }
  const gotoMapURL = `${WEBSITE}${getRandomMap()}`;
  document.location.href = gotoMapURL;
}

(async function catchEm() {
  'use strict';
  let flag = null;

  setTimeout(
    async () => {
      flag = true;
      setTimeout(
        async () => {
          if (flag) {
            await GM_deleteValue('difficultPokemonToCatch');
            window.location.reload();
          }
        },
        10 * 60 * 1000
      );
    },
    10 * 60 * 1000
  );

  if (
    document.location.href.includes('/pokemon/release') &&
    CATCH_AND_RELEASE
  ) {
    if (document.location.href.includes('confirm')) {
      // setTimeout(overworld, 125); // HERE
      overworld();
    }
    setTimeout(() => {
      if (!document.location.href.includes('confirm')) {
        if (
          document.querySelector('a.btn.btn-primary').href.includes('release')
        ) {
          setTimeout(
            () => document.querySelector('a.btn.btn-primary').click(),
            500 * DELAY_MULTIPLIER
          );
        }
      }
    }, 1000 * DELAY_MULTIPLIER);
  }

  let loggerCount = true;
  async function logger() {
    // FIXME not redirecting
    try {
      if (!loggerCount) return;
      loggerCount = false;
      const getCaught = Number(GM_getValue('catches', 0));
      GM_setValue('catches', 1 + getCaught).then(() => {
        if (totalCaught >= STOP_AT_CAUGHT) {
          GM_openInTab(
            `https://www.google.com/search?client=firefox-b-d&q=CAUGHT ${totalCaught}%21`
          );
          window.close();
        }
      });
    } catch (e) {}
  }

  async function checkIfReleasePokemon() {
    const infobox = document.querySelector('div.infobox');
    if (!infobox) return; // Exit early if infobox is not found

    const hasSpe = !!infobox.querySelector('i.sbtn-spe');
    const hasAtk = !!infobox.querySelector('i.sbtn-atk');
    const hasDef = !!infobox.querySelector('i.sbtn-def');
    const releaseButton = document.querySelector(
      'a.btn.btn-danger.btn-sm.btn-sc'
    );
    const pokemonName = infobox
      .querySelector('.infobox > b')
      .textContent.trim()
      .toLowerCase();

    if (shouldRelease(releaseButton, hasSpe, hasAtk, hasDef, pokemonName)) {
      await updateStats(hasSpe, hasAtk, hasDef, pokemonName);
      setTimeout(() => releaseButton.click(), 500 * DELAY_MULTIPLIER);
    } else {
      await updateStats(hasSpe, hasAtk, hasDef, pokemonName);
      overworld();
    }
  }

  function shouldRelease(releaseButton, hasSpe, hasAtk, hasDef, pokemonName) {
    return (
      releaseButton.href.includes('release') &&
      CATCH_AND_RELEASE &&
      !hasSpe &&
      !isLegend(pokemonName) &&
      !catchDex(pokemonName)
    );
  }

  async function updateStats(hasSpe, hasAtk, hasDef, pokemonName) {
    if (isLegend(pokemonName)) {
      await incrementGMValue('legends');
    }
    if (hasSpe && hasAtk && hasDef) {
      await incrementGMValue('tses');
    }
    if (hasSpe) {
      await incrementGMValue('spes');
    }
    if (hasAtk) {
      await incrementGMValue('atks');
    }
    if (hasDef) {
      await incrementGMValue('defs');
    }
  }

  async function incrementGMValue(key) {
    const currentValue = Number(await GM_getValue(key, 0));
    await GM_setValue(key, currentValue + 1);
  }

  async function clickButton(selector, value) {
    setTimeout(async () => {
      const potentialButtons = document.querySelectorAll(selector);
      for (const btn of potentialButtons) {
        const val = btn.value.trim();
        const altBtn = btn.style.length;
        if (val === value && altBtn === 0) {
          btn.click();
        }
      }
    }, 500 * DELAY_MULTIPLIER);
  }

  async function clickButtonLooselyMatch(selector, value) {
    setTimeout(async () => {
      const potentialButtons = document.querySelectorAll(selector);
      for (const btn of potentialButtons) {
        const val = btn.value.trim();
        const altBtn = btn.style.length;
        if (val.includes(value) && altBtn === 0) {
          btn.click();
        }
      }
    }, 500 * DELAY_MULTIPLIER);
  }

  /**
   * Check if a button with a specific class and value exists
   * @param {string} selector targetted button's class
   * @param {string} value targetted button's value
   * @returns boolean
   */
  function hasButton(selector, value) {
    const potentialButtons = document.querySelectorAll(selector);
    for (const btn of potentialButtons) {
      const val = btn.value.trim();
      const altBtn = btn.style.length;
      if (val === value && altBtn === 0) {
        return true;
      }
    }
    return false;
  }

  let difficultPokemonToCatch = await GM_getValue('difficultPokemonToCatch', 0);

  setInterval(async () => {
    if (hasButton('.btn-battle-action', 'Continue')) {
      await GM_setValue('difficultPokemonToCatch', difficultPokemonToCatch + 1);
      clickButton('.btn-battle-action', 'Continue');
    }
  }, 3500 * DELAY_MULTIPLIER);

  if (document.location.pathname.includes('/catch/')) {
    // const buttons = document.querySelectorAll('input.btn-battle-action');
    // if (buttons[0].style.display === 'none') {
    //   buttons[1].click();
    // } else {
    //   buttons[0].click();
    // }

    setInterval(async () => {
      if (hasButton('.btn-battle-action', 'Continue')) {
        await GM_setValue(
          'difficultPokemonToCatch',
          difficultPokemonToCatch + 1
        );
        clickButton('.btn-battle-action', 'Continue');
      }
    }, 3500 * DELAY_MULTIPLIER);

    if (hasButton('.btn-battle-action', 'Continue')) {
      await GM_setValue('difficultPokemonToCatch', difficultPokemonToCatch + 1);
      clickButton('.btn-battle-action', 'Continue');
    } else {
      clickButton('.btn-battle-action', 'Start Battle');
    }
  }
  if (document.querySelector('div.infobox')) {
    setTimeout(async () => {
      if (document.querySelector('div.infobox')) {
        if (
          document
            .querySelector('div.infobox')
            .textContent.substring(0, 10)
            .trim() === 'You threw'
        ) {
          await GM_deleteValue('difficultPokemonToCatch');
          await logger();
          await checkIfReleasePokemon();
          // overworld();
        }
      }
    }, 1000 * DELAY_MULTIPLIER);
  }
  const throwing = setInterval(() => {
    const hasPokeball = Number(
      document
        .querySelector('input#item-pokeball')
        .closest('.itemitem')
        .querySelector('.iname')
        .textContent.trim()
        .match(/\((\d+)\)/)[1]
    );
    const hasGreatball = Number(
      document
        .querySelector('input#item-greatball')
        .closest('.itemitem')
        .querySelector('.iname')
        .textContent.trim()
        .match(/\((\d+)\)/)[1]
    );
    const hasUltraball = Number(
      document
        .querySelector('input#item-ultraball')
        .closest('.itemitem')
        .querySelector('.iname')
        .textContent.trim()
        .match(/\((\d+)\)/)[1]
    );
    const hasMasterball = Number(
      document
        .querySelector('input#item-masterball')
        .closest('.itemitem')
        .querySelector('.iname')
        .textContent.trim()
        .match(/\((\d+)\)/)[1]
    );
    const oppHpText = document
      .querySelector('#opponent > .pokemon > .hp')
      .textContent.trim()
      .split('/');
    const oppHp = Number(oppHpText[oppHpText.length - 1]);
    if (
      (hasGreatball > 0 || hasUltraball > 0 || hasPokeball > 0) &&
      oppHp < 150 &&
      !difficultPokemonToCatch < 2
    ) {
      const prob = Math.random();
      if (
        prob < ULTRA_BALL_PROBABILITY &&
        (hasGreatball !== 0 || hasGreatball !== 0)
      ) {
        const fiftyFiftyPokeGreatBall = Math.random();
        if (hasPokeball > 0 && fiftyFiftyPokeGreatBall >= 0.5) {
          document
            .querySelector('input#item-pokeball')
            .closest('.itemitem')
            .click();
        } else {
          document
            .querySelector('input#item-greatball')
            .closest('.itemitem')
            .click();
        }
      } else {
        document
          .querySelector('input#item-ultraball')
          .closest('.itemitem')
          .click();
        // document.querySelector('input#item-ultraball').click();
        // clickPokeballButton('input#item-ultraball');
      }
    } else if (
      (hasMasterball > 0 && oppHp >= 150) ||
      (hasMasterball > 0 &&
        hasGreatball === 0 &&
        hasUltraball === 0 &&
        hasPokeball === 0) ||
      difficultPokemonToCatch >= 2
    ) {
      // document.querySelector('input#item-masterball').click();
      document
        .querySelector('input#item-masterball')
        .closest('.itemitem')
        .click();
      // clickPokeballButton('input#item-masterball');
    } else {
      window.close();
      // console.error(oppHp, hasGreatball, hasUltraball, hasMasterball);
    }
    setTimeout(async () => {
      clickButtonLooselyMatch('.btn-battle-action', 'Throw ');
      setTimeout(async () => {
        if (document.querySelector('div.infobox')) {
          clearInterval(throwing);
          if (
            document
              .querySelector('div.infobox')
              .textContent.substring(0, 10)
              .trim() === 'You threw'
          ) {
            await GM_deleteValue('difficultPokemonToCatch');
            await logger();
            await checkIfReleasePokemon();
            // overworld();
          }
        }
      }, 1000 * DELAY_MULTIPLIER);
    }, 250 * DELAY_MULTIPLIER);
    setTimeout(
      async () => {
        await GM_deleteValue('difficultPokemonToCatch');
        setTimeout(overworld, 200);
      },
      5 * 60 * 1000 * DELAY_MULTIPLIER
    );
  }, 1100 * DELAY_MULTIPLIER);
})();
