<center><img src="./img/logo.png" alt="logo" id="top-placeholder"></center>

# DelugeRPG Cheats


DelugeRPG Hacks is a collection of automation scripts for [DelugeRPG](https://delugerpg.com). Here you'll find multiple DelugeRPG [userscripts](https://en.wikipedia.org/wiki/Userscript) that are run using a userscript manager browser extension (e.g., [Tampermonkey](https://www.tampermonkey.net/)). Over time, I've automated _almost every_ possible aspect from the client side. For example:

- DelugeRPG auto Pokemon finder
- DelugeRPG Pokemon catcher
- DelugeRPG experience bot
- Duplicate Pokemon remover
- Specific Pokemon selector for making offers
- TS Pokemon trade finder

> __WARNING:__
> I am not responsible if you are banned from the game. The bots have a very low footprint, but if you use any script, be prepared mentally to possibly be banned in the future, especially now that the scripts are public. I'm pretty sure the game admin will take a look at these scripts at some point.

The scripts listed above are the ones I used daily. I wasn't banned until I decided I was playing too much even though I had completed all the in-game goals. I was bored and needed to quit.

The only way I found to "hard quit" was by getting banned. So, I ran the bot continuously, catching all the Pokemons for about 68 hours. I'd say my ban was self-inflicted; otherwise, why wasn’t I banned during the two years I played?

Prior to the ban, my account was massive, worth approximately 20 billion EXP, plus an additional 2 billion liquid EXP (200x10m Pokemons,) and over 100,000 wins. So in my personal experience the scripts did their job stealthily.

## Table of Contents

- [DelugeRPG Cheats](#delugerpg-cheats)
- [How To Use](#how-to-use)
- [Scripts](#scripts)
  - [Auto Catcher](#auto-catcher)
  - [Auto Exp Farming](#auto-exp-farming)
  - [Other Scripts](#other-scripts)
- [Solving Captchas](#solving-captchas)
- [Tips](#tips)

# How To Use

As briefly mentioned above, these are userscripts and require a userscript manager to run. Below are the step by step instructions on how to run the scripts in general:

1. Install Tampermonkey browser extension ([Chrome](https://chrome.google.com/webstore/detail/dhdgffkkebhmkfjojejmpbldmpobfkfo) | [Firefox](https://addons.mozilla.org/en-US/firefox/addon/tampermonkey/))
2. Install any userscript
    1. By going [here](./src)
    2. Clicking any file ending with `.user.js`
    3. Clicking `Raw` on top right
    4. A popup should open, click on the `Install` button

![Github Raw Button](./img/github-raw-visual.png)

Feel free to explore the repository and install any userscript, I may add descriptions for each but for now I'll describe the two potentially most desired scripts [Pokemon Auto Finder/Catcher](./src/pokemon-catcher/) and [Pokemon Auto EXP Farming](./src/farm-exp/).

> User: "I need more help, I don't understand!"

If you still need help or want to work scripts that have stopped working please open an issue [here](https://gitlab.com/delugerpg-hacks/delugerpg-cheats/-/issues) (you will have to create an account on this website first). Here is a video tutorial on [how to open an issue](https://www.youtube.com/watch?v=-4SGhGpwZDY). __Don't open an issue if you got banned in game. That is out of my control and you have already been warned of the dangers.__

# Scripts

## Auto Catcher

![badge](https://img.shields.io/badge/Status-Working-green)

<img src="img/pokemon-catcher.gif" height="250px">

This script is designed to catch any class. Currently it is configured to catch retro, shiny, and chrome class Pokemons and any class legends. You can modify which classes it catches by commenting or uncommenting the `if` blocks [here](https://github.com/rosavant/delugerpg-bots/blob/a80466d697bf445c7b6404e05c3982fd0da3b431/src/pokemon-catcher/pokemon-catcher.user.js#L269).

The script is configured to avoid using Master Balls for all Pokemon, reserving them only for legendary Pokemon. Therefore, it's advisable to keep all types of Poke Balls. I recommend prioritizing Great Balls and Ultra Balls, followed by Master Balls.

In addition, this script is configured to release pokemons that don't have `SPE` stat. This behavior can be changed by changing `const CATCH_AND_RELEASE` to 0.

### Download

For this to work you have to install multiple scripts, specifically:

- [pokemon-catcher.user.js](https://gitlab.com/delugerpg-hacks/delugerpg-cheats/-/blob/main/src/pokemon-catcher/pokemon-catcher.user.js)
- [throw-pokeballs.user.js](https://gitlab.com/delugerpg-hacks/delugerpg-cheats/-/blob/main/src/pokemon-catcher/throw-pokeballs.user.js)
- [walk-more.user.js](https://gitlab.com/delugerpg-hacks/delugerpg-cheats/-/blob/main/src/pokemon-catcher/walk-more.user.js)
- [catching-captcha.user.js](https://gitlab.com/delugerpg-hacks/delugerpg-cheats/-/blob/main/src/pokemon-catcher/catching-captcha.user.js) (optional)

## Auto Exp Farming

![badge](https://img.shields.io/badge/Status-Working-green)

<img src="img/exp-farming-2.gif" height="250px">

In DelugeRPG, you are limited to one attack per second and one battle every 10 seconds. This script has been designed with those limitations in mind. I recommend that you do not change the delays in the script. The script will train your Pokemon to just over 10 million experience points and then stop.

### Download

For this to work you have to install multiple scripts, specifically:

- [intelligent-grind.user.js](https://gitlab.com/delugerpg-hacks/delugerpg-cheats/-/blob/main/src/farm-exp/intelligent-grind.user.js)
- [grinding-captcha.user.js](https://gitlab.com/delugerpg-hacks/delugerpg-cheats/-/blob/main/src/farm-exp/grinding-captcha.user.js ) (optional)

## Other Scripts

![badge](https://img.shields.io/badge/Status-Working-green)

A lot of them, the duplicate remover, trade offerer and the ts trade finder ones are here. Might require some coding knowledge to sift through.

You can find them [here.](./src/utilities/) Mostly require one file for one feature. I've tried to keep self explainatory names.

# Solving Captchas

This is what makes botting in DelugeRPG challenging. You might try using a free library to solve ReCaptcha, but these typically have a very low success rate, or they may not run properly.

I used and recommend a [Captcha solving service](https://prowebscraper.com/blog/top-10-captcha-solving-services-compared/), but please note that this requires spending money. However, these services are quite affordable. I'd estimate that I played the game daily for two years, and in that time, I only spent about $15 (around $0.02 per day) on aggressive botting. If you bot more conservatively, it might cost even less.

# Tips

- Try to play like a human, perhaps an addicted one. That means not running the bots, specifically the EXP/Catcher ones, for the whole day as humans sleep.
- Try running only one bot at a time.
- If you open an alt account while botting, open it from a different device.

<!-- 
# Easter Eggs

- `https://www.delugerpg.com/ajax/pokemon/info/{pokemonID}` 
-->

---

[🔼 Back to top](#table-of-contents)
